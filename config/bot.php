<?php
return [
    'token' => env('BOT_TOKEN',''),
    'address' =>  env('BOT_ADDRESS',''),
    'domain' => 'https://api.telegram.org/bot',
    'file_domain' => 'https://api.telegram.org/file/bot',
    'commands' => [
        'start' => '/start',
        'usage' => '/usage',
        'upgrade' => '/upgrade',
        'default_thumbnail' => '/default_thumbnail',
        'settings' => '/settings'
    ],
    'remote_servers' => [
        'fileApi' => [
            'get_file_info' => 'http://87.107.172.249/fileapi/get_file_info.php',
            'get_file_content' => 'http://87.107.172.249/fileapi/get_file_content.php',
        ]
    ]
];

