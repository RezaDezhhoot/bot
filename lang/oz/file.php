<?php

return [
    'types' => [
        'file' => 'Fayl',
        'media' => 'OAV',
        'link' => 'Havola',
        'youtube' => 'Youtube'
    ],
    'link' => [
        'get_download_link' => '🔗Yuklab olish havolasini oling',
        'file_not_found' => '❌ Fayl topilmadi',
        'download_link_generated' => "
        ✅Faylni yuklab olish havolasi muvaffaqiyatli yaratildi

Havola: :link
        "
    ],
    'actions' => [
        'change_name' => '✏️Fayl nomini oʻzgartiring',
        'change_title' => '🎵Audio sarlavhasini oʻzgartirish',
        'change_performer' => "🗣Ijrochini o'zgartirish",
        'change_thumbnail' => "🌇 Eskizni o'zgartirish",
        'get_thumbnail' => '📥 eskiz olish',
        'add_soft_sub' => "💬Subtitr qoʻshish (SoftSub)",
        'change_soft_sub' => "💬Subtitrni oʻzgartirish",
        'auto_convert_false' => '⬜️Fayl turini avtomatik aniqlash (video, musiqa,...)',
        'auto_convert_ok' => '✅Fayl turini avtomatik aniqlash (video, musiqa,...)',
        'apply_changes' => "♻️O'zgarishlarni qo'llang va yangi faylni oling",
        'get_as_document_false' => '⬜️Telegram hujjati sifatida oling',
        'get_as_document_ok' => '✅Telegram hujjati sifatida oling',
        'start_download_link' => '⚡️Telegram faylini ishga tushiring va oling⚡️'
    ],
    'messages' => [
        'process_error' => "❌️ Mediani qayta ishlash jarayonida xatolik yuz berdi",
        'change_name_message' => "
        •
💾 Hozirgi ism: :name

📝 Iltimos, kengaytmali yangi fayl nomini kiriting:

📂 Misol: 123.mp4 yoki 123.mp3 va...

⛔️ Fayl nomi 60 yoki undan kam belgidan iborat boʻlishi kerak.
⚠️ Ruxsat berilmagan belgilar: / \ :? \" > < | # ~ agar ismingizda ushbu belgilar mavjud bo'lsa, ular o'chiriladi.
•
        ",
        'filename_changed' => "
        ✅ Fayl nomi muvaffaqiyatli oʻzgartirildi.
•
        ",
        'change_audio_title' => "
        🎵 Joriy audio nomi:
:title
✏️ Iltimos, ushbu xabarga javob sifatida yangi Sarlavha nomini yuboring:

•
        ",
        'audio_title_changed' => "
        ✅ Audio nomi muvaffaqiyatli oʻzgartirildi.
•
        ",
        'change_audio_performer' => "
        🗣 Hozirgi ijrochi nomi:
:performer
✏️ Iltimos, ushbu xabarga javob sifatida yangi Ijrochi ismingizni yuboring:

•
        ",
        'audio_performer_changed' => "
        ✅ Audio ijrochi muvaffaqiyatli o'zgartirildi.
•
        ",
        'change_thumbnail' => "
        🌆 Iltimos, ushbu xabarga javob sifatida yangi eskiz rasmingizni yuboring:

❔ Eskiz deb nomlanuvchi ushbu rasm video, musiqa yoki Telegram faylining boshida joylashtirilgan.

🖼 Shuningdek, agar siz rasmni birlamchi eskiz sifatida o‘rnatmoqchi bo‘lsangiz va undan keyingi safar foydalanmoqchi bo‘lsangiz, tugmasini bosing /default_thumbnail.
▫️
        ",
        'thumbnail_changed' => "
        ✅ Eskiz muvaffaqiyatli o'zgartirildi.
▫️
        ",
        'thumbnail_doesnt_exist' => "📁 Eskiz uchun rasm o‘rnatilmagan",
        'sent_subtitle_file' => "
        💬 Iltimos, ushbu xabarga javob sifatida videoga joylashtirmoqchi bo'lgan SRT subtitringizni yuboring.
▫️
        ",
        'subtitle_added' => "
        ✅ Subtitr muvaffaqiyatli qo'shildi.
        "
    ],
    'thumbnail' => [
        'default' => [
            'title' => "
            🖼 From the bottom menu you can make changes to the default Thumbnail.

❔ Eskiz deb nomlanuvchi ushbu rasm video, musiqa yoki Telegram faylining boshida joylashtirilgan.
▫️
            ",
            'btn' => [
                'get_current_default_thumbnail' => '🖼Joriy birlamchi eskizni oling',
                'change_default_thumbnail' => "⚙️Birlamchi eskizni oʻzgartiring",
                'delete_default_thumbnail' => "🗑Birlamchi eskizni oʻchirish",
            ],
            'doesnt_exist' => "📁 Birlamchi eskiz uchun rasm o‘rnatilmagan",
            'update' => '🖼 Iltimos, ushbu xabarga javob sifatida birlamchi eskiz rasmingizni yuboring.
🔙 Yoki bosing /start bekor qilish.
▫️',
            'changed' => "☑️ Birlamchi eskiz muvaffaqiyatli oʻzgartirildi.
▫️",
            'deleted' =>  "☑️ Birlamchi eskiz muvaffaqiyatli oʻchirildi."
        ]
    ],
    'photo' => [
        'result' => "
        🗃 Turi: Rasm 🎥
📥 Hajmi: :size
↔️ Ruxsat: :resolution
▫️
        "
    ],
    'audio' => [
        'result' => "
        🗃 Turi: Audio 🎵
📥 Hajmi: :size
🎵 Audio nomi:
:title
🗣 Ijrochi:
:performer
⏱ Davomiyligi: :duration
🌇 Eskiz bormi? :thumbnail
📄 Fayl nomi: :name

•
        "
    ],
    'document' => [
        'result' => "
        🗃 Turi: Hujjat 📁
📥 Hajmi: :size
🌇 Eskiz bormi? :thumbnail
📄 Fayl nomi: :name

•
        "
    ],
    'video' => [
        'result' => "
        🗃 Turi: Video 🎥
📥 Hajmi: :size
🌇 Eskiz bormi? :thumbnail
↔️ Ruxsat: :resolution
⏱ Davomiyligi: :duration
📄 Fayl nomi: :name

•
        "
    ],
    'url' => [
        'result1' => "
        💾 Fayl nomi: :name

📥 Fayl hajmi: :size

⏳ Holat: Tasdiqlash kutilmoqda...
•
        ",
        'result2' => "
        💾 Fayl nomi: :name

📥 Fayl hajmi: :size

⏳ Holat: Serverga yuklab olinmoqda...

⚡️ :progress
•
        ",
        'result3' => "
        💾 Fayl nomi: :name

📥 Fayl hajmi: :size

⏳ Holati: Telegramga yuklanmoqda (bir oz kuting)
•
        ",
    ]
];
