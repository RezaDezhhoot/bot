<?php

return [
    'processStarted' => '⏳Faylingizni tayyorlayapmiz...',
    'words' => [
        'yes' => 'Ha',
        'no' => "Yo'q"
    ],
    'lang' => [
        'select' => "🇺🇿 Oʻzingiz yoqtirgan tilni tanlang"
    ],
    'btn' => [
        'back' => 'Orqaga↩️',
    ],
    'commands' => [
        'start' => "
            📤 Agar menga faylning to'g'ridan-to'g'ri havolasini yuborsangiz, men uni telegram fayli sifatida yuborishim mumkin.

📥 Va aksincha, agar siz menga telegram faylini yuborsangiz, uni IDM yoki ADM kabi dasturlar bilan yuqori tezlikda yuklab olishingiz uchun sizga to'g'ridan-to'g'ri havolani yuborishim mumkin.

💥 Yoki YouTube'dan yuklab olish uchun menga YouTube video havolasini yuboring.
❔ Quyidagi buyruq bo'yicha siz YouTube'da qidirishingiz mumkin:
🔍 Masalan, videolarni qidirish uchun pitbull yozing

`@vid pitbull`

•
        ",
        'usage' => "
            📄 Ruxsat etilgan fayl hajmi: :max_file_size

📂 Kunlik foydalaniladigan hajm: :daly_volume

⏳ Kunlik qolgan hajm: :remaining_volume

-------
Agar siz 3,91 Gb gacha bo'lgan fayllarni yuklamoqchi yoki yuklab olmoqchi bo'lsangiz yoki kunlik limitingizni 60,00 Gb gacha oshirmoqchi bo'lsangiz, bosing. /upgrade 🌹
        ",
        "settings" => "
        ⚙️ Sozlamalar:

Nimani o'zgartirmoqchisiz? ☺️

Pastki menyudan tanlang: 👇
•
        "
    ],
];
