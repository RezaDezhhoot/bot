<?php

return [
    'btn' => [
        'change_language' => "Tilni o'zgartirish / تغییر زبان",
        'default_thumbnail' => 'Birlamchi eskiz 🖼',
        'change_thumbnail_size_ok' => "✅Video oʻlchamiga eskizni oʻzgartiring↔",
        'change_thumbnail_size_false' => "⬜️Video oʻlchamiga eskizni oʻzgartiring↔",
    ],
];
