<?php

return [
    'results' => [
        'waiting' => '🕧 Mijoz toʻlovni yuborishini kutmoqda.',
        'confirming' => '⏳ Tranzaksiya blokcheynda qayta ishlanmoqda.',
        'confirmed' => '🔘 Jarayon blokcheyn tomonidan tasdiqlangan.',
        'sending' => "🔁 Mablag'lar mijozning shaxsiy hamyoniga yuboriladi.",
        'partially_paid' => "⚠️ Toʻlangan summa buyurtma miqdoridan kam edi.

🔸 Kuzatuv kodi: :tracking_code
🔸 Miqdori: `:paid_amount`
🔸 Haqiqatda to'langan: `:actually_paid`

iltimos, qo'llab-quvvatlash xizmatiga murojaat qiling.",
        'finished' => "✅ Toʻlov muvaffaqiyatli amalga oshirildi.

🔸 Kuzatuv kodi: :tracking_code
🔸 Miqdori: :paid_amount
🔸 Obuna: :subscription
🔸 Ruxsat etilgan fayl hajmi: :max_file_size
🔸 Kunlik foydalaniladigan hajm: :daly_volume
        ",
        'failed' => "❌ Ba'zi turdagi xatolik tufayli to'lov tugallanmadi, qayta urinib ko'ring.",
    ],
    'payment' => [
        'paid' => "To'langan",
        'success_payment_creation' => "
        ✅ Toʻlov muvaffaqiyatli yaratildi.

🔸 Kuzatuv kodi: :tracking_code
🔸 Miqdori: `:amount`
🔸 Manzil: :address
        ",
        'error_payment_creation' => '❌ Toʻlovni yaratishda xatolik yuz berdi.',
        'try_again' => "Qayta urinib ko'ring"
    ],
    'invoice' => [
        'invoice' => 'Hisob-faktura',
        'date' => 'Sana',
        'customer' => 'Mijoz',
        'phone' => 'Telefon',
        'total' => 'Jami',
        'subscription' => 'Obuna',
        'currency' => 'Valyuta',
        'amount' => 'Miqdori',
    ],
    'messages' => [
        'creating' => "Buyurtmangizni qayta ishlaymiz..."
    ]
];
