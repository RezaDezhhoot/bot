<?php

return [
    'title' => '🌟 Maxsus obuna sotib oling',
    'size_allowed' => '📁 Ruxsat etilgan fayl hajmi: :size_allowed',
    'daily_volume' => '📅 Ruxsat etilgan kunlik hajm: :daily_volume',
    'price' => '💸 Miqdori: :price',
    'alert' => "🚫🚫 TeleFileGram botda VPN, proksi-server yoki filtrlash mavjud emas. Iltimos, bu maqsadda obuna sotib olmang. 🚫🚫",
    'buy' => '💲Sotib oling :title obuna',
    'limits' => [
        'convert_file' => 'fayllarni aylantirish',
        'upload_size' => 'yuklash hajmini oshirish',
        'daly_size' => 'kunlik yuklash hajmini oshirish',
    ],
];
