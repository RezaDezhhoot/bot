<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 Fayl nomi: `:name`

❔ Iltimos, fayl turini tanlang: 👇
•
    ",
        'select_quality' => "
        •
💾 Fayl nomi: `:name`
📽 Fayl turi: `:type`

❔ Iltimos, sifatni tanlang: 👇
•
        ",
        'select_type' => "
        •
💾 Fayl nomi: `:name`
📽 Fayl turi: `:type`
💿 Sifat: `:quality`
📥 Fayl hajmi: `:size`

❔ Iltimos, yuklab olish turini tanlang: 👇
•
        ",
        'final_result' => "
        •
💾 Fayl nomi: `:name`
📽 Fayl turi: `:type`
💿 Sifat: `:quality`
📥 Fayl hajmi: `:size`
❔ Yuklab olish turi: `:download_type`

⏳ Holat: Tasdiqlash kutilmoqda...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 Video (ovozli)",
        'audio_format' => '🔉 Faqat ovoz',
        'download_link' => 'Yuklab olish havolasi',
        'telegram_file' => 'Telegram fayl',
    ]
];
