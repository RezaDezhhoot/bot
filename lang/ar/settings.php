<?php

return [
    'btn' => [
        'change_language' => 'تغيير اللغة / تغییر زبان',
        'default_thumbnail' => 'الصورة المصغرة الافتراضية 🖼',
        'change_thumbnail_size_ok' => '"تغيير حجم الصورة المصغرة إلى حجم الفيديو"',
        'change_thumbnail_size_false' => '"تغيير حجم الصورة المصغرة إلى حجم الفيديو"',
    ],
];
