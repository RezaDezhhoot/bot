<?php

return [
    'results' => [
        'waiting' => 'انتظار قيام العميل بإرسال الدفعة.',
        'confirming' => '⏳ تتم معالجة المعاملة على blockchain.',
        'confirmed' => 'تم تأكيد العملية بواسطة blockchain.',
        'sending' => "🔁 يتم إرسال الأموال إلى المحفظة الشخصية للعميل.",
        'partially_paid' => "⚠️ المبلغ المدفوع كان أقل من مبلغ الطلب.

🔸 كود التتبع: :tracking_code
🔸 المبلغ: `:paid_amount`
🔸 مدفوع بالفعل: `:actually_paid`

يرجى الاتصال بالدعم.",
        'finished' => "✅ تم الدفع بنجاح.

🔸 كود التتبع: :tracking_code
🔸 المبلغ: :paid_amount
🔸 الاشتراك: :subscription
🔸 حجم الملف المسموح به: :max_file_size
🔸 الحجم اليومي المستخدم: :daly_volume
        ",
        'failed' => "❌ لم يكتمل الدفع بسبب خطأ من نوع ما ، يرجى المحاولة مرة أخرى.",
    ],
    'payment' => [
        'paid' => 'تم الدفع',
        'success_payment_creation' => "
        ✅ تم إنشاء الدفع بنجاح.

🔸 كود التتبع: :tracking_code
🔸 المبلغ: `:amount`
🔸 العنوان: :address
        ",
        'error_payment_creation' => '❌ خطأ في إنشاء الدفع.',
        'try_again' => 'حاول ثانية'
    ],
    'invoice' => [
        'invoice' => 'فاتورة',
        'date' => 'تاريخ',
        'customer' => 'عميل',
        'phone' => 'هاتف',
        'total' => 'المجموع',
        'subscription' => 'الاشتراك',
        'currency' => 'عملة',
        'amount' => 'كمية',
    ],
    'messages' => [
        'creating' => "نقوم بمعالجة طلبك ..."
    ]
];
