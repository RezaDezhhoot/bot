<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 اسم الملف: `:name`

❔ الرجاء تحديد نوع الملف: 👇
•
    ",
        'select_quality' => "
        •
💾 اسم الملف: `:name`
📽 نوع الملف: `:type`

❔ الرجاء تحديد الجودة: 👇
•
        ",
        'select_type' => "
        •
💾 اسم الملف: `:name`
📽 نوع الملف: `:type`
💿 الجودة: `:quality`
📥 حجم الملف: `:size`

❔ الرجاء تحديد نوع التنزيل: 👇
•
        ",
        'final_result' => "
        •
💾 اسم الملف: `:name`
📽 نوع الملف: `:type`
💿 الجودة: `:quality`
📥 حجم الملف: `:size`
❔ نوع التنزيل: `:download_type`

⏳ الحالة: في انتظار التأكيد ...
•
        "
    ],
    'key' => [
        'video_format' => "🎥فيديو (بالصوت)",
        'audio_format' => '🔉 الصوت فقط',
        'download_link' => 'رابط التحميل',
        'telegram_file' => 'ملف Telegram',
    ]
];
