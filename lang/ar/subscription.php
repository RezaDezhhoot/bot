<?php

return [
    'title' => '🌟 شراء اشتراك خاص',
    'size_allowed' => '📁 حجم الملف المسموح به: :size_allowed',
    'daily_volume' => '📅 الحجم اليومي المسموح به: :daily_volume',
    'price' => '💸 المبلغ: :price',
    'alert' => '🚫🚫 لا يتم توفير VPN أو وكيل أو تصفية في TeleFileGram bot. من فضلك لا تشتري اشتراك لهذا الغرض. 🚫🚫',
    'buy' => '💲 شراء الاشتراك:title ',
    'limits' => [
        'convert_file' => 'تحويل ملفات',
        'upload_size' => 'زيادة حجم التحميل',
        'daly_size' => 'زيادة حجم التحميل اليومي',
    ],
];
