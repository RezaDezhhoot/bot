<?php

return [
    'btn' => [
        'change_language' => 'Change Language / تغییر زبان',
        'default_thumbnail' => 'تصویر بندانگشتی پیش فرض 🖼',
        'change_thumbnail_size_ok' => '✅اندازه تصویر کوچک را به اندازه ویدئو تغییر دهید↔',
        'change_thumbnail_size_false' => '⬜️اندازه تصویر کوچک را به اندازه ویدیو تغییر دهید↔',
    ],
];
