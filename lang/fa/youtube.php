<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 نام فایل: `:name`

❔ لطفا نوع فایل را انتخاب کنید:👇
•
    ",
        'select_quality' => "
        •
💾 نام فایل: `:name`
📽 نوع فایل: `:type`

❔ لطفا کیفیت را انتخاب کنید:👇
•
        ",
        'select_type' => "
        •
💾 نام فایل: `:name`
📽 نوع فایل: `:type`
💿 کیفیت: `:quality`
📥 حجم فایل: `:size`

❔ Please select download type: 👇
•
        ",
        'final_result' => "
        •
💾 نام فایل: `:name`
📽 نوع فایل: `:type`
💿 کیفیت: `:quality`
📥 حجم فایل: `:size`
❔ نوع دانلود: `:download_type`

⏳ وضعیت: در انتظار تایید شما...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 ویدئو (با صدا)",
        'audio_format' => '🔉 فقط صدا',
        'download_link' => 'لینک دانلود',
        'telegram_file' => 'فایل تلگرام',
    ]
];
