<?php

return [
    'results' => [
        'waiting' => '🕧 منتظر ارسال وجه توسط مشتری.',
        'confirming' => '⏳ تراکنش در بلاک چین در حال پردازش است.',
        'confirmed' => '🔘 فرآیند توسط بلاک چین تایید می شود.',
        'sending' => "🔁 وجه به کیف پول شخصی مشتری ارسال می شود.",
        'partially_paid' => "⚠️ مبلغ پرداختی کمتر از مبلغ سفارش بوده است.

🔸 کد رهگیری: :tracking_code
🔸هزینه: `:paid_amount`
🔸 پرداخت شده واقعی: `:actually_paid`

لطفا با پشتیبانی تماس بگیرید",
        'finished' => "✅ پرداخت با موفقیت انجام شد.

🔸 کد رهگیری: :tracking_code
🔸هزینه: :paid_amount
🔸 اشتراک: :subscription
🔸 اندازه فایل مجاز: :max_file_size
🔸 حجم مصرف روزانه: :daly_volume
        ",
        'failed' => "❌ پرداخت به دلیل بروز خطا انجام نشد، لطفا دوباره امتحان کنید.",
    ],
    'payment' => [
        'paid' => 'پرداخت شد',
        'success_payment_creation' => "
        ✅ پرداخت با موفقیت ایجاد شد.

🔸 کد رهگیری: :tracking_code
🔸هزینه: `:amount`
🔸 آدرس: :address
        ",
        'error_payment_creation' => '❌ خطا در ایجاد پرداخت.',
        'try_again' => 'دوباره امتحان کنید'
    ],
    'invoice' => [
        'invoice' => 'صورتحساب',
        'date' => 'تاریخ',
        'customer' => 'مشتری',
        'phone' => 'تلفن',
        'total' => 'جمع',
        'subscription' => 'اشتراک',
        'currency' => 'واحد پول',
        'amount' => 'هزینه',
    ],
    'messages' => [
        'creating' => "در حال پردازش سفارش شما هستیم..."
    ]
];
