<?php

return [
    'title' => '🌟 خرید اشتراک ویژه',
    'size_allowed' => '📁 اندازه فایل مجاز: :size_allowed',
    'daily_volume' => '📅 حجم مجاز روزانه: :daily_volume',
    'price' => '💸 مبلغ: :price',
    'alert' => '🚫🚫 هیچ VPN، پروکسی یا فیلترینگ در ربات لینکگرام ارائه نشده است. لطفا برای این منظور اشتراک نخرید. 🚫🚫',
    'buy' => '💲خرید اشتراک :title',
    'limits' => [
        'convert_file' => 'تبدیل فایل ها',
        'upload_size' => 'افزایش حجم آپلود',
        'daly_size' => 'افزایش حجم آپلود روزانه',
    ],
];
