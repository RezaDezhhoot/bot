<?php

return [
    'results' => [
        'waiting' => '🕧 In attesa che il cliente invii il pagamento.',
        'confirming' => '⏳ La transazione è in fase di elaborazione sulla blockchain.',
        'confirmed' => '🔘 Il processo è confermato dalla blockchain.',
        'sending' => "🔁 I fondi vengono inviati al portafoglio personale del cliente.",
        'partially_paid' => "⚠️ L'importo pagato era inferiore all'importo dell'ordine.

🔸 Codice di monitoraggio: :tracking_code
🔸 Importo: `:paid_amount`
🔸 Effettivamente pagato: `:actually_paid`

si prega di contattare l'assistenza.",
        'finished' => "✅ Il pagamento è andato a buon fine.

🔸 Codice di monitoraggio: :tracking_code
🔸 Importo: :paid_amount
🔸 Abbonamento: :subscription
🔸 Dimensione file consentita: :max_file_size
🔸 Volume giornaliero utilizzato: :daly_volume
        ",
        'failed' => "❌ Il pagamento non è stato completato a causa di un errore di qualche tipo, riprova.",
    ],
    'payment' => [
        'paid' => 'È stato pagato',
        'success_payment_creation' => "
        ✅ Pagamento creato con successo.

🔸 Codice di monitoraggio: :tracking_code
🔸 Importo: `:amount`
🔸 Indirizzo: :address
        ",
        'error_payment_creation' => '❌ Errore durante la creazione del pagamento.',
        'try_again' => 'Riprova'
    ],
    'invoice' => [
        'invoice' => 'Fattura',
        'date' => 'Data',
        'customer' => 'Cliente',
        'phone' => 'Telefono',
        'total' => 'Totale',
        'subscription' => 'Sottoscrizione',
        'currency' => 'Valuta',
        'amount' => 'Quantità',
    ],
    'messages' => [
        'creating' => "Stiamo elaborando il tuo ordine..."
    ]
];
