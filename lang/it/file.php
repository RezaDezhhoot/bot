<?php

return [
    'types' => [
        'file' => 'File',
        'media' => 'Media',
        'link' => 'Collegamento',
        'youtube' => 'Youtube'
    ],
    'link' => [
        'get_download_link' => '🔗Ottieni il link per il download',
        'file_not_found' => '❌File non trovato',
        'download_link_generated' => "
        ✅Il tuo link per il download del file è stato creato con successo

Collegamento: :link
        "
    ],
    'actions' => [
        'change_name' => '✏️Rinomina file',
        'change_title' => '🎵Cambia titolo audio',
        'change_performer' => '🗣Cambia esecutore',
        'change_thumbnail' => '🌇Cambia miniatura',
        'get_thumbnail' => '📥 ottieni la miniatura',
        'add_soft_sub' => '💬Aggiungi sottotitoli (SoftSub)',
        'change_soft_sub' => '💬Cambia sottotitolo',
        'auto_convert_false' => '⬜️Rileva automaticamente il tipo di file (video, musica,...)',
        'auto_convert_ok' => '✅Rileva automaticamente il tipo di file (video, musica,...)',
        'apply_changes' => '♻️Applica le modifiche e ottieni un nuovo file',
        'get_as_document_false' => '⬜️Ottieni come documento Telegram',
        'get_as_document_ok' => '✅Ottieni come documento Telegram',
        'start_download_link' => '⚡️Avvia e ottieni il file Telegram⚡️'
    ],
    'messages' => [
        'process_error' => "❌️ L'operazione di elaborazione multimediale ha riscontrato un errore",
        'change_name_message' => "
        •
💾 Nome attuale: :name

📝 Inserisci il nuovo nome file con estensione:

📂 Esempio: 123.mp4 o 123.mp3 e...

⛔️ Il nome del file non deve superare i 60 caratteri.
⚠️ Caratteri non consentiti: / \ : ؟ \" > < | # ~ se il tuo nome contiene questi caratteri, verranno rimossi.
•
        ",
        'filename_changed' => "
        ✅ Nome file modificato con successo.
•
        ",
        'change_audio_title' => "
        🎵 Nome del titolo audio attuale:
:title
✏️ Si prega di inviare il nuovo nome del titolo in risposta a questo messaggio:

•
        ",
        'audio_title_changed' => "
        ✅ Titolo audio modificato con successo.
•
        ",
        'change_audio_performer' => "
        🗣 Nome del performer attuale:
:performer
✏️ Si prega di inviare il nome del nuovo Attore in risposta a questo messaggio:

•
        ",
        'audio_performer_changed' => "
        ✅ Audio Performer cambiato con successo.
•
        ",
        'change_thumbnail' => "
        🌆 Invia la tua nuova immagine in miniatura in risposta a questo messaggio:

❔ Questa immagine, nota come Miniatura, viene posizionata all'inizio del video, della musica o del file Telegram.

🖼 Inoltre, se vuoi impostare un'immagine come miniatura predefinita e usarla la prossima volta, premi su /default_thumbnail.
▫️
        ",
        'thumbnail_changed' => "
        ✅ Miniatura modificata con successo.
▫️
        ",
        'thumbnail_doesnt_exist' => '📁 Nessuna immagine impostata per la miniatura',
        'sent_subtitle_file' => "
        💬 Si prega di inviare il sottotitolo SRT che si desidera incorporare nel video in risposta a questo messaggio.
▫️
        ",
        'subtitle_added' => "
        ✅ Sottotitolo aggiunto con successo.
        "
    ],
    'thumbnail' => [
        'default' => [
            'title' => "
            🖼 Dal menu in basso è possibile apportare modifiche alla miniatura predefinita.

❔ Questa immagine, nota come Miniatura, viene posizionata all'inizio del video, della musica o del file Telegram.
▫️
            ",
            'btn' => [
                'get_current_default_thumbnail' => '🖼Ottieni la miniatura predefinita corrente',
                'change_default_thumbnail' => '⚙️Cambia la miniatura predefinita',
                'delete_default_thumbnail' => '🗑Elimina la miniatura predefinita',
            ],
            'doesnt_exist' => '📁 Nessuna immagine impostata per la miniatura predefinita',
            'update' => '🖼 Invia la tua immagine di anteprima predefinita in risposta a questo messaggio.
🔙Oppure continua /start per cancellare.
▫️',
            'changed' => '☑️ La miniatura predefinita è stata modificata correttamente.
▫️',
            'deleted' =>  '☑️ La miniatura predefinita è stata eliminata correttamente.'
        ]
    ],
    'photo' => [
        'result' => "
        🗃 Tipo: Foto 🎥
📥 Dimensioni: :size
↔️ Risoluzione: :resolution
▫️
        "
    ],
    'audio' => [
        'result' => "
        🗃 Tipo: audio 🎵
📥 Dimensioni: :size
🎵 Titolo audio:
:title
🗣 Esecutore:
:performer
⏱ Durata: :duration
🌇 Ha una miniatura? :thumbnail
📄 File Name: :name

•
        "
    ],
    'document' => [
        'result' => "
        🗃 Type: Document 📁
📥 Dimensioni: :size
🌇 Ha una miniatura? :thumbnail
📄 Nome file: :name

•
        "
    ],
    'video' => [
        'result' => "
        🗃 Type: Video 🎥
📥 Dimensioni: :size
🌇 Has thumbnail? :thumbnail
↔️ Risoluzione: :resolution
⏱ Durata: :duration
📄 Nome file: :name

•
        "
    ],
    'url' => [
        'result1' => "
        💾 Nome file: :name

📥 Dimensioni: :size

⏳ Stato: In attesa della tua conferma...
•
        ",
        'result2' => "
        💾 Nome file: :name

📥 Dimensioni: :size

⏳ Stato: Download sul server...

⚡️ :progress
•
        ",
        'result3' => "
        💾 Nome file: :name

📥 Dimensioni: :size

⏳ Stato: Caricamento su Telegram (aspetta un po')
•
        ",
    ]
];
