<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 Nome file: `:name`

❔ Seleziona il tipo di file: 👇
•
    ",
        'select_quality' => "
        •
💾 Nome file: `:name`
📽 Tipo di file: `:type`

❔ Seleziona la qualità: 👇
•
        ",
        'select_type' => "
        •
💾 Nome file: `:name`
📽 Tipo di file: `:type`
💿 Qualità: `:quality`
📥 Dimensione del file: `:size`

❔ Seleziona il tipo di download: 👇
•
        ",
        'final_result' => "
        •
💾 Nome file: `:name`
📽 Tipo di file: `:type`
💿 Qualità: `:quality`
📥 Dimensione del file: `:size`
❔ Tipo di download: `:download_type`

⏳ Stato: In attesa della tua conferma...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 Video (con audio)",
        'audio_format' => '🔉 Solo suono',
        'download_link' => 'Link per scaricare',
        'telegram_file' => 'Fascicolo telegramma',
    ]
];
