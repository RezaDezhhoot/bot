<?php

return [
    'title' => '🌟 Acquista un abbonamento speciale',
    'size_allowed' => '📁 Dimensione del file consentita: :size_allowed',
    'daily_volume' => '📅 Volume giornaliero consentito: :daily_volume',
    'price' => '💸 Importo: :price',
    'alert' => '🚫🚫 Nessuna VPN, proxy o filtro è fornito nel bot di TeleFileGram. Si prega di non acquistare un abbonamento per questo scopo. 🚫🚫',
    'buy' => '💲Acquista :title sottoscrizione',
    'limits' => [
        'convert_file' => 'convertire i file',
        'upload_size' => 'aumentare il volume di caricamento',
        'daly_size' => 'aumentare la dimensione del caricamento giornaliero',
    ],
];
