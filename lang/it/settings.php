<?php

return [
    'btn' => [
        'change_language' => 'Cambia lingua / تغییر زبان',
        'default_thumbnail' => 'Miniatura predefinita 🖼',
        'change_thumbnail_size_ok' => '✅Ridimensiona la miniatura in base alle dimensioni del video↔',
        'change_thumbnail_size_false' => '⬜️Ridimensiona la miniatura in base alle dimensioni del video↔',
    ],
];
