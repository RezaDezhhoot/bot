<?php

return [
    'processStarted' => '⏳Stiamo preparando il tuo file...',
    'words' => [
        'yes' => 'SÌ',
        'no' => 'No'
    ],
    'lang' => [
        'select' => '🇮🇹 Si prega di selezionare la lingua preferita'
    ],
    'btn' => [
        'back' => 'Indietro↩️',
    ],
    'commands' => [
        'start' => "
            📤 Se mi mandi il link diretto di un file, posso inviarlo come file telegram.

📥 E viceversa, se mi mandi un file telegram, posso inviarti il link diretto in modo che tu possa scaricarlo con programmi come IDM o ADM ad alta velocità.

💥 Oppure per scaricare da YouTube, inviami il link del video di YouTube.
❔ Secondo il seguente comando, puoi cercare su YouTube:
🔍 Ad esempio, digita pitbull per cercare video

`@vid pitbull`

•
        ",
        'usage' => "
            📄 Dimensione file consentita: :max_file_size

📂 Volume giornaliero utilizzato: :daly_volume

⏳ Volume residuo giornaliero: :remaining_volume

-------
Se desideri caricare o scaricare file fino a 3,91 GB o aumentare il tuo limite giornaliero a 60,00 GB, tocco /upgrade 🌹
        ",
        "settings" => "
        ⚙️ Impostazioni:

Cosa vuoi cambiare? ☺️

Seleziona dal menu in basso: 👇
•
        "
    ],
];
