<?php

return [
    'btn' => [
        'change_language' => 'Изменение языка / تغییر زبان',
        'default_thumbnail' => 'Миниатюра по умолчанию 🖼',
        'change_thumbnail_size_ok' => '✅Измените размер миниатюры до размера видео↔',
        'change_thumbnail_size_false' => '⬜️Измените размер миниатюры до размера видео↔',
    ],
];
