<?php

return [
    'processStarted' => '⏳Мы готовим ваш файл...',
    'words' => [
        'yes' => 'Да',
        'no' => 'Нет'
    ],
    'lang' => [
        'select' => '🇷🇺 Пожалуйста, выберите предпочитаемый язык'
    ],
    'btn' => [
        'back' => 'Назад↩️',
    ],
    'commands' => [
        'start' => "
            📤 Если вы пришлете мне прямую ссылку на файл, я могу отправить его в виде телеграм-файла.

📥 И наоборот, если вы пришлете мне телеграм-файл, я могу выслать вам прямую ссылку, чтобы вы могли скачать его такими программами, как IDM или ADM на высокой скорости.

💥 Или чтобы скачать с YouTube, пришлите мне ссылку на видео с YouTube.
❔ В соответствии со следующей командой вы можете искать на YouTube:
🔍 Например, введите pitbull для поиска видео

`@vid pitbull`

•
        ",
        'usage' => "
            📄 Допустимый размер файла: :max_file_size

📂 Ежедневный объем: :daly_volume

⏳ Дневной остаточный объем: :remaining_volume

-------
Если вы хотите загружать или скачивать файлы размером до 3,91 ГБ или увеличить дневной лимит до 60,00 ГБ, нажмите /upgrade 🌹
        ",
        "settings" => "
        ⚙️ Настройки:

Что ты хочешь изменить? ☺️

Выберите в нижнем меню: 👇
•
        "
    ],
];
