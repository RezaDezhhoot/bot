<?php

return [
    'results' => [
        'waiting' => '🕧 Ожидание отправки платежа клиентом.',
        'confirming' => '⏳ Транзакция обрабатывается в блокчейне.',
        'confirmed' => '🔘 Процесс подтвержден блокчейном.',
        'sending' => "🔁 Средства отправляются на личный кошелек клиента.",
        'partially_paid' => "⚠️ Оплаченная сумма меньше суммы заказа.

🔸Код отслеживания: :tracking_code
🔸 Количество: `:paid_amount`
🔸 Реально оплачено: `:actually_paid`

пожалуйста, свяжитесь со службой поддержки.",
        'finished' => "✅ Оплата прошла успешно.

🔸 Код отслеживания: :tracking_code
🔸 Сумма: :paid_amount
🔸 Подписка: :subscription
🔸 Допустимый размер файла: :max_file_size
🔸 Ежедневный объем использования: :daly_volume
        ",
        'failed' => "❌ Платеж не был завершен из-за какой-то ошибки. Пожалуйста, попробуйте еще раз.",
    ],
    'payment' => [
        'paid' => 'Было оплачено',
        'success_payment_creation' => "
        ✅ Платеж успешно создан.

🔸 Код отслеживания: :tracking_code
🔸 Количество: `:amount`
🔸 Адрес: г. :address
        ",
        'error_payment_creation' => '❌ Ошибка создания платежа.',
        'try_again' => 'Попробуйте еще раз'
    ],
    'invoice' => [
        'invoice' => 'Счет',
        'date' => 'Дата',
        'customer' => 'Клиент',
        'phone' => 'Телефон',
        'total' => 'Общий',
        'subscription' => 'Подписка',
        'currency' => 'Валюта',
        'amount' => 'Количество',
    ],
    'messages' => [
        'creating' => "Мы обрабатываем ваш заказ..."
    ]
];
