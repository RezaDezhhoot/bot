<?php

return [
    'results' => [
        'waiting' => '🕧 Esperando que el cliente envíe el pago.',
        'confirming' => '⏳ La transacción se está procesando en la cadena de bloques.',
        'confirmed' => '🔘 El proceso es confirmado por la cadena de bloques.',
        'sending' => "🔁 Los fondos se envían a la billetera personal del cliente.",
        'partially_paid' => "⚠️ El importe pagado fue inferior al importe del pedido.

🔸 Código de seguimiento: :tracking_code
🔸 Importe: `:paid_amount`
🔸 Realmente pagado: `:actually_paid`

por favor, póngase en contacto con el soporte.",
        'finished' => "✅ El pago fue exitoso.

🔸 Código de seguimiento: :tracking_code
🔸 Importe: :paid_amount
🔸 Suscripción: :subscription
🔸 Tamaño de archivo permitido: :max_file_size
🔸 Volumen diario utilizado: :daly_volume
        ",
        'failed' => "❌ El pago no se completó debido a algún tipo de error, intente nuevamente.",
    ],
    'payment' => [
        'paid' => 'Fue pagado',
        'success_payment_creation' => "
        ✅ Pago creado con éxito.

🔸 Código de seguimiento: :tracking_code
🔸 Importe: `:amount`
🔸 Dirección: :address
        ",
        'error_payment_creation' => '❌ Error al crear el pago.',
        'try_again' => 'Intentar otra vez'
    ],
    'invoice' => [
        'invoice' => 'Factura',
        'date' => 'Fecha',
        'customer' => 'Cliente',
        'phone' => 'Teléfono',
        'total' => 'Total',
        'subscription' => 'Suscripción',
        'currency' => 'Divisa',
        'amount' => 'Cantidad',
    ],
    'messages' => [
        'creating' => "Estamos procesando tu pedido..."
    ]
];
