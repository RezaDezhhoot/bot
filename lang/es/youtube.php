<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 Nombre del archivo: `:name`

❔ Seleccione el tipo de archivo: 👇
•
    ",
        'select_quality' => "
        •
💾 Nombre del archivo: `:name`
📽 Tipo de archivo: `:type`

❔ Seleccione la calidad: 👇
•
        ",
        'select_type' => "
        •
💾 Nombre del archivo: `:name`
📽 Tipo de archivo: `:type`
💿 Calidad: `:quality`
📥 Tamaño del archivo: `:size`

❔ Seleccione el tipo de descarga: 👇
•
        ",
        'final_result' => "
        •
💾 Nombre del archivo: `:name`
📽 Tipo de archivo: `:type`
💿 Calidad: `:quality`
📥 Tamaño del archivo: `:size`
❔ Tipo de descarga: `:download_type`

⏳ Estado: Esperando su confirmación...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 Vídeo (con sonido)",
        'audio_format' => '🔉 solo sonido',
        'download_link' => 'Enlace de descarga',
        'telegram_file' => 'archivo de telegrama',
    ]
];
