<?php

return [
    'types' => [
        'file' => 'Archivo',
        'media' => 'Medios de comunicación',
        'link' => 'Enlace',
        'youtube' => 'Youtube'
    ],
    'link' => [
        'get_download_link' => '🔗Obtener enlace de descarga',
        'file_not_found' => '❌ Archivo no encontrado',
        'download_link_generated' => "
        ✅Su enlace de descarga de archivos se ha creado con éxito

Enlace: :link
        "
    ],
    'actions' => [
        'change_name' => '✏️Renombrar archivo',
        'change_title' => '🎵Cambiar título de audio',
        'change_performer' => '🗣Cambiar de intérprete',
        'change_thumbnail' => '🌇Cambiar miniatura',
        'get_thumbnail' => '📥obtener miniatura',
        'add_soft_sub' => '💬Añadir subtítulos (SoftSub)',
        'change_soft_sub' => '💬Cambiar subtítulo',
        'auto_convert_false' => '⬜️Detección automática del tipo de archivo (video, música,...)',
        'auto_convert_ok' => '✅Detección automática del tipo de archivo (video, música,...)',
        'apply_changes' => '♻️Aplicar cambios y obtener un nuevo archivo',
        'get_as_document_false' => '⬜️Obtener como documento de Telegram',
        'get_as_document_ok' => '✅Obtener como documento de Telegram',
        'start_download_link' => '⚡️Comience y obtenga el archivo de Telegram⚡️'
    ],
    'messages' => [
        'process_error' => "❌️ La operación de procesamiento de medios encontró un error",
        'change_name_message' => "
        •
💾 Nombre actual: :name

📝 Ingrese el nuevo nombre de archivo con extensión:

📂 Ejemplo: 123.mp4 o 123.mp3 y...

⛔️ El nombre del archivo debe tener 60 caracteres o menos.
⚠️ Caracteres no permitidos: / \ : ؟ \" > < | # ~ si su nombre contiene estos caracteres, serán eliminados.
•
        ",
        'filename_changed' => "
        ✅ Nombre de archivo cambiado con éxito.
•
        ",
        'change_audio_title' => "
        🎵 Nombre del título de audio actual:
:title
✏️ Envíe su nuevo nombre de título en respuesta a este mensaje:

•
        ",
        'audio_title_changed' => "
        ✅ Título de audio cambiado con éxito.
•
        ",
        'change_audio_performer' => "
        🗣 Nombre del artista actual:
:performer
✏️ Envíe su nuevo nombre de Artista en respuesta a este mensaje:

•
        ",
        'audio_performer_changed' => "
        ✅ Audio Performer cambiado con éxito.
•
        ",
        'change_thumbnail' => "
        🌆 Envíe su nueva imagen en miniatura en respuesta a este mensaje:

❔ Esta imagen, que se conoce como Miniatura, se coloca al principio del archivo de video, música o Telegram.

🖼 Además, si desea establecer una imagen como miniatura predeterminada y usarla la próxima vez, presione /default_thumbnail.
▫️
        ",
        'thumbnail_changed' => "
        ✅ Miniatura cambiada con éxito.
▫️
        ",
        'thumbnail_doesnt_exist' => '📁 No hay imagen establecida para Miniatura',
        'sent_subtitle_file' => "
        💬 Envíe su subtítulo SRT que desea incrustar en el video en respuesta a este mensaje.
▫️
        ",
        'subtitle_added' => "
        ✅ Subtítulo agregado con éxito.
        "
    ],
    'thumbnail' => [
        'default' => [
            'title' => "
            🖼 Desde el menú inferior, puede realizar cambios en la miniatura predeterminada.

❔ Esta imagen, que se conoce como Miniatura, se coloca al principio del archivo de video, música o Telegram.
▫️
            ",
            'btn' => [
                'get_current_default_thumbnail' => '🖼Obtener la miniatura predeterminada actual',
                'change_default_thumbnail' => '⚙️Cambiar miniatura predeterminada',
                'delete_default_thumbnail' => '🗑Eliminar miniatura predeterminada',
            ],
            'doesnt_exist' => '📁 No hay imagen configurada como miniatura predeterminada',
            'update' => '🖼 Envíe su imagen en miniatura predeterminada en respuesta a este mensaje.
🔙 O presiona /comenzar para cancelar.
▫️',
            'changed' => '☑️ La miniatura predeterminada se cambió con éxito.
▫️',
            'deleted' =>  '☑️ Miniatura predeterminada eliminada con éxito.'
        ]
    ],
    'photo' => [
        'result' => "
        🗃 Tipo: Foto 🎥
📥 Tamaño: :size
↔️ Resolución: :resolution
▫️
        "
    ],
    'audio' => [
        'result' => "
        🗃 Tipo: Audio 🎵
📥 Tamaño: :size
🎵 Título de audio:
:title
🗣 Intérprete:
:performer
⏱ Duración: :duration
🌇 Tiene miniatura? :thumbnail
📄 Nombre del archivo: :name

•
        "
    ],
    'document' => [
        'result' => "
        🗃 Tipo: Documento 📁
📥 Tamaño: :size
🌇 Tiene miniatura? :thumbnail
📄 Nombre del archivo: :name

•
        "
    ],
    'video' => [
        'result' => "
        🗃 Tipo: Vídeo 🎥
📥 Tamaño: :size
🌇 Tiene miniatura? :thumbnail
↔️ Resolución: :resolution
⏱ Duración: :duration
📄 Nombre del archivo: :name

•
        "
    ],
    'url' => [
        'result1' => "
        💾 Nombre del archivo: :name

📥 Tamaño del archivo: :size

⏳ Estado: Esperando su confirmación...
•
        ",
        'result2' => "
        💾 Nombre del archivo: :name

📥 Tamaño del archivo: :size

⏳ Estado: Descargando al servidor...

⚡️ :progress
•
        ",
        'result3' => "
        💾 Nombre del archivo: :name

📥 Tamaño del archivo: :size

⏳ Estado: Subiendo a Telegram (por favor espere un poco)
•
        "
    ]
];
