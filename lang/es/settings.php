<?php

return [
    'btn' => [
        'change_language' => 'Cambiar idioma / تغییر زبان',
        'default_thumbnail' => 'Miniatura predeterminada 🖼',
        'change_thumbnail_size_ok' => '✅Cambiar el tamaño de la miniatura al tamaño del video↔',
        'change_thumbnail_size_false' => '⬜️Cambiar el tamaño de la miniatura al tamaño del video↔',
    ],
];
