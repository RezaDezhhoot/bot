<?php

return [
    'title' => '🌟 Compra una suscripción especial',
    'size_allowed' => '📁 Tamaño de archivo permitido: :size_allowed',
    'daily_volume' => '📅 Volumen diario permitido: :daily_volume',
    'price' => '💸 Monto: :price',
    'alert' => '🚫🚫 No se proporciona VPN, proxy ni filtrado en el bot de TeleFileGram. Por favor, no compre una suscripción para este propósito. 🚫🚫',
    'buy' => '💲Comprar :title suscripción ',
    'limits' => [
        'convert_file' => 'convertir archivos',
        'upload_size' => 'aumentar el volumen de carga',
        'daly_size' => 'aumentar el tamaño de carga diario',
    ],
];
