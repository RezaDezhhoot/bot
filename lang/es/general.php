<?php

return [
    'processStarted' => '⏳Estamos preparando tu expediente...',
    'words' => [
        'yes' => 'Sí',
        'no' => 'No'
    ],
    'lang' => [
        'select' => '🇪🇸 Seleccione su idioma preferido'
    ],
    'btn' => [
        'back' => 'Volver↩️',
    ],
    'commands' => [
        'start' => "
            📤 Si me envías el enlace directo de un archivo, puedo enviarlo como un archivo de Telegram.

📥 Y viceversa, si me envías un archivo de Telegram, puedo enviarte el enlace directo para que lo descargues con programas como IDM o ADM a alta velocidad.

💥 O para descargar de YouTube, envíame el enlace del video de YouTube.
❔ De acuerdo con el siguiente comando, puede buscar en YouTube:
🔍 Por ejemplo, escriba pitbull para buscar videos

`@vid pitbull`

•
        ",
        'usage' => "
            📄 Tamaño de archivo permitido: :max_file_size

📂 Volumen diario utilizado: :daly_volume

⏳ Volumen restante diario: :remaining_volume

-------
Si desea cargar o descargar archivos de hasta 3,91 GB o aumentar su límite diario a 60,00 GB, toque /upgrade 🌹
        ",
        "settings" => "
        ⚙️ Configuración:

¿Qué quieres cambiar? ☺️

Seleccione del menú inferior: 👇
•
        "
    ],
];
