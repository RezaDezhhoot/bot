<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 Dosya adı: `:name`

❔ Lütfen dosya türünü seçin: 👇
•
    ",
        'select_quality' => "
        •
💾 Dosya adı: `:name`
📽 Dosya türü: `:type`

❔ Lütfen kaliteyi seçin: 👇
•
        ",
        'select_type' => "
        •
💾 Dosya adı: `:name`
📽 Dosya türü: `:type`
💿 Kalite: `:quality`
📥 Dosya boyutu: `:size`

❔ Lütfen indirme türünü seçin: 👇
•
        ",
        'final_result' => "
        •
💾 Dosya adı: `:name`
📽 Dosya türü: `:type`
💿 Kalite: `:quality`
📥 Dosya boyutu: `:size`
❔ İndirme türü: `:download_type`

⏳ Durum: Onayınız bekleniyor...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 Video(sesli)",
        'audio_format' => '🔉 sadece ses',
        'download_link' => 'Bağlantılar İndir',
        'telegram_file' => 'telgraf dosyası',
    ]
];
