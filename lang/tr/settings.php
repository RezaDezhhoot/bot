<?php

return [
    'btn' => [
        'change_language' => 'Dili değiştir / تغییر زبان',
        'default_thumbnail' => 'Varsayılan Küçük Resim 🖼',
        'change_thumbnail_size_ok' => '✅Küçük resmi videonun boyutuna göre yeniden boyutlandırın↔',
        'change_thumbnail_size_false' => '⬜️Küçük resmi videonun boyutuna göre yeniden boyutlandırın↔',
    ],
];
