<?php

return [
    'types' => [
        'file' => 'Dosya',
        'media' => 'medya',
        'link' => 'Bağlantı',
        'youtube' => 'Youtube'
    ],
    'link' => [
        'get_download_link' => '🔗İndirme bağlantısını al',
        'file_not_found' => '❌ Dosya bulunamadı',
        'download_link_generated' => "
        ✅Dosya indirme bağlantınız başarıyla oluşturuldu

Bağlantı: :link
        "
    ],
    'actions' => [
        'change_name' => '✏️Dosyayı yeniden adlandır',
        'change_title' => '🎵Ses başlığını değiştir',
        'change_performer' => '🗣Değişim Sanatçısı',
        'change_thumbnail' => '🌇Küçük resmi değiştir',
        'get_thumbnail' => '📥küçük resim al',
        'add_soft_sub' => '💬Alt yazı ekle (SoftSub)',
        'change_soft_sub' => '💬Altyazıyı değiştir',
        'auto_convert_false' => '⬜️Dosya türünü otomatik algıla(video,müzik,...)',
        'auto_convert_ok' => '✅Dosya türünü otomatik algıla(video,müzik,...)',
        'apply_changes' => '♻️Değişiklikleri uygula ve yeni dosya al',
        'get_as_document_false' => '⬜️Telegram Belgesi Olarak Alın',
        'get_as_document_ok' => '✅Telegram Belgesi Olarak Alın',
        'start_download_link' => '⚡️Telegram dosyasını başlatın ve alın⚡️'
    ],
    'messages' => [
        'process_error' => "❌️ Medya işleme işlemi bir hatayla karşılaştı",
        'change_name_message' => "
        •
💾 Mevcut adı: :name

📝 Lütfen uzantılı yeni dosya adını girin:

📂 Örnek: 123.mp4 veya 123.mp3 ve...

⛔️ Dosya adı 60 karakter veya daha az olmalıdır.
⚠️ İzin verilmeyen karakterler: / \ : ؟ \" > < | # ~ adınız bu karakterleri içeriyorsa, bunlar kaldırılacaktır.
•
        ",
        'filename_changed' => "
        ✅ Dosya adı başarıyla değiştirildi.
•
        ",
        'change_audio_title' => "
        🎵 Geçerli Ses Başlığı Adı:
:title
✏️ Lütfen bu mesaja yanıt olarak yeni Başlık adınızı gönderin:

•
        ",
        'audio_title_changed' => "
        ✅ Ses Başlığı başarıyla değiştirildi.
•
        ",
        'change_audio_performer' => "
        🗣 Mevcut Oyuncu adı:
:performer
✏️ Lütfen bu mesaja yanıt olarak yeni Sanatçı adınızı gönderin:

•
        ",
        'audio_performer_changed' => "
        ✅ Audio Performer changed successfully.
•
        ",
        'change_thumbnail' => "
        🌆 Lütfen bu mesaja yanıt olarak yeni küçük resminizi gönderin:

❔ Küçük resim olarak bilinen bu resim video, müzik veya Telegram dosyasının başına yerleştirilir.

🖼 Ayrıca, bir görüntüyü varsayılan Küçük Resim olarak ayarlamak ve bir dahaki sefere kullanmak istiyorsanız, düğmesine basın /default_thumbnail.
▫️
        ",
        'thumbnail_changed' => "
        ✅ Küçük resim başarıyla değiştirildi.
▫️
        ",
        'thumbnail_doesnt_exist' => '📁 Küçük resim için ayarlanmış görüntü yok',
        'sent_subtitle_file' => "
        💬 Lütfen videoya eklenmesini istediğiniz SRT altyazınızı bu mesaja yanıt olarak gönderiniz.
▫️
        ",
        'subtitle_added' => "
        ✅ Altyazı başarıyla eklendi.
        "
    ],
    'thumbnail' => [
        'default' => [
            'title' => "
            🖼 Alt menüden varsayılan Küçük Resimde değişiklik yapabilirsiniz.

❔ Küçük resim olarak bilinen bu resim video, müzik veya Telegram dosyasının başına yerleştirilir.
▫️
            ",
            'btn' => [
                'get_current_default_thumbnail' => '🖼Mevcut varsayılan küçük resmi alın',
                'change_default_thumbnail' => '⚙️Varsayılan küçük resmi değiştir',
                'delete_default_thumbnail' => '🗑Varsayılan küçük resmi sil',
            ],
            'doesnt_exist' => '📁 Varsayılan Küçük Resim için ayarlanmış görüntü yok',
            'update' => '🖼 Lütfen bu mesaja yanıt olarak varsayılan küçük resminizi gönderin.
🔙 Veya üzerine basın /start iptal etmek.
▫️',
            'changed' => '☑️ Varsayılan küçük resim başarıyla değiştirildi.
▫️',
            'deleted' =>  '☑️ Varsayılan küçük resim başarıyla silindi.'
        ]
    ],
    'photo' => [
        'result' => "
        🗃 Tür: Fotoğraf 🎥
📥 Boyut: :size
↔️ Çözünürlük: :resolution
▫️
        "
    ],
    'audio' => [
        'result' => "
        🗃 Tür: Ses 🎵
📥 Boyut: :size
🎵 Ses Başlığı:
:title
🗣 Oyuncu:
:performer
⏱ Süre: :duration
🌇 Küçük resmi var mı? :thumbnail
📄 Dosya Adı: :name

•
        "
    ],
    'document' => [
        'result' => "
        🗃 Tür: Belge 📁
📥 Boyut: :size
🌇 Has thumbnail? :thumbnail
📄 Dosya Adı: :name

•
        "
    ],
    'video' => [
        'result' => "
        🗃 Tür: Video 🎥
📥 Boyut: :size
🌇 Küçük resmi var mı? :thumbnail
↔️ Çözünürlük: :resolution
⏱ Süre: :duration
📄 Dosya Adı: :name

•
        "
    ],
    'url' => [
        'result1' => "
        💾 Dosya adı: :name

📥 Dosya boyutu: :size

⏳ Durum: Onayınız bekleniyor...
•
        ",
        'result2' => "
        💾 Dosya adı: :name

📥 Dosya boyutu: :size

⏳ Durum: Sunucuya yükleniyor...

⚡️ :progress
•
        ",
        'result1' => "
        💾 Dosya adı: :name

📥 Dosya boyutu: :size

⏳ Durum: Telegram'a yükleniyor (lütfen biraz bekleyin)
•
        ",
    ]
];
