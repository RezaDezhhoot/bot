<?php

return [
    'processStarted' => '⏳Dosyanızı hazırlıyoruz...',
    'words' => [
        'yes' => 'Evet',
        'no' => 'HAYIR'
    ],
    'lang' => [
        'select' => '🇹🇷 Lütfen tercih ettiğiniz dili seçin'
    ],
    'btn' => [
        'back' => 'geri↩️',
    ],
    'commands' => [
        'start' => "
            📤 Bana bir dosyanın direkt linkini gönderirseniz, onu telegram dosyası olarak gönderebilirim.

📥 Ve tam tersi, bana bir telegram dosyası gönderirseniz, IDM veya ADM gibi programlarla yüksek hızda indirebilmeniz için size doğrudan bağlantıyı gönderebilirim.

💥 Veya YouTube'dan indirmek için YouTube video bağlantısını bana gönderin.
❔ Aşağıdaki komuta göre YouTube'da arama yapabilirsiniz:
🔍 Örneğin, video aramak için pitbull yazın

`@vid pitbull`

•
        ",
        'usage' => "
            📄 İzin verilen dosya boyutu: :max_file_size

📂 Günlük kullanılan hacim: :daly_volume

⏳ Günlük kalan hacim: :remaining_volume

-------
3,91 GB'a kadar dosya yüklemek veya indirmek veya günlük limitinizi 60,00 GB'a çıkarmak istiyorsanız, /upgrade 🌹 öğesine dokunun.
        ",
        "settings" => "
        ⚙️ Ayarlar:

Neyi değiştirmek istiyorsun? ☺️

Alt menüden seçin: 👇
•
        "
    ],
];
