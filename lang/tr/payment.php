<?php

return [
    'results' => [
        'waiting' => '🕧 Müşterinin ödemeyi göndermesi bekleniyor.',
        'confirming' => '⏳ İşlem blok zincirinde işleniyor.',
        'confirmed' => '🔘 İşlem blok zinciri tarafından onaylanır.',
        'sending' => "🔁 Para, müşterinin kişisel cüzdanına gönderilir.",
        'partially_paid' => "⚠️ Ödenen tutar sipariş tutarından azdı.

🔸 İzleme kodu: :tracking_code
🔸 Amount: `:paid_amount`
🔸 Actually paid: `:actually_paid`

lütfen destekle iletişime geçin.",
        'finished' => "✅ Payment was successful.

🔸 İzleme kodu: :tracking_code
🔸 Tutar: :paid_amount
🔸 Abonelik: :subscription
🔸 İzin verilen dosya boyutu: :max_file_size
🔸 Günlük kullanılan hacim: :daly_volume
        ",
        'failed' => "❌ Ödeme bir tür hata nedeniyle tamamlanamadı, lütfen tekrar deneyin.",
    ],
    'payment' => [
        'paid' => 'Ödenmişti',
        'success_payment_creation' => "
        ✅ Ödeme başarıyla oluşturuldu.

🔸 İzleme kodu: :tracking_code
🔸 Tutar: `:amount`
🔸 Adres: :address
        ",
        'error_payment_creation' => '❌ Ödeme oluşturulurken hata oluştu.',
        'try_again' => 'Tekrar deneyin'
    ],
    'invoice' => [
        'invoice' => 'Fatura',
        'date' => 'Tarih',
        'customer' => 'Müşteri',
        'phone' => 'Telefon',
        'total' => 'Toplam',
        'subscription' => 'abonelik',
        'currency' => 'Para birimi',
        'amount' => 'Miktar',
    ],
    'messages' => [
        'creating' => "Siparişinizi işliyoruz..."
    ]
];
