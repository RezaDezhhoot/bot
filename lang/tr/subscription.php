<?php

return [
    'title' => '🌟 Özel bir abonelik satın alın',
    'size_allowed' => '📁 İzin verilen dosya boyutu: :size_allowed',
    'daily_volume' => '📅 İzin verilen günlük hacim: :daily_volume',
    'price' => '💸 Miktar: :price',
    'alert' => '🚫🚫TeleFileGram botunda VPN, proxy veya filtreleme sağlanmamaktadır. Lütfen bu amaçla abonelik satın almayın. 🚫🚫',
    'buy' => '💲Satın al :title abonelik',
    'limits' => [
        'convert_file' => 'Dosyaları dönüştür',
        'upload_size' => 'yükleme hacmini artır',
        'daly_size' => 'günlük yükleme boyutunu artır',
    ],
];
