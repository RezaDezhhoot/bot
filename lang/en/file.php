<?php

return [
    'types' => [
        'file' => 'File',
        'media' => 'Media',
        'link' => 'Link',
        'youtube' => 'Youtube'
    ],
    'link' => [
        'get_download_link' => '🔗Get download link',
        'file_not_found' => '❌ File not found',
        'download_link_generated' => "
        ✅Your file download link has been created successfully

Link: :link
        "
    ],
    'actions' => [
        'change_name' => '✏️Rename file',
        'change_title' => '🎵Change audio title',
        'change_performer' => '🗣Change Performer',
        'change_thumbnail' => '🌇Change thumbnail',
        'get_thumbnail' => '📥get thumbnail',
        'add_soft_sub' => '💬Add subtitle (SoftSub)',
        'change_soft_sub' => '💬Change subtitle',
        'auto_convert_false' => '⬜️Auto detect file type(video,music,...)',
        'auto_convert_ok' => '✅Auto detect file type(video,music,...)',
        'apply_changes' => '♻️Apply changes and get new file',
        'get_as_document_false' => '⬜️Get as a Telegram Document',
        'get_as_document_ok' => '✅Get as a Telegram Document',
        'start_download_link' => '⚡️Start and get Telegram file⚡️'
    ],
    'messages' => [
        'process_error' => "❌️The media processing operation encountered an error",
        'change_name_message' => "
        •
💾 Current name: :name

📝 Please enter the new file name with extension:

📂 Example: 123.mp4 or 123.mp3 and...

⛔️ The file name must be 60 characters or less.
⚠️ Disallowed characters:  / \ :  ؟ \" > < | # ~  if your name contains these characters, they will be removed.
•
        ",
        'filename_changed' => "
        ✅ Filename changed successfully.
•
        ",
        'change_audio_title' => "
        🎵 Current Audio Title Name:
:title
✏️ Please send your new Title name in reply to this message:

•
        ",
        'audio_title_changed' => "
        ✅ Audio Title changed successfully.
•
        ",
        'change_audio_performer' => "
        🗣 Current Performer name:
:performer
✏️ Please send your new Performer name in reply to this message:

•
        ",
        'audio_performer_changed' => "
        ✅ Audio Performer changed successfully.
•
        ",
        'change_thumbnail' => "
        🌆 Please send your new thumbnail image in reply to this message:

❔ This image, which is known as Thumbnail, is placed at the beginning of the video, music or Telegram file.

🖼 Also, if you want to set an image as the default Thumbnail and use it next time, press on /default_thumbnail.
▫️
        ",
        'thumbnail_changed' => "
        ✅ Thumbnail successfully changed.
▫️
        ",
        'thumbnail_doesnt_exist' => '📁 No image set for Thumbnail',
        'sent_subtitle_file' => "
        💬 Please send your SRT subtitle that you want to be embedded in the video in reply to this message.
▫️
        ",
        'subtitle_added' => "
        ✅ Subtitle successfully added.
        "
    ],
    'thumbnail' => [
        'default' => [
            'title' => "
            🖼 From the bottom menu you can make changes to the default Thumbnail.

❔ This image, which is known as Thumbnail, is placed at the beginning of the video, music or Telegram file.
▫️
            ",
            'btn' => [
                'get_current_default_thumbnail' => '🖼Get current default thumbnail',
                'change_default_thumbnail' => '⚙️Change default thumbnail',
                'delete_default_thumbnail' => '🗑Delete default thumbnail',
            ],
            'doesnt_exist' => '📁 No image set for default Thumbnail',
            'update' => '🖼 Please send your default thumbnail image in reply to this message.
🔙 Or press on /start to cancel.
▫️',
            'changed' => '☑️ Default thumbnail successfully changed.
▫️',
            'deleted' =>  '☑️ Default thumbnail successfully deleted.'
        ]
    ],
    'photo' => [
        'result' => "
        🗃 Type: Photo 🎥
📥 Size: :size
↔️ Resolution: :resolution
▫️
        "
    ],
    'audio' => [
        'result' => "
        🗃 Type: Audio 🎵
📥 Size: :size
🎵 Audio Title:
:title
🗣 Performer:
:performer
⏱ Duration: :duration
🌇 Has thumbnail? :thumbnail
📄 File Name: :name

•
        "
    ],
    'document' => [
        'result' => "
        🗃 Type: Document 📁
📥 Size: :size
🌇 Has thumbnail? :thumbnail
📄 File Name: :name

•
        "
    ],
    'video' => [
        'result' => "
        🗃 Type: Video 🎥
📥 Size: :size
🌇 Has thumbnail? :thumbnail
↔️ Resolution: :resolution
⏱ Duration: :duration
📄 File Name: :name

•
        "
    ],
    'url' => [
        'result' => "
        💾 File name: :name

📥 File size: :size

⏳ Status: Waiting for your confirmation...
•
        ",
        'result2' => "
        💾 File name: :name

📥 File size: :size

⏳ Status: Downloading to the server...

⚡️ :progress
•
        ",
        'result3' => "
        💾 File name: :name

📥 File size: :size

⏳ Status: Uploading to Telegram (please wait a little)
•
        ",
    ]
];
