<?php

return [
    'title' => '🌟 Buy a special subscription',
    'size_allowed' => '📁 File size allowed: :size_allowed',
    'daily_volume' => '📅 Allowed daily volume: :daily_volume',
    'price' => '💸 Amount: :price',
    'alert' => '🚫🚫 No VPN, proxy or filtering is provided in TeleFileGram bot. Please do not buy a subscription for this purpose. 🚫🚫',
    'buy' => '💲Buy :title subscription ',
    'limits' => [
        'convert_file' => 'convert files',
        'upload_size' => 'increase upload volume',
        'daly_size' => 'increase daily upload size',
    ],
];
