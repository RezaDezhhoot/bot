<?php

return [
    'btn' => [
        'change_language' => 'Change Language / تغییر زبان',
        'default_thumbnail' => 'Default Thumbnail 🖼',
        'change_thumbnail_size_ok' => '✅Resize the thumbnail to size of the video↔',
        'change_thumbnail_size_false' => '⬜️Resize the thumbnail to size of the video↔',
    ],
];
