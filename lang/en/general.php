<?php

return [
    'processStarted' => '⏳We are preparing your file...',
    'words' => [
        'yes' => 'Yes',
        'no' => 'No'
    ],
    'lang' => [
        'select' => '🇺🇸 Please select your preferred language'
    ],
    'btn' => [
        'back' => 'Back↩️',
    ],
    'commands' => [
        'start' => "
            📤 If you send me the direct link of a file, I can send it as a telegram file.

📥 And vice versa, if you send me a telegram file, I can send you the direct link so that you can download it with programs like IDM or ADM at high speed.

💥 Or to download from YouTube, send me the YouTube video link.
❔ According to the following command, you can search in YouTube:
🔍 For example, type pitbull to search for videos

`@vid pitbull`

•
        ",
        'usage' => "
            📄 Allowed file size: :max_file_size

📂 Daily used volume: :daly_volume

⏳ Daily remaining volume: :remaining_volume

-------
If you want to upload or download files up to 3.91 GB or increase your daily limit to 60.00 GB, touch /upgrade 🌹
        ",
        "settings" => "
        ⚙️ Settings :

What you wanna change? ☺️

Select from the bottom menu : 👇
•
        "
    ],
];
