<?php

return [
    'results' => [
        'waiting' => '🕧 Waiting for the customer to send the payment.',
        'confirming' => '⏳ The transaction is being processed on the blockchain.',
        'confirmed' => '🔘 The process is confirmed by the blockchain.',
        'sending' => "🔁 Funds are sent to the customer's personal wallet.",
        'partially_paid' => "⚠️ The amount paid was less than the amount of the order.

🔸 Tracking code: :tracking_code
🔸 Amount: `:paid_amount`
🔸 Actually paid: `:actually_paid`

please contact support.",
        'finished' => "✅ Payment was successful.

🔸 Tracking code: :tracking_code
🔸 Amount: :paid_amount
🔸 Subscription: :subscription
🔸 Allowed file size: :max_file_size
🔸 Daily used volume: :daly_volume
        ",
        'failed' => "❌ The payment was not completed due to the error of some kind , Please try again.",
    ],
    'payment' => [
        'paid' => 'Was paid',
        'success_payment_creation' => "
        ✅ Payment successfully created.

🔸 Tracking code: :tracking_code
🔸 Amount: `:amount`
🔸 Address: :address
        ",
        'error_payment_creation' => '❌ Error creating payment.',
        'try_again' => 'Try again'
    ],
    'invoice' => [
        'invoice' => 'Invoice',
        'date' => 'Date',
        'customer' => 'Customer',
        'phone' => 'Phone',
        'total' => 'Total',
        'subscription' => 'Subscription',
        'currency' => 'Currency',
        'amount' => 'Amount',
    ],
    'messages' => [
        'creating' => "We are processing your order..."
    ]
];
