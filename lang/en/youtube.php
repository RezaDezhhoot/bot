<?php

return [
    'messages' => [
        'select_format' => "
    •
💾 File name: `:name`

❔ Please select the file type: 👇
•
    ",
        'select_quality' => "
        •
💾 File name: `:name`
📽 File type: `:type`

❔ Please select quality: 👇
•
        ",
        'select_type' => "
        •
💾 File name: `:name`
📽 File type: `:type`
💿 Quality: `:quality`
📥 File size: `:size`

❔ Please select download type: 👇
•
        ",
        'final_result' => "
        •
💾 File name: `:name`
📽 File type: `:type`
💿 Quality: `:quality`
📥 File size: `:size`
❔ Download type: `:download_type`

⏳ Status: Waiting for your confirmation...
•
        "
    ],
    'key' => [
        'video_format' => "🎥 Video(with sound)",
        'audio_format' => '🔉 Only sound',
        'download_link' => 'Download link',
        'telegram_file' => 'Telegram file',
    ]
];
