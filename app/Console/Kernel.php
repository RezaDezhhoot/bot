<?php

namespace App\Console;

use App\Modules\Currency\Commands\UpdatePrice;
use App\Modules\File\Commands\RemoveFiles;
use App\Modules\Telegram\Commands\StartMadeLine;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected $commands = [
        UpdatePrice::class,
        StartMadeLine::class,
        RemoveFiles::class
    ];

    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('currency:update-price')->runInBackground()->withoutOverlapping()->dailyAt('07:00');
        $schedule->command('file:remove')->runInBackground()->withoutOverlapping()->dailyAt('00:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
