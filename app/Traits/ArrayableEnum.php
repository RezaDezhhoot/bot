<?php

namespace App\Traits;

trait ArrayableEnum
{
    /**
     * Returns enum values as an array.
     */
    static function toArray($cases): array
    {
        $values = [];

        foreach ($cases as $index => $enumCase) {
            $values[$index] = $enumCase->value ?? $enumCase->name;
        }

        return $values;
    }


    public static function getLabels($default = null): array
    {
        $values = [];

        foreach (self::cases() as $index => $enumCase) {
            $values[$enumCase->value] = $enumCase->label().($default == $enumCase->value ? '✅' : '');
        }

        return $values;
    }

    public static function getValues(): array
    {
        $values = [];

        foreach (self::cases() as $index => $enumCase) {
            $values[$enumCase->value] = $enumCase->value;
        }

        return $values;
    }

}
