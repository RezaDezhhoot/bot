<?php

namespace App\Modules\Payment\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Payment\Enums\PaymentStatus;
use App\Modules\Payment\Enums\TransactionEnum;
use App\Modules\Payment\Models\Payment;
use Livewire\WithPagination;

class PaymentController extends BaseComponent
{
    use WithPagination;

    public $status , $transaction_status , $placeholder = 'ادرس پرداخت | کد رهگیری';

    protected $queryString = ['status','transaction_status'];

    public function mount()
    {
        $this->data['status'] = PaymentStatus::getLabels();
        $this->data['transaction_status'] = TransactionEnum::getValues();
    }

    public function render()
    {
        $items = Payment::query()->latest()
            ->when($this->status,function ($q){
                return $q->where('status',$this->status);
            })->when($this->transaction_status,function ($q) {
               return $q->where('transaction_status',$this->transaction_status);
            })->search($this->search)->paginate($this->per_page);
        return view('admin.payments.index-payment',get_defined_vars())->extends('admin.layouts.admin');
    }
}
