<?php

namespace App\Modules\Payment\Controllers\Bot;

use App\Http\Controllers\Controller;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Payment\Enums\Gateways;
use App\Modules\Payment\Models\Payment;
use App\Modules\Payment\Services\Providers\AqayePardakht;
use App\Modules\Payment\Services\Providers\NOWPayments;
use App\Modules\Subscription\Models\Subscription;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\ArrayShape;

class PaymentController extends Controller
{

    public static function payment(User $user , $subscription_id): array
    {
        $subscription = Subscription::query()->active()->findOrFail($subscription_id);
        $payment = match ($subscription->fiat_currency) {
            FiatCurrency::USD->value => (new NOWPayments(Gateways::NOWPayments->value)),
            FiatCurrency::IRR->value => (new AqayePardakht(Gateways::AqayePardakht->value)),
        };

        $payment = $payment->setSubscription($subscription)
            ->setUser($user)
            ->createPayment();

        return [
            'status' => $payment->getStatus(),
            'tracking_code' => $payment->getTrackingCode(),
            'payment_id' => $payment->getPaymentId(),
            'payment' => $payment->getInvoice(),
            'message' => $payment->getMessage(),
        ];
    }

    public static function callback(User $user , $payment_id): array
    {
        $payment = Payment::query()->findOrFail($payment_id);
        $service = match ($payment->gateway) {
            Gateways::NOWPayments->value => (new NOWPayments(Gateways::NOWPayments->value)),
            Gateways::AqayePardakht->value => (new AqayePardakht(Gateways::AqayePardakht->value)),
        };

        $callback = $service->settInvoice($payment)
            ->setUser($user)
            ->callback();

        return [
            'result' => $callback->getResult(),
            'status' => $callback->getStatus()
        ];
    }

    public static function webhook($gateway, Request $request): Model|Collection|Builder|array|null
    {
        return match ($gateway) {
            Gateways::AqayePardakht->value => (new AqayePardakht(Gateways::AqayePardakht->value))
                ->webhook($request),
            default => null,
        };
    }
}
