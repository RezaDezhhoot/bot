<?php

namespace App\Modules\Payment\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Payment\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __invoke($gateway , Request $request)
    {
        $payment = \App\Modules\Payment\Controllers\Bot\PaymentController::webhook($gateway,$request);

        abort_if(is_null($payment),404);

        return view('web.payments.callback',['payment' => $payment]);
    }
}
