<?php

use \Illuminate\Support\Facades\Route;

Route::prefix('payment')->as('payment.')->group(function (){
    Route::any('callback/{gateway}',\App\Modules\Payment\Controllers\Web\PaymentController::class)->name('callback');
});
