<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/payments',\App\Modules\Payment\Controllers\Admin\PaymentController::class)->name('payments');
});
