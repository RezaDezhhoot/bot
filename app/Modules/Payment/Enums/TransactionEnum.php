<?php

namespace App\Modules\Payment\Enums;

use App\Traits\ArrayableEnum;

enum TransactionEnum: string
{
    use ArrayableEnum;

    case WAITING = 'waiting';
    case CONFIRMING = 'confirming';
    case CONFIRMED = 'confirmed';
    case SENDING = 'sending';
    case PARTIALLY_PAID  = 'partially_paid';
    case FINISHED = 'finished';
    case FAILED = 'failed';
    case REFUNDED = 'refunded';
    case EXPIRED = 'expired';
}
