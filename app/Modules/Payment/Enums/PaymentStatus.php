<?php

namespace App\Modules\Payment\Enums;

use App\Traits\ArrayableEnum;

enum PaymentStatus: string
{
    use ArrayableEnum;

    case PENDING = 'pending';
    case PAID = 'paid';
    case REVERT = 'revert';
    case PARTIALLY_PAID  = 'partially_paid';

    public function label():string
    {
        return match ($this) {
            self::PENDING => 'در انتظار پداخت',
            self::PAID => 'پرداخت شده',
            self::REVERT => 'لغو شده',
            self::PARTIALLY_PAID => 'پرداخت کمتر از مبلغ سفارش',
        };
    }
}
