<?php

namespace App\Modules\Payment\Enums;

enum Gateways: string
{
    case NOWPayments = 'NOWPayments';
    case AqayePardakht = 'AqayePardakht';
}
