<?php

namespace App\Modules\Payment\Services\Providers;

use App\Modules\Currency\Enums\CryptoCurrencyEnum;
use App\Modules\Payment\Enums\PaymentStatus;
use App\Modules\Payment\Enums\TransactionEnum;
use App\Modules\Payment\Models\Payment;
use App\Modules\Payment\Services\Abstract\Gateway;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class NOWPayments extends Gateway
{
    public function createPayment(): static
    {
        $payment = Http::acceptJson()
            ->withHeader('x-api-key',$this->gateway->config['api_key'])
            ->post($this->gateway->config['base_url'].'/payment',[
                'price_amount' => exchange($this->subscription->fiat_currency,$this->gateway->config['base_currency'],$this->subscription->price),
                'price_currency' => $this->gateway->config['base_currency'],
                'pay_currency' => $this->subscription->crypto_currency,
                'ipn_callback_url' => route('payment.callback',$this->gateway->name),
                'order_id' => $this->subscription->id,
                'order_description' => 'description'
            ]);

        $this->status = $payment->status() == 201;
        if ($this->status) {
            $payment = $payment->object();
            $this->tracking_code = $payment->payment_id;
            $this->payment = $this->user->payments()->create([
                'fiat_price' => $payment->price_amount,
                'crypto_price' => $payment->pay_amount,
                'fiat_currency' => $payment->price_currency,
                'crypto_currency' => $payment->pay_currency,
                'gateway' => $this->gateway->name,
                'status' => PaymentStatus::PENDING->value,
                'subscription_id' => $this->subscription->id,
                'data' => $payment,
                'subscription_data' => $this->subscription->toArray(),
                'tracking_code' => $this->tracking_code,
                'address' => $payment->pay_address,
                'transaction_status' => $payment->payment_status
            ]);
            $this->message = __('payment.payment.success_payment_creation',[
                'tracking_code' => $this->tracking_code,
                'amount' => $payment->pay_amount.' '.CryptoCurrencyEnum::tryFrom($payment->pay_currency)->name,
                'address' => "`".$payment->pay_address."`",
            ]);
        }
        return $this;
    }

    public function callback(): static
    {
        $status = Http::acceptJson()
            ->withHeader('x-api-key',$this->gateway->config['api_key'])
            ->get($this->gateway->config['base_url'].'/payment/'.$this->payment->tracking_code);

        $this->tracking_code = $this->payment->tracking_code;
        $message = '';
        logger($status->body());
        if ($status->successful()) {
            $this->payment->load('subscription');
            $status_body = $status->body();
            $status = $status->object();
            switch ($status->payment_status) {
                case TransactionEnum::WAITING->value:
                    $message = __('payment.results.waiting');
                    break;
                case TransactionEnum::CONFIRMING->value:
                    $message = __('payment.results.confirming');
                    break;
                case TransactionEnum::CONFIRMED->value:
                    $message = __('payment.results.confirmed');
                    break;
                case TransactionEnum::SENDING->value:
                    $message = __('payment.results.sending');
                    break;
                case TransactionEnum::PARTIALLY_PAID->value:
                    $message = __('payment.results.partially_paid',[
                        'tracking_code' => $status->payment_id,
                        'paid_amount' => $status->pay_amount.' '.CryptoCurrencyEnum::tryFrom($status->pay_currency)->name,
                        'actually_paid' => $status->actually_paid.' '.CryptoCurrencyEnum::tryFrom($status->pay_currency)->name,
                    ]);
                    $this->payment->update([
                        'status' => PaymentStatus::PARTIALLY_PAID->value,
                        'data' => $status_body,
                        'transaction_status' => $status->payment_status,
                        'paid_amount' => $status->pay_amount,
                        'actually_paid' => $status->actually_paid,
                        'paid_at' => now(),
                    ]);
                    $this->payment->refresh();
                    break;
                case TransactionEnum::FINISHED->value:
                    $message = __('payment.results.finished',[
                        'tracking_code' => $this->tracking_code,
                        'paid_amount' => $status->pay_amount.' '.CryptoCurrencyEnum::tryFrom($status->pay_currency)->name,
                        'subscription' => $this->payment->subscription->title,
                        'max_file_size' => formatBytes($this->payment->subscription->subscription_size_byte),
                        'daly_volume' => formatBytes($this->payment->subscription->daly_volume_byte),
                    ]);
                    $this->status = true;
                    if ($this->payment->status != PaymentStatus::PAID->value) {
                        $this->payment->update([
                            'status' => PaymentStatus::PAID->value,
                            'data' => $status_body,
                            'transaction_status' => $status->payment_status,
                            'paid_amount' => $status->pay_amount,
                            'actually_paid' => $status->actually_paid,
                            'paid_at' => now(),
                            'expire_at' => now()->addDays($this->payment->subscription->subscription_day)
                        ]);
                    }
                    $this->payment->refresh();
                    break;
                case TransactionEnum::FAILED->value || TransactionEnum::EXPIRED->value || TransactionEnum::REFUNDED->value:
                    $message = __('payment.results.failed');
                    $this->payment->update([
                        'status' => PaymentStatus::REVERT->value,
                        'data' => $status_body,
                        'transaction_status' => $status->payment_status
                    ]);
                    $this->payment->refresh();
                    break;
            }
        } else {
            $message = __('payment.results.failed');
        }
        $this->result = $message;
        return $this;
    }
}
