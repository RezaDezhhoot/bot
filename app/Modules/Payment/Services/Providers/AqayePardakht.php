<?php

namespace App\Modules\Payment\Services\Providers;

use App\Modules\Currency\Enums\CryptoCurrencyEnum;
use App\Modules\Payment\Enums\PaymentStatus;
use App\Modules\Payment\Models\Payment;
use App\Modules\Payment\Services\Abstract\Gateway;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AqayePardakht extends Gateway
{
    public function createPayment(): static
    {
        try {
            DB::beginTransaction();

            $amount = exchange($this->subscription->fiat_currency,$this->gateway->config['base_currency'],$this->subscription->price);

            $this->payment = $this->user->payments()->create([
                'fiat_price' => $amount,
                'crypto_price' => 0,
                'fiat_currency' => $this->gateway->config['base_currency'],
                'gateway' => $this->gateway->name,
                'status' => PaymentStatus::PENDING->value,
                'subscription_id' => $this->subscription->id,
                'subscription_data' => $this->subscription->toArray(),
            ]);

            $payment = Http::acceptJson()
                ->post($this->gateway->config['api_url'].'/create',[
                    'pin' => app()->environment('production') ? $this->gateway->config['api_pin'] : 'sandbox',
                    'amount' => $amount/10,
                    'callback' => route('payment.callback',$this->gateway->name),
                    'invoice_id' => $this->payment->id,
                    'mobile' => $this->user->phone,
                    'description' => $this->subscription->title,
                ]);

            $this->status = $payment->status() == 200;
            if ($this->status) {
                $payment = $payment->object();
                $this->tracking_code = $payment->transid;

                $address = app()->environment('local') ?
                    $this->gateway->config['web_url']."/startpay/sandbox/$this->tracking_code" :
                    $this->gateway->config['web_url']."/startpay/$this->tracking_code";

                $this->payment->update([
                    'tracking_code' => $this->tracking_code,
                    'data' => $payment,
                    'address' => $address
                ]);

                $this->message = __('payment.payment.success_payment_creation',[
                    'tracking_code' => $this->tracking_code,
                    'amount' => number_format($amount).' '.$this->gateway->config['base_currency'],
                    'address' => $address,
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
        }
        return $this;
    }

    public function callback(): static
    {
        try {
            $status = Http::acceptJson()
                ->post($this->gateway->config['api_url'].'/verify',[
                    'pin' => app()->environment('production') ? $this->gateway->config['api_pin'] : 'sandbox',
                    'amount' => $this->payment->fiat_price / 10,
                    'transid' => $this->payment->tracking_code
                ]);
        } catch (\Exception $e) {
            report($e);
            $this->result = __('payment.results.failed');
            return $this;
        }

        $this->tracking_code = $this->payment->tracking_code;
        $message = '';
        if ( ($status->successful() && $status->object()->code == 1 ) || $status->object()->code == 2) {
            $this->payment->load('subscription');
            $message = __('payment.results.finished',[
                'tracking_code' => $this->tracking_code,
                'paid_amount' => number_format($this->payment->fiat_price).' '.$this->payment->fiat_currency,
                'subscription' => $this->payment->subscription->title,
                'max_file_size' => formatBytes($this->payment->subscription->subscription_size_byte),
                'daly_volume' => formatBytes($this->payment->subscription->daly_volume_byte),
            ]);
            $this->status = true;
            if ($this->payment->status != PaymentStatus::PAID->value) {
                $this->payment->update([
                    'status' => PaymentStatus::PAID->value,
                    'data' => $status->object(),
                    'paid_amount' => $this->payment->fiat_price,
                    'actually_paid' => $this->payment->fiat_price,
                    'paid_at' => now(),
                    'expire_at' => now()->addDays($this->payment->subscription->subscription_day)
                ]);
                $this->payment->refresh();
            }
        } else {
            $message = __('payment.results.failed');
        }
        $this->result = $message;
        return $this;
    }

    public function webhook(\Illuminate\Http\Request $request): Builder|array|Collection|Model|null
    {
        $payment = null;
        if ($request->has(['transid','tracking_number','cardnumber','status']) && $request->input('status')) {
            $payment = Payment::query()->findOrFail($request->input('invoice_id'));

            app()->setLocale($payment->user->lang);

            if ($payment->status != PaymentStatus::PAID->value) {
                $payment->update([
                    'callback_data' => $request->all()
                ]);
            }
        }
        return $payment;
    }
}
