<?php

namespace App\Modules\Payment\Services\Abstract;

use App\Modules\Payment\Models\Payment;
use App\Modules\Payment\Services\Interface\Payable;
use App\Modules\Subscription\Models\Subscription;
use App\Modules\User\Models\User;

abstract class Gateway implements Payable
{
    protected  $status = false , $subscription , $payment, $user , $tracking_code , $gateway ;

    protected $fiat_currency;

    protected $result , $message;

    public function __construct($gateway)
    {
        $this->fiat_currency = '';
        $this->gateway = \App\Modules\Payment\Models\Gateway::query()->where('name',$gateway)->firstOrFail();
    }

    public function getTrackingCode(): string|int|null
    {
        return $this->tracking_code;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setSubscription(Subscription $subscription): static
    {
        $this->subscription = $subscription;
        return $this;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getPaymentId(): int|null
    {
        return $this->payment->id ?? null;
    }

    public function getInvoice()
    {
        return $this->payment;
    }

    public function settInvoice(Payment $payment): static
    {
        $this->payment = $payment;
        return $this;
    }

    public function getResult(): string|null
    {
        return $this->result;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
