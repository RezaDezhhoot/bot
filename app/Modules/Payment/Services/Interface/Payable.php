<?php

namespace App\Modules\Payment\Services\Interface;

use App\Modules\Payment\Models\Payment;
use App\Modules\Subscription\Models\Subscription;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface Payable
{
    public function createPayment(): static;

    public function getStatus(): bool;

    public function getTrackingCode(): string|int|null;

    public function callback(): static;

    public function setSubscription(Subscription $subscription): static;

    public function setUser(User $user): static;

    public function getPaymentId(): int|null;

    public function getInvoice();

    public function settInvoice(Payment $payment): static;

    public function getResult(): string|null;

}
