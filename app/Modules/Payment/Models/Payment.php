<?php

namespace App\Modules\Payment\Models;

use App\Modules\Currency\Enums\CryptoCurrencyEnum;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Payment\Enums\PaymentStatus;
use App\Modules\Subscription\Models\Subscription;
use App\Modules\User\Models\User;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Morilog\Jalali\Jalalian;

/**
 * @property mixed $tracking_code
 * @property mixed $created_at
 */
class Payment extends Model
{
    use HasFactory , SoftDeletes , Searchable;

    protected $guarded = ['id'];

    public array $searchAbleColumns = ['tracking_code','address'];

    public $casts = [
        'data' => 'array',
        'subscription_data' => 'array'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function subscription(): BelongsTo
    {
        return $this->belongsTo(Subscription::class);
    }

    public function subscriptionData(): Attribute
    {
        return Attribute::make(set: fn($value) => json_encode($value));
    }

    public function scopePaid($q)
    {
        return $q->where('status',PaymentStatus::PAID->value);
    }

    public function scopeValid($q)
    {
        return $q->where('expire_at','>=' ,now());
    }

    public function statusLabel(): Attribute
    {
        return Attribute::get(function (){
           return PaymentStatus::tryFrom($this->status)->label();
        });
    }

    public function fiatLabel(): Attribute
    {
        return Attribute::get(function (){
            return FiatCurrency::tryFrom($this->fiat_currency)->name;
        });
    }

    public function cryptoLabel(): Attribute
    {
        return Attribute::get(function (){
            if (!is_null($this->crypto_currency))
                return CryptoCurrencyEnum::tryFrom($this->crypto_currency)->name;

            return null;
        });
    }

    public function date(): Attribute
    {
        return Attribute::get(function (){
            if (app()->getLocale() == 'fa') {
                return Jalalian::forge($this->created_at)->format('%A, %d %B %Y');
            } else {
                return Carbon::make($this->created_at)->format('M d , Y');
            }
        });
    }

}
