<?php

namespace App\Modules\Payment\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gateway extends Model
{
    use HasFactory , SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'config' => 'array'
    ];
}
