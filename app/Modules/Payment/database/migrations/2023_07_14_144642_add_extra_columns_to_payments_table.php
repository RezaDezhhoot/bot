<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('transaction_status')->default(\App\Modules\Payment\Enums\TransactionEnum::WAITING->value);
            $table->decimal('paid_amount',50,12)->default(0);
            $table->decimal('actually_paid',50,12)->default(0);
            $table->string('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn(['transaction_status','paid_amount','actually_paid','address']);
        });
    }
};
