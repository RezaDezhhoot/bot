<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        \Illuminate\Support\Facades\Artisan::call('db:seed',[
            '--class' => \App\Modules\Payment\database\seeders\GatewaySeeder::class,
            '--force' => true
        ]);
    }
};
