<?php

namespace App\Modules\Payment\database\seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Payment\Enums\Gateways;
use App\Modules\Payment\Models\Gateway;
use Illuminate\Database\Seeder;

class GatewaySeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Gateway::query()->updateOrCreate([
            'name' => Gateways::NOWPayments->value
        ],[
            'config' => [
                'username' => 'foaddavoodi@hotmail.com',
                'password' => 'f.31B31545',
                'api_key' => 'SE6F64N-28A4XER-MXM50X6-24DJYAE',
                'base_url' => 'https://api.nowpayments.io/v1',
                'base_currency' => FiatCurrency::USD->value
            ]
        ]);

        Gateway::query()->updateOrCreate([
            'name' => Gateways::AqayePardakht->value
        ],[
            'config' => [
                'username' => 'myf.davoodi@gmail.com',
                'password' => 'f.31B31545',
                'api_pin' => '135E9AB69C818D82967E',
                'api_url' => 'https://panel.aqayepardakht.ir/api/v2',
                'web_url' => 'https://panel.aqayepardakht.ir',
                'base_currency' => FiatCurrency::IRR->value
            ]
        ]);
    }
}
