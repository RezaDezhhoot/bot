<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/users',\App\Modules\User\Controllers\Admin\UserController::class)->name('users');
    Route::get('/users/{action}/{id?}',\App\Modules\User\Controllers\Admin\StoreUserController::class)->name('users.store');
});
