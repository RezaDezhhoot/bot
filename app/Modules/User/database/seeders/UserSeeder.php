<?php

namespace App\Modules\User\database\seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Modules\User\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         User::factory()->create();
    }
}
