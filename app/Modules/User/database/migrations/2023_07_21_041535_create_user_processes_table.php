<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_processes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('telegram_chat_id');
            $table->decimal('cpu_usage',60,10)->default(0);
            $table->bigInteger('memory_usage')->default(0);
            $table->bigInteger('time_left')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_processes');
    }
};
