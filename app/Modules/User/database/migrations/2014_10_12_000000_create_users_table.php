<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('phone')->nullable()->unique();
            $table->string('role')->default(\App\Modules\Role\Enums\RoleEnum::USER->value);
            $table->string('password')->nullable();
            $table->string('lang')->default(\App\Modules\Localization\Enums\Langs::FA->value);
            $table->text('thumbnail')->nullable();
            $table->boolean('default_thumbnail')->default(false);
            $table->string('telegram_chat_id')->unique()->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
