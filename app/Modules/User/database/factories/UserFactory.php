<?php

namespace App\Modules\User\database\factories;


use App\Modules\Role\Enums\RoleEnum;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory
 */
class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'name' => "admin",
            'password' => '12345678', // password
            'phone' => '1234',
            'role' => RoleEnum::SUPER_ADMIN->value
        ];
    }
}
