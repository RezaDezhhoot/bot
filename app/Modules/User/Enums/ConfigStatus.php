<?php

namespace App\Modules\User\Enums;

use App\Traits\ArrayableEnum;

enum ConfigStatus: string
{
    use ArrayableEnum;

    case ACTIVE = 'active';
    case NOT_ACTIVE = 'not_active';
}
