<?php

namespace App\Modules\User\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Role\Enums\RoleEnum;
use App\Modules\User\Models\User;
use Livewire\WithPagination;

class UserController extends BaseComponent
{
    use WithPagination;
    public $role , $placeholder = 'نام | شماره | ایمیل';
    public $queryString = ['role'];

    public function mount()
    {
        $this->data['roles'] = RoleEnum::getValues();
    }

    public function render()
    {
        $items = User::query()
            ->latest('id')->when($this->role,function ($q){
                return $q->where('role',$this->role);
            })->search($this->search)->paginate($this->per_page);
        return view('admin.users.index-user',get_defined_vars())->extends('admin.layouts.admin');
    }
}
