<?php

namespace App\Modules\User\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Role\Enums\RoleEnum;
use App\Modules\User\Enums\ConfigStatus;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;

class StoreUserController extends BaseComponent
{
    use WithPagination;

    public $user , $name , $phone , $role , $password , $thumbnail;

    public function mount($action , $id = null)
    {
        $this->set_mode($action);
        if ($this->mode == self::UPDATE_MODE)
        {
            $this->header = 'کاربر شماره '.$id;
            $this->user = User::query()->findOrFail($id);
            $this->name = $this->user->name;
            $this->phone = $this->user->phone;
            $this->thumbnail = $this->user->thumbnail;
            $this->role = $this->user->role;
        } else {
            $this->header = 'کاربر جدید';
        }

        $this->data['role'] = RoleEnum::getValues();
    }

    public function store()
    {
        if ($this->mode == self::UPDATE_MODE)
            $this->saveInDataBase($this->user);
        else {
            $this->saveInDataBase(new User());
            return redirect()->route('admin.users');
        }
    }

    public function saveInDataBase(User $model)
    {
        $fields = [
            'name' => ['nullable', 'string','max:65'],
            'phone' => ['required', 'string' , 'unique:users,phone,'. ($this->user->id ?? 0)],
            'role' => ['required',Rule::in($this->data['role'])],
            'thumbnail' => ['nullable','string','max:2000']
//            'v2rayNG' => [$this->day > 0 ? 'required' : 'nullable' ,'string','max:10000000000000000']
        ];
        $messages = [
            'name' => 'نام ',
            'phone' => 'شماره همراه',
            'role' => 'نقش',
            'thumbnail' => 'تصویر بند انگشتی'
//            'v2rayNG' => 'config',
        ];

        if ($this->mode == self::CREATE_MODE)
        {
            $fields['password'] = ['required','min:6','regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9]).*$/'];
            $messages['password'] = 'گذرواژه';
        }

        $this->validate($fields,[],$messages);

        $model->name = $this->name;
        $model->phone = $this->phone;
        $model->thumbnail = $this->thumbnail;
        $model->role = $this->role;
//        $model->v2rayNG = $this->v2rayNG;
        if ($this->mode == self::CREATE_MODE)
            $model->password = $this->password;

        try {
            DB::beginTransaction();
            $model->save();
            $model->refresh();
            DB::commit();
            $this->emitNotify('اطلاعات با موفقیت ذحیره شد');
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            $this->emitNotify('خظایی رخ داده است','warning');
//            $this->emitNotify($e->getMessage());
        }

        $this->emitNotify('اطلاعات با موفقیت ثبت شد');
    }

    public function render()
    {
        return view('admin.users.store-user')->extends('admin.layouts.admin');
    }
}
