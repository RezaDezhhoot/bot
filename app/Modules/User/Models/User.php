<?php

namespace App\Modules\User\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Modules\File\Models\ActiveProcess;
use App\Modules\File\Models\File;
use App\Modules\Payment\Models\Payment;
use App\Modules\Role\Enums\RoleEnum;
use App\Modules\Settings\Models\Setting;
use App\Modules\Telegram\Models\Action;
use App\Modules\Transaction\Models\TransactionHistory;
use App\Modules\User\database\factories\UserFactory;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property mixed $service_expires_at
 * @property mixed $role
 * @property mixed $telegram_chat_id
 * @property mixed $default_thumbnail
 * @property mixed $lang
 * @property mixed $thumbnail
 * @property mixed $action
 * @property mixed $id
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable , Searchable , SoftDeletes;

    protected array $searchAbleColumns = ['name','phone'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
   protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
    ];

    public $appends = ['usage_data'];

    public function password(): Attribute
    {
        return Attribute::make(
            set: fn($value) => Hash::make($value)
        );
    }

    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }

    public function isAdmin(): Attribute
    {
        return Attribute::make(
            get: fn() => in_array($this->role,[RoleEnum::SUPER_ADMIN->value,RoleEnum::ADMIN->value])
        );
    }

    public function generateToken(?string $tokenName = 'mobile-application'): string
    {
        return $this->createToken($tokenName)->plainTextToken;
    }

    public function action(): HasOne
    {
        return $this->hasOne(Action::class);
    }

    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class);
    }

    public function files(): HasMany
    {
        return  $this->hasMany(File::class);
    }

    public function getUsageDataAttribute()
    {
        $subscriptions = $this->payments()->with('subscription')->whereHas('subscription',function ($q) {
            return $q->active()->select(['subscription_size','daly_volume']);
        })->valid()->paid()->get();
        $today_uploaded = $this->files()->latest()->select('size')->withTrashed()->today()->converted()->sum('size');

        if ($subscriptions->count() >= 1) {
            $max_file_size = $subscriptions->max('subscription.subscription_size_byte');
            $daly_volume = $subscriptions->sum('subscription.daly_volume_byte') ;
        } else {
            $max_file_size = Setting::getSingleRow('default_size',0) * 1024 * 1024;
            $daly_volume = Setting::getSingleRow('default_daly_volume',0) * 1024 * 1024;
        }
        $remaining_volume = max($daly_volume - $today_uploaded , 0);
        return [
            'today_uploaded' => $today_uploaded, // B
            'max_file_size' => $max_file_size, // B
            'remaining_volume' => $remaining_volume, // B
            'daly_volume' => $daly_volume // B
        ];
    }

    public function activeProcess(): HasMany
    {
        return $this->hasMany(ActiveProcess::class);
    }
}
