<?php

namespace App\Modules\Settings\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $name)
 * @method static updateOrCreate(array $key, array $value)
 * @method static findOrFail($id)
 */
class Setting extends Model
{
    use HasFactory ;

    protected $fillable = ['name','value'];

    public static function getSingleRow($name, $default = '')
    {
        return Setting::where('name', $name)->first()->value ?? $default;
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = str_replace(env('APP_URL') . '/', '', $value);
    }

    public function getValueAttribute($value)
    {
        $data = json_decode($value, true);
        return is_array($data) ? $data : $value;
    }
}
