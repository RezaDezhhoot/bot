<?php

namespace App\Modules\Settings\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Settings\Models\Setting;
use Dflydev\DotAccessData\Data;

class SettingController extends BaseComponent
{
    public $header , $channels = [];

    public $default_size , $default_daly_volume;

    public function mount()
    {
        $this->header = 'تنظیمات پایه';
//        $this->channels = Setting::getSingleRow('channels',[]);
        $this->default_daly_volume = Setting::getSingleRow('default_daly_volume',0);
        $this->default_size = Setting::getSingleRow('default_size',0);
    }

    public function store()
    {
        $this->validate([
//            'channels' => ['nullable','array','max:10'],
//            'channels.*.link' => ['required','url','max:250'],
//            'channels.*.id' => ['required','string','max:250'],
//            'channels.*.title' => ['required','string','max:250'],
            'default_daly_volume' => ['required','numeric','between:0.1,102400000000'],
            'default_size' => ['required','numeric','between:0.1,102400000000'],
        ],[],[
            'channels' => 'کانال های اجباری',
            'channels.*.link' => 'لینک کانال',
            'channels.*.id' => 'ای دی کانال',
            'channels.*.title' => 'عنوان',
            'default_daly_volume' => 'حجم پیشفرض روزانه',
            'default_size' => 'حجم اپلود فایل پیشفرض'
        ]);
//        Setting::query()->updateOrCreate(['name' => 'channels'], ['value' => json_encode($this->channels)]);
        Setting::query()->updateOrCreate(['name' => 'default_daly_volume'], ['value' => $this->default_daly_volume]);
        Setting::query()->updateOrCreate(['name' => 'default_size'], ['value' => $this->default_size]);
        $this->emitNotify('اطلاعات با موفقیت ثبت شد');
    }


    public function addChannel()
    {
        $this->channels[] = [];
    }

    public function deleteChannel($key)
    {
        unset($this->channels[$key]);
    }

    public function render()
    {
        return view('admin.settings.setting-controller')->extends('admin.layouts.admin');
    }
}
