<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/settings', App\Modules\Settings\Controllers\Admin\SettingController::class)->name('settings');
});
