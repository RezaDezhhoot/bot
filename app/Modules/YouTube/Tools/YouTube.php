<?php

namespace App\Modules\YouTube\Tools;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\ShellExec;
use Symfony\Component\Process\Process;
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;


class YouTube
{
    protected $url , $youTube ;

    public $formats = [];

    public $data , $title , $video_id;

    public $user;

    public function __construct(\App\Modules\YouTube\Models\YouTube $youTube = null)
    {
        $this->youTube = $youTube;
    }

    public static function make(\App\Modules\YouTube\Models\YouTube $youTube = null): static
    {
        return new static($youTube);
    }

    public function url($url , $user): static
    {
        $this->url = $url;
        $this->user = $user;

        return $this;
    }

    public function save(): Model|Builder
    {
        return \App\Modules\YouTube\Models\YouTube::query()
            ->create([
                'base_url' => $this->url,
                'user_id' => $this->user->id,
                'data' => $this->data,
                'formats' => $this->formats,
                'title' => $this->title,
                'video_id' => $this->video_id
            ]);
    }

    public function getVideo(): static
    {
        $videoUrl = $this->url;
        $cmd = "yt-dlp -J -F " . escapeshellarg($videoUrl);
        exec($cmd, $output);

        try {
            $videoInfo = json_decode($output[count($output) - 1],true);
            $formats = array();
            $this->title = $videoInfo['title'];
            $this->video_id = $videoInfo['id'];
            foreach ($videoInfo['formats'] as $format) {
                if (isset($format['format_note']) && $format['format_note'] === 'DASH video') {
                    continue; // Skip DASH formats
                }

                $formatData = [
                    'format_code' => $format['format_id'],
                    'format_label' => $format['format_note'] ?? '',
                    'size' => $format['filesize'] ?? 0,
                    'resolution' => $format['resolution'],
                    'video_ext' => $format['video_ext'],
                    'audio_ext' => $format['audio_ext'],
                    'url' => $format['url'],
                    'tbr' => $format['tbr'],
                    'vcodec' => $format['vcodec'] ?? 'none',
                    'acodec' => $format['acodec'] ?? 'none',
                ];
                $formats[] = $formatData;
            }
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
        $this->formats = $formats;
        $this->data = $videoInfo;

        return $this;
    }


    public function refresh(): static
    {
        $this->url = $this->youTube->base_url;
        $this->getVideo();
        $this->youTube->update([
            'data' => $this->data,
            'formats' => $this->formats
        ]);
        $this->youTube->refresh();
        return $this;
    }

    public function getUrl($format_id): string
    {
        $format = collect(
            $this->youTube->formats
        )->where('format_code',$format_id)->first();
        return $format['url'];
    }

    public function downloadFormat($format_id , $name ,$prefix =  null): bool|string
    {
        $file = $this->youTube->file;
        $url = $this->getUrl($format_id);

        echo $url."\n";
        $path = generateTemplatePath($prefix).'/'.$name;
        $ok = Storage::disk($file->storage)->put($path,file_get_contents($url,false,stream_context_create([
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        ])));
        return  $ok ? $path : false;
    }
}
