<?php

namespace App\Modules\YouTube\Models;

use App\Modules\File\Models\File;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property mixed $format_id
 * @property mixed $formats
 */
class YouTube extends Model
{
    use HasFactory;

    protected $table = 'youtube';

    protected $guarded = ['id'];

    public $casts = [
        'data' => 'array',
        'formats' => 'array'
    ];

    public function file(): HasOne
    {
        return $this->hasOne(File::class,'youtube_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function format(): Attribute
    {
        return Attribute::get(function (){
            if (is_null($this->format_id))
                return null;

            return collect(
                $this->formats
            )->where('format_code',$this->format_id)->first();
        });
    }
}
