<?php

namespace App\Modules\YouTube\Enums;

enum Types: string
{
    case TELEGRAM_FILE = 'telegram';
    case DOWNLOAD_LINK = 'download';
}
