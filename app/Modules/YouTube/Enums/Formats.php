<?php

namespace App\Modules\YouTube\Enums;

enum Formats: string
{
    case VIDEO = 'video';
    case AUDIO = 'audio';
}
