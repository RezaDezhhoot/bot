<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('youtube', function (Blueprint $table) {
            $table->id();
            $table->text('base_url');
            $table->boolean('video')->nullable();
            $table->boolean('audio')->nullable();
            $table->string('type')->nullable();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('format_id')->nullable();
            $table->json('data')->nullable();
            $table->json('formats')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('youtube');
    }
};
