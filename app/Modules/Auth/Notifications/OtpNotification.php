<?php

namespace App\Modules\Auth\Notifications;

use App\Modules\SMS\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OtpNotification extends Notification
{
//    use Queueable;

    /**
     * Create a new notification instance.
     */
    protected $phone , $country_code , $code;
    public function __construct($phone , $country_code , $code)
    {
        $this->phone = $phone;
        $this->country_code = $country_code;
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toSms(object $notifiable)
    {
        return [
            'phone' => $this->phone,
            'country_code' => $this->country_code,
            'code' => $this->code
        ];
    }
}
