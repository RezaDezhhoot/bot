<?php

namespace App\Modules\Auth\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Auth\Models\Token;
use App\Modules\Auth\Notifications\OtpNotification;
use App\Modules\Auth\Rules\ReCaptchaRule;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\RateLimiter;

class AuthController extends BaseComponent
{
    public $recaptcha;
    public $phone , $password , $name, $otp ;
    public $logo , $authImage , $sms = false ;
    public $passwordLabel = 'رمز عبور';
    public $email , $sent = false , $user_name;

    public function render()
    {
        return view('admin.auth.auth')->extends('admin.layouts.auth');
    }

    public function login()
    {
        $this->validate([
            'phone' => ['required','string','exists:users,phone','max:250'],
            'password' => ['required','string','max:250'],
            'recaptcha' => ['required', new ReCaptchaRule],
        ],[],[
            'phone' => 'شماره همراه یا نام کاربری',
            'password' => 'رمز عبور',
            'recaptcha' => 'فیلد امنیتی'
        ]);
        if (Auth::attempt([
            'phone' => $this->phone,
            'password' => $this->password
        ],true)) {
            request()->session()->regenerate();
            return redirect()->intended(route('admin.dashboard'));
        } else
            return $this->addError('password','گذواژه یا شماره همراه اشتباه می باشد.');
    }

    private function resetInputs()
    {
        $this->reset(['phone', 'password']);
    }
}
