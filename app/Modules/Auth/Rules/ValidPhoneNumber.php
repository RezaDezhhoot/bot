<?php

namespace App\Modules\Auth\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidPhoneNumber implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (
            collect(['+61', '0061'])->contains(request('country_code')) &&
            strlen($value) == 9
        ) {
            return true;
        }

        return preg_match('/(0|\+?\d{2})(\d{7,8})/', $value) ||  preg_match('/(0047|\\+47|47)?\\d{8}/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('the phone number is invalid.');
    }
}
