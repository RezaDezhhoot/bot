<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Token extends Model
{
    use HasFactory , Notifiable;

    protected $guarded = ['id'];
}
