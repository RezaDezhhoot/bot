<?php

namespace App\Modules\Localization\Enums;

use App\Traits\ArrayableEnum;

Enum Langs: string
{
    use ArrayableEnum;

    case FA = 'fa';
    case EN = 'en';
    case ES = 'es';
    case IT = 'it';
    case AR = 'ar';
    case TR = 'tr';
    case RU = 'ru';
    case OZ = 'oz';

    public function label(): string
    {
        return match ($this) {
            self::FA => 'فارسی',
            self::EN => 'English',
            self::ES => 'Español',
            self::IT => 'Italiano',
            self::AR => 'العربية',
            self::TR => 'Türkçe',
            self::RU => 'Русский',
            self::OZ => "O'zbek",
        };
    }
}
