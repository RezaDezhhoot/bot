<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('crypto_currency')->default(\App\Modules\Currency\Enums\CryptoCurrencyEnum::BTC->value);
            $table->string('fiat_currency')->default(\App\Modules\Currency\Enums\FiatCurrency::USD->value);
            $table->renameColumn('usd_price','price');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn(['crypto_currency','fiat_currency']);
            $table->renameColumn('price','usd_price');
        });
    }
};
