<?php

namespace App\Modules\Subscription\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Currency\Enums\CryptoCurrencyEnum;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Subscription\Enums\Status;
use App\Modules\Subscription\Models\Subscription;
use Illuminate\Validation\Rule;

class StoreSubscription extends BaseComponent
{
    public $subscription , $header;

    public $title , $price  , $status , $subscription_day , $subscription_size , $daly_volume;

    public $crypto_currency , $fiat_currency;

    public function mount($action , $id = null)
    {
        self::set_mode($action);
        if ($this->mode == self::UPDATE_MODE) {
            $this->subscription = Subscription::query()->findOrFail($id);
            $this->title = $this->subscription->title;
            $this->header= $this->title;
            $this->price = $this->subscription->price;
            $this->status = $this->subscription->status;
            $this->subscription_day = $this->subscription->subscription_day;
            $this->subscription_size = $this->subscription->subscription_size;
            $this->daly_volume = $this->subscription->daly_volume;
            $this->crypto_currency = $this->subscription->crypto_currency;
            $this->fiat_currency = $this->subscription->fiat_currency;
        } else {
            $this->header = 'پلن جدید';
            $this->fiat_currency = FiatCurrency::USD->value;
            $this->crypto_currency = CryptoCurrencyEnum::BTC->value;
        }
        $this->data['status'] = Status::getValues();
        $this->data['crypto_currency'] = CryptoCurrencyEnum::getValues();
        $this->data['fiat_currency'] = FiatCurrency::getValues();
    }

    public function store()
    {
        if ($this->mode == self::UPDATE_MODE) {
            $this->saveInDataBase($this->subscription);
        } else {
            $this->saveInDataBase(new Subscription());
            $this->reset(['title','price','status','subscription_day','subscription_size','daly_volume','crypto_currency','fiat_currency']);
        }
    }

    private function saveInDataBase(Subscription $subscription) {
        $this->validate([
            'title' => ['required','string','max:200'],
            'price' => ['required','numeric','between:9,100000000.999'],
            'subscription_day' => ['required','integer','between:1,10000000'],
            'subscription_size' => ['required','integer','between:0.1,100000000000'],
            'daly_volume' => ['required','integer','between:0.1,100000000000'],
            'status' => ['required','string',Rule::in($this->data['status'])],
            'crypto_currency' => ['required','string',Rule::in($this->data['crypto_currency'])],
            'fiat_currency' => ['required','string',Rule::in($this->data['fiat_currency'])],
        ],[],[
            'title' => 'عنوان',
            'price' => 'قیمت',
            'subscription_day' => 'اعتبار اشتراک',
            'subscription_size' => 'حجم اشتراک برای اپلود',
            'daly_volume' => 'حجم روزانه',
            'status' => 'وضعیت اشتراک',
            'fiat_currency' => 'واحد پول فیات',
            'crypto_currency' => 'واحد پول کریپتو'
        ]);
        $subscription->title = $this->title;
        $subscription->price = $this->price;
        $subscription->subscription_day = $this->subscription_day;
        $subscription->subscription_size = $this->subscription_size;
        $subscription->daly_volume = $this->daly_volume;
        $subscription->status = $this->status;
        $subscription->fiat_currency = $this->fiat_currency;
        $subscription->crypto_currency = $this->crypto_currency;
        $subscription->save();
        $this->emitNotify('اطلاعات با موفقیت ثبت شد');
    }

    public function deleteItem()
    {
        Subscription::destroy($this->subscription->id);
        return redirect()->route('admin.subscription');
    }

    public function render()
    {
        return view('admin.subscription.store-subscription')->extends('admin.layouts.admin');
    }
}
