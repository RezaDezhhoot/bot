<?php

namespace App\Modules\Subscription\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Subscription\Enums\Status;
use App\Modules\Subscription\Models\Subscription;

class IndexSubscription extends BaseComponent
{
    protected $queryString = ['status'];

    public $status , $placeholder = 'عنوان';

    public function mount()
    {
        $this->data['status'] = Status::getValues();
    }

    public function render()
    {
        $items = Subscription::query()->latest()
            ->when($this->status,function ($q) {
                return $q->where('status',$this->status);
            })->search($this->search)->paginate($this->per_page);
        return view('admin.subscription.index-subscription',get_defined_vars())->extends('admin.layouts.admin');
    }

    public function delete($id)
    {
        Subscription::destroy($id);
    }
}
