<?php

namespace App\Modules\Subscription\Enums;

use App\Traits\ArrayableEnum;

Enum Status: string
{
    use ArrayableEnum;

    case DRAFT = 'draft';
    case ACTIVE = 'active';
}
