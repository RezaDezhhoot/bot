<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/subscription', \App\Modules\Subscription\Controllers\Admin\IndexSubscription::class)->name('subscription');
    Route::get('/subscription/{action}/{id?}', \App\Modules\Subscription\Controllers\Admin\StoreSubscription::class)->name('subscription.store');
});
