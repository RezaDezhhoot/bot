<?php

namespace App\Modules\Subscription\Models;

use App\Modules\Subscription\Enums\Status;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed $title
 * @property mixed $price
 * @property mixed $subscription_day
 * @property mixed $rial_price
 * @property mixed $subscription_size
 * @property mixed $status
 * @property mixed $auto_update
 * @property mixed $daly_volume
 * @property mixed $crypto_currency
 * @property mixed $fiat_currency
 */
class Subscription extends Model
{
    use HasFactory , SoftDeletes , Searchable;

    public array $searchAbleColumns = ['id'];

    public $appends = ['subscription_size_byte','daly_volume_byte'];

    public function scopeActive($q)
    {
        return $q->where('status',Status::ACTIVE->value);
    }

    public function getSubscriptionSizeByteAttribute($value)
    {
        return $this->subscription_size * 1024 * 1024;
    }


    public function getDalyVolumeByteAttribute($value)
    {
        return $this->daly_volume * 1024 * 1024;
    }

}
