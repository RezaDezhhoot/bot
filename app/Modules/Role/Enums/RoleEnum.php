<?php

namespace App\Modules\Role\Enums;

use App\Traits\ArrayableEnum;

enum RoleEnum: string
{
    use ArrayableEnum;

    case ADMIN = 'admin';
    case SUPER_ADMIN = 'super_admin';
    case USER = 'user';
}
