<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/currencies',\App\Modules\Currency\Controllers\Admin\IndexCurrency::class)->name('currencies');
    Route::get('/currencies/{action}/{id}',\App\Modules\Currency\Controllers\Admin\StoreCurrency::class)->name('currencies.store');
});
