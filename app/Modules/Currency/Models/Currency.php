<?php

namespace App\Modules\Currency\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory , Searchable;

    public array $searchAbleColumns = ['code'];

    protected $guarded = ['id'];
}
