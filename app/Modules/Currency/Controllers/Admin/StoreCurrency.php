<?php

namespace App\Modules\Currency\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Currency\Models\Currency;
use Livewire\Component;
use Livewire\WithPagination;

class StoreCurrency extends BaseComponent
{
    use WithPagination;

    public $value , $currency;

    public function mount($action , $id)
    {
        self::set_mode($action);
        $this->currency = Currency::query()->findOrFail($id);
        $this->value = $this->currency->value;
        $this->header = $this->currency->code;
    }

    public function store()
    {
        $this->validate([
            'value' => ['required','numeric','between:1,9999999999999999.9999999']
        ],[],[
            'value' => 'مقدار واحد'
        ]);
        $this->currency->value = $this->value;
        $this->currency->save();
        $this->emitNotify('اطلاعات با موفقیت ذخیره شده');
    }

    public function render()
    {
        return view('admin.currencies.store-currency')->extends('admin.layouts.admin');
    }
}
