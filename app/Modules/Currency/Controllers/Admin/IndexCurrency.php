<?php

namespace App\Modules\Currency\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Currency\Models\Currency;
use Livewire\Component;
use Livewire\WithPagination;

class IndexCurrency extends BaseComponent
{
    use WithPagination;

    public $placeholder = 'کد ارز';

    public function render()
    {
        $items = Currency::query()
            ->where('code','!=' , FiatCurrency::IRR->name)
            ->search($this->search)
            ->paginate($this->per_page);

        return view('admin.currencies.index-currency',get_defined_vars())->extends('admin.layouts.admin');
    }
}
