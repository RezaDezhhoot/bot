<?php

namespace App\Modules\Currency\Commands;

use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Currency\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdatePrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:update-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update price';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $urlWeb = sprintf('https://www.cbar.az/currencies/%s.%s.%s.xml',
            date('d'),
            date('m'),
            date('Y')
        );
        $response = Http::get($urlWeb);

        $currencies = simplexml_load_string($response->body())->children()[1]->children();

        $baseCurrencyCode = FiatCurrency::IRR->name; // Set the base currency code
        $baseCurrencyValue = 1; // Base currency value is always 1
        $base_value = 0;

        foreach ($currencies as $currency) {
            $code = (string)$currency->attributes()[0];
            $value = (float)$currency->Value;
            $ratio = (float)$currency->Nominal;

            if ($code === $baseCurrencyCode) {
                $base_value = ($value / $ratio) / 10 ;
                break;
            }
        }
        Currency::query()->updateOrCreate(['code' => $baseCurrencyCode], ['value' => $baseCurrencyValue]);

        foreach ($currencies as $currency) {
            $code = $currency->attributes()[0];
            $name = (string)$currency->Name;
            $value = (float)$currency->Value;
            $ratio = (float)$currency->Nominal;

            if ($code !== $baseCurrencyCode) {
                $value = 1 / ($base_value / ($value / $ratio)) ;
            }

            Currency::query()->updateOrCreate(['code' => $code], ['value' => $value]);
        }

        $this->info('price updated successfully');
    }
}
