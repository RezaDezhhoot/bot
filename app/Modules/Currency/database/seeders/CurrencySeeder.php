<?php

namespace App\Modules\Currency\database\seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\Currency\Models\Currency;
use App\Modules\User\Models\User;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         Currency::query()->updateOrCreate([
             'code' => FiatCurrency::USD->name,
             'value' => 500000
         ]);
        Currency::query()->updateOrCreate([
            'code' => FiatCurrency::IRR->name,
            'value' => 1
        ]);
    }
}
