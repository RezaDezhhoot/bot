<?php

namespace App\Modules\Currency\Enums;

use App\Traits\ArrayableEnum;

enum FiatCurrency: string
{
    use ArrayableEnum;

    case USD = 'usd';
    case IRR = 'irr';
}
