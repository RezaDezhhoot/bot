<?php

namespace App\Modules\Currency\Enums;

use App\Traits\ArrayableEnum;

enum CryptoCurrencyEnum: string
{
    use ArrayableEnum;

    case BTC = 'btc';
    case ETH = 'eth';
    case BTG = 'btg';
    case XMR = 'xmr';
    case ZEC = 'zec';
    case XVG = 'xvg';
    case ADA = 'ada';
    case LTC = 'ltc';
    case BCH = 'bch';
    case QTUM = 'qtum';
    case DASH = 'dash';
    case XLM = 'xlm';
    case XRP = 'xrp';
    case XEM = 'xem';
    case DGP = 'dgb';
    case LSK = 'lsk';
    case DOGE = 'doge';
    case TRX = 'trx';
    case KMD = 'kmd';
    case REP = 'rep';
    case BAT = 'bat';
    case ARK = 'ark';
    case WAVES = 'waves';
    case BNB = 'bnb';
    case XZC = 'xzc';
    case NANO = 'nano';
    case TUSD = 'tusd';
    case VET = 'vet';
    case ZEN = 'zen';
    case GRS = 'grs';
    case FUN = 'fun';
    case NEO = 'neo';
    case GAS = 'gas';
    case PAX = 'pax';
    case USDC = 'usdc';
    case ONT = 'ont';
    case XTZ = 'xtz';
    case LINK = 'link';
    case RVN = 'rvn';
    case USDT = 'usdt';
}
