<?php

namespace App\Modules\Base\Controllers\Web;

use App\Http\Controllers\BaseComponent;

class IndexController extends BaseComponent
{
    public function render()
    {
        return view('base.web.index');
    }
}
