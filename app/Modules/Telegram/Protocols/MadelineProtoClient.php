<?php

namespace App\Modules\Telegram\Protocols;

use danog\MadelineProto\API;
use danog\MadelineProto\Tools;

class MadelineProtoClient
{
    protected $MadelineProto;

    public function __construct()
    {
        $sessionPath = storage_path('madeline/session.madeline');
        $settingsPath = storage_path('madeline/settings.madeline');

        if (! file_exists(storage_path('madeline'))) {
            mkdir(storage_path('madeline'));
        }

        $settings = [
            'serialization' => [
                'serialization_interval' => 30,
            ],
            'app_info' => [
                'api_id' => '20121749',
                'api_hash' => '3a206fe30fb7704ca4b1c5e97d55f96b',
            ],
        ];
        $this->MadelineProto = new API($settingsPath, $settings);
    }

    public function start(): void
    {
        $this->MadelineProto->start();
    }

    public function stop(): void
    {
        $this->MadelineProto->stop();
    }

    public function login($config): void
    {
        $this->MadelineProto->botLogin($config);
    }

    public function getMadelineProtoInstance(): API
    {
        return $this->MadelineProto;
    }
}
