<?php

namespace App\Modules\Telegram\Jobs;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Models\ActiveProcess;
use App\Modules\File\Models\File;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Enums\ProcessType;
use App\Modules\Telegram\Enums\ProtocolEnum;
use App\Modules\Telegram\Tools\File as FileTool;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\YouTube\Tools\YouTube;
use FFMpeg\Filters\Video\VideoFilters;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ProtoneMedia\LaravelFFMpeg\Filters\WatermarkFactory;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use App\Modules\Telegram\Tools\File as FileTools;
use function Amp\File\exists;
use function Amp\File\move;

class ProcessFileOnBackground implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 6;
    /**
     * Create a new job instance.
     */

    protected $file , $messageId , $user , $downloaded_path = null , $destinationFilePath = null , $thumbnail_path = null;

    public function __construct(File $file , $messageId)
    {
        $this->file = $file;
        $this->messageId = $messageId;
        $this->user = $file->user;
    }

    /**
     * Execute the job.
     */

    private function sendPhoto($user,$downloaded_path,$file) {
        return FileTools::to($user->telegram_chat_id)
            ->sendMediaUsingMadeLine(
                $downloaded_path,
                [
                    '_' => 'inputMediaUploadedPhoto',
                    'file' => FileTools::getPath($downloaded_path),
                ]
            );
    }

    private function sendVideo($user,$downloaded_path,$file,$thumbnail_path = null)
    {
        return FileTools::to($user->telegram_chat_id)
            ->sendMediaUsingMadeLine(
                $downloaded_path,
                [
                    '_' => 'inputMediaUploadedDocument',
                    'file' => FileTools::getPath($downloaded_path),
                    'thumb' => $thumbnail_path ? FileTools::getPath($thumbnail_path) : null,
                    'mime_type' => $file->data['mime_type'] ?? 'video/mp4',
                    'attributes' => [
                        [
                            '_' => 'documentAttributeVideo',
                            'round_message' => false,
                            'supports_streaming' => true,
                            'duration' => (double)$file->duration ?? 0.00,
                            'w' => $file->data['width'] ?? 0 ,
                            'h' => $file->data['height'] ?? 0 ,
                        ]
                    ]
                ]
            );
    }

    private function sendAudio($user,$downloaded_path,$file,$thumbnail_path = null)
    {
        return FileTools::to($user->telegram_chat_id)
            ->sendMediaUsingMadeLine(
                $downloaded_path,
                [
                    '_' => 'inputMediaUploadedDocument',
                    'file' => FileTools::getPath($downloaded_path),
                    'thumb' => $thumbnail_path ? FileTools::getPath($thumbnail_path) : null,
                    'mime_type' => $file->data['mime_type'] ?? 'audio/mpeg',
                    'attributes' => [
                        [
                            '_' => 'documentAttributeAudio',
                            'duration' => $file->duration ?? 0,
                            'title' => $file->audio_title ?? $file->name,
                            'performer' => $file->audio_performer,
                        ]
                    ]
                ]
            );
    }

    private function sendDocument($user,$downloaded_path,$file,$thumbnail_path = null , $force_file = true)
    {
        return FileTools::to($user->telegram_chat_id)
            ->sendMediaUsingMadeLine(
                $downloaded_path,
                [
                    '_' => 'inputMediaUploadedDocument',
                    'file' => FileTools::getPath($downloaded_path),
                    'thumb' => $thumbnail_path ? FileTools::getPath($thumbnail_path) : null,
                    'force_file' => $force_file,
                    'mime_type' => $file->data['mime_type'] ?? null,
                    'attributes' => [
                        [
                            '_' => 'documentAttributeFilename',
                            'file_name' => $file->name,
                        ]
                    ]
                ]
            );
    }

    public function handle(): void
    {
        set_time_limit(0);

        $file = $this->file;
        $messageId = $this->messageId;
        $user = $file->user;
        $downloaded_path = null;
        $thumbnail_path = null;
        $api = null;
        $audio_path = null;

        try {
            if ($file->type == Types::FILE->value) {
                $downloaded_path = FileTools::download($file->data,$file->name,$file->storage,ProtocolEnum::MADE_LINE->value,"/$file->id");
            } elseif ($file->type == Types::LINK->value) {
                $downloaded_path = General::downloadUrlFileWithProgress($file->name,$file,$messageId,$user,'/'.$file->id);
            } elseif ($file->type == Types::YOUTUBE->value) {
                $youtubeTool = YouTube::make($file->youtube)->refresh();
                $downloaded_path = $youtubeTool->downloadFormat($file->youtube->format_id,$file->name,"/$file->id");
//                if ($file->youtube->format['acodec'] == 'none' && $file->youtube->video) {
//                    $audio_format = collect(
//                        $file->youtube->formats
//                    )->where('resolution','audio only')->first();
//                    $audio_path = $youtubeTool->downloadFormat($audio_format,Str::uuid().'.'.$audio_format['audio_ext']);
//                    $downloaded_path = $this->mergeAudio('templates/5486fbf5-d15b-45e4-8a74-941cc0ec9286/prefix_64c2e97f0fe3481f5d68b-b998-4065-855f-8d956ded0a9d.mp3','templates/97a4dc11-60bc-4f63-a44e-84fece46a5d6/aaaaa.mp4');
//                }
            }


            if (!$downloaded_path) {
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.link.file_not_found'));
                return;
            }

            if ($file->type == Types::YOUTUBE->value) {
                if (isset($file->youtube->data['thumbnail'])) {
                    $thumbnail_path = FileTools::download($file->youtube->data['thumbnail'],null,$file->storage,ProtocolEnum::STANDARD->value,"/$file->id");
                }
            } else {
                $thumbnail = FileTools::to($user->telegram_chat_id)->getFile($file->thumbnail);
                if ($thumbnail && $thumbnail['ok']) {
                    $thumbnail_path = FileTools::download($thumbnail['result']['file_path'],null,'public',null,"/$file->id");
                }
            }

            $file->update([
                'path' => $downloaded_path,
            ]);
            $file->refresh();

            if ($file->type == Types::LINK->value && !$file->auto_convert) {
                $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_path);
            } elseif ($file->type == Types::YOUTUBE->value) {
                if ($file->auto_convert) {
                    if ($file->format == FileEnum::VIDEO->value) {
                        $api = $this->sendVideo($user,$downloaded_path,$file,$thumbnail_path);
                    } elseif ($file->format == FileEnum::AUDIO->value) {
                        $api = $this->sendAudio($user,$downloaded_path,$file,$thumbnail_path);
                    }
                } else {
                    $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_path);
                }
            } else {
                switch ($file->format) {
                    case FileEnum::AUDIO->value:
                        $api = $this->sendAudio($user,$downloaded_path,$file,$thumbnail_path);
                        break;
                    case FileEnum::VIDEO->value:
                        if (!is_null($file->soft_sub)) {
                            $downloaded_path = $this->mergeSubTitle();
                        }
                        $api = $this->sendVideo($user,$downloaded_path,$file,$thumbnail_path);
                        break;
                    case FileEnum::DOCUMENT->value:
                        if ($file->auto_convert && !is_null($file->mime)) {
                            $type = $file->mime;
                            if (str_starts_with($type,'video')) {
                                $api = $this->sendVideo($user,$downloaded_path,$file,$thumbnail_path);
                            } elseif (str_starts_with($type, 'image')) {
                                $api = $this->sendPhoto($user,$downloaded_path,$file);
                            } elseif (str_starts_with($type,'audio')) {
                                $api = $this->sendAudio($user,$downloaded_path,$file,$thumbnail_path);
                            } else {
                                $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_path);
                            }
                        } else {
                            $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_path);
                        }
                        break;
                }
            }
            if (!is_null($api) && isset($api['_'])) {
                $file->update([
                    'converted' => true,
                ]);
                Utils::to($user->telegram_chat_id)->delete($messageId);
                $file->delete();
            }
            dropActiveProcess($user,ProcessType::JOB->value,$file->id);
            $this->removeDir();
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }

    private function mergeSubTitle(): ?string
    {
        $api= null;
        $outputVideoPath = null;
        $destinationFilePath = null;
        $videoPath = null;

        try {
            $subtitle = FileTool::to($this->file->user->telegram_chat_id)->getFile($this->file->soft_sub);
            if ($subtitle['ok']) {
                // Download subtitle
                $subtitlePath = FileTool::getPath(
                    FileTool::download($subtitle['result']['file_path'],$this->file->subtitle_name,'public',null,'/'.$this->file->id)
                );

                // Get real video path
                $videoPath = $this->file->path;

                // Set output directory
                $output_path = generateTemplatePath('/'.$this->file->id);
                Storage::disk($this->file->storage)->makeDirectory($output_path);
                $outputVideoPath = $output_path.'/'.$this->file->name;

                // Generate a unique fcat ilename for the destination file in the public path
                $destinationFileName = uniqid('prefix_') . $this->file->subtitle_name;

                if (app()->environment('local')) {
                    $this->destinationFilePath = public_path($destinationFileName);
                } else {
                    $this->destinationFilePath = base_path($destinationFileName);
                }
                move($subtitlePath,$this->destinationFilePath);

                // Begin the process of merging subtitles and video rendering
                FFMpeg::open($videoPath)
                    ->addFilter(function (VideoFilters $filters) use ($destinationFileName) {
                        $filters->custom("subtitles=".$destinationFileName);
                    })
                    ->export()
                    ->toDisk($this->file->storage)
                    ->inFormat(new \FFMpeg\Format\Video\X264('aac'))
                    ->save($outputVideoPath);
                $this->file->update([
                    'path' => $outputVideoPath
                ]);

            }
        } catch (\Exception $e) {
            report($e);
            @unlink($this->destinationFilePath);
            throw $e;
        }

        return $outputVideoPath;
    }


    public function failed($exception = null)
    {
        report($exception);
        dropActiveProcess($this->user,ProcessType::JOB->value,$this->file->id);
        Message::to($this->user->telegram_chat_id)->send(__('file.messages.process_error'));
        if (!is_null($this->destinationFilePath)) {
            if (file_exists($this->destinationFilePath)) {
                unlink($this->destinationFilePath);
            }
        }
        if ($this->file->type == Types::LINK->value) {
            $urlInstance = (new \App\Modules\Telegram\Actions\Medias\Url());
            $message = $urlInstance->getMessage($this->file);
            $urlInstance->getAPI($this->file->user,$this->file)->keyboard()->edit($this->messageId ,$message);
        }
        $this->removeDir();
    }

    private function removeDir()
    {
        removeFileDir($this->file , $this->file->id);
        if (!is_null($this->destinationFilePath)) {
            @unlink($this->destinationFilePath);
        }
    }
}
