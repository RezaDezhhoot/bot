<?php

namespace App\Modules\Telegram\Jobs;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Models\ActiveProcess;
use App\Modules\File\Models\File;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Enums\ProcessType;
use App\Modules\Telegram\Enums\ProtocolEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\YouTube\Tools\YouTube;
use danog\MadelineProto\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PrepareDownloadLinkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 6;
    /**
     * Create a new job instance.
     */
    protected $file , $messageId , $uploaded_path = null , $user;
    public function __construct(File $file , $messageId)
    {
        $this->messageId = $messageId;
        $this->file = $file;
        $this->user = $file->user;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        set_time_limit(0);

        $file = $this->file;
        $messageId = $this->messageId;
        $user = $file->user;

        try {
            if ($file->type == Types::FILE->value) {
                if (! $file->converted) {
                    $uploaded_path = \App\Modules\Telegram\Tools\File::download($file->data,$file->type != FileEnum::PHOTO->value ? $file->name : null,$file->storage,ProtocolEnum::MADE_LINE->value,'/'.$file->id);
                    $this->uploaded_path = $uploaded_path;
                    $file->update([
                        'link_generated_at' => now(),
                        'path' => $uploaded_path,
                        'converted' => true,
                    ]);
                }
            } elseif ($file->type == Types::LINK->value && $file->url) {
                if (! $file->converted) {
                    $uploaded_path = General::downloadUrlFileWithProgress($file->name,$file,$messageId,$user,'/'.$file->id,false);
                    $this->uploaded_path = $uploaded_path;
                    $file->update([
                        'link_generated_at' => now(),
                        'path' => $uploaded_path,
                        'converted' => true,
                    ]);
                } else throw new Exception('error convert');
            } elseif ($file->type == Types::YOUTUBE->value) {
                $youtubeTool = YouTube::make($file->youtube)->refresh();
                $uploaded_path = $youtubeTool->downloadFormat($file->youtube->format_id,$file->name,'/'.$file->id);
                $this->uploaded_path = $uploaded_path;
                $file->update([
                    'link_generated_at' => now(),
                    'path' => $uploaded_path,
                    'converted' => true,
                ]);
            }
            Utils::to($user->telegram_chat_id)->delete($messageId);
            Message::to($user->telegram_chat_id)->send(__('file.link.download_link_generated',[
                'link' => route('api.file.get',[base64_encode($file->id)])
            ]));
            dropActiveProcess($user,ProcessType::JOB->value,$file->id);
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }

    public function failed($exception = null)
    {
        report($exception);
        dropActiveProcess($this->user,ProcessType::JOB->value,$this->file->id);
        Message::to($this->file->user->telegram_chat_id)->send(__('file.messages.process_error'));
        removeFileDir($this->file , $this->file->id);
    }
}
