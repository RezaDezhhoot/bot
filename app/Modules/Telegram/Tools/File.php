<?php

namespace App\Modules\Telegram\Tools;

use App\Modules\Telegram\Core\Telegram;
use App\Modules\Telegram\Enums\ProtocolEnum;
use App\Modules\Telegram\Protocols\MadelineProtoClient;
use danog\MadelineProto\updates;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function Amp\File\exists;

class File extends Telegram
{
    public $caption = null , $disable_content_type_detection = false;

    public function caption($text): static
    {
        $this->caption = $text;
        return $this;
    }

    public function getFile($file_id)
    {
        return $this->http_request('getFile',[
            'file_id' => $file_id
        ]);
    }

    public function content_type_detection($allow): static
    {
        $this->disable_content_type_detection = ! $allow;
        return $this;
    }

    public function sendPhoto($file_id)
    {
        return $this->http_request('sendPhoto',[
            'chat_id' => $this->to,
            'photo' => $file_id,
            'reply_markup' => $this->keyboard,
            'caption' => $this->caption,
            'reply_to_message_id' => $this->replyId
        ]);
    }

    public function sendAudio($file_id , $title = null , $performer = null)
    {
        return $this->http_request('sendAudio',[
            'audio' => $file_id,
            'reply_markup' => $this->keyboard,
            'title' => $title,
            'caption' => $this->caption,
            'performer' => $performer,
            'reply_to_message_id' => $this->replyId
        ]);
    }

    public static function file($path): bool|string
    {
        return file_get_contents(config('bot.file_domain').config('bot.token')."/".$path,false);
    }

    //
    public static function download($file_path , $name = null , $storage = 'public' , $protocol = null , $prefix = null): string
    {
        if (is_null($name)) {
            $ex = 'jpg';
            $name = Str::uuid().'.'.$ex;
        }

        $path = generateTemplatePath($prefix);
        $output = $path.'/'.$name;
        switch ($protocol) {
            case ProtocolEnum::MADE_LINE->value:
                Storage::disk($storage)->makeDirectory($path);
                $madelineProtoClient = new MadelineProtoClient();
                $madelineProtoClient->start();
                $file_path['file_name'] = $name;
                $downloaded = $madelineProtoClient->getMadelineProtoInstance()
                    ->downloadToFile($file_path,self::getPath($path).'/'.$name);
                $madelineProtoClient->stop();
                break;
            case ProtocolEnum::STANDARD->value:
                $downloaded = Storage::disk($storage)->put($output,file_get_contents($file_path));
                break;
            default:
                $downloaded = Storage::disk($storage)->put($output, self::file($file_path));
        }

        return $downloaded ? $output : false;
    }

    public static function deleteFIle($path = null , $storage = 'public'): bool
    {
        if (!is_null($path)) {
            if (Storage::disk($storage)->exists($path)) {
                return Storage::disk($storage)->delete($path);
            }
        }
        return false;
    }

    public static function getLocal($path , $storage = 'public')
    {
        return Storage::disk($storage)->get($path);
    }

    public function sendFormDataImage($file , $storage = 'public' ,... $data)
    {
        $url = config('bot.domain').config('bot.token')."/sendPhoto?reply_markup=$this->keyboard&caption=$this->caption&reply_to_message_id=$this->replyId&chat_id=".$this->to;

        $client = new Client();
        $body = $client->post($url,[
            'multipart' => [
                [
                    'name'     => 'photo',
                    'contents' =>  self::getLocal($file,$storage),
                    'filename' => basename($file)
                ], ...array_filter($data)],
        ])->getBody();

        return json_decode($body , true);
    }

    public function sendFormDataAudio($file , $storage = 'public' ,... $data)
    {
        $url = config('bot.domain').config('bot.token')."/sendAudio?reply_markup=$this->keyboard&caption=$this->caption&reply_to_message_id=$this->replyId&chat_id=".$this->to;

        $client = new Client();
        $body = $client->post($url,[
            'multipart' => [
                [
                    'name'     => 'audio',
                    'contents' =>  self::getLocal($file,$storage),
                    'filename' => basename($file)
                ], ...array_filter($data)],
        ])->getBody();

        return json_decode($body , true);
    }

    public function sendFormDataVideo($file , $storage = 'public' ,... $data)
    {
        $url = config('bot.domain').config('bot.token')."/sendVideo?reply_markup=$this->keyboard&caption=$this->caption&reply_to_message_id=$this->replyId&chat_id=".$this->to;

        $client = new Client();
        $body = $client->post($url,[
            'multipart' => [
                [
                    'name'     => 'video',
                    'contents' =>  self::getLocal($file,$storage),
                    'filename' => basename($file)
                ], ...array_filter($data)],
        ])->getBody();

        return json_decode($body , true);
    }

    //
    public static function getPath($path): string
    {
        return Storage::path($path);
    }

    public function sendFormDataDocument($file , $storage = 'public' ,... $data)
    {
        $url = config('bot.domain').config('bot.token')."/sendDocument?disable_content_type_detection=$this->disable_content_type_detection&reply_markup=$this->keyboard&caption=$this->caption&reply_to_message_id=$this->replyId&chat_id=".$this->to;

        $client = new Client();
        $body = $client->post($url,[
            'multipart' => [
                [
                    'name'     => 'document',
                    'contents' =>  self::getLocal($file,$storage),
                    'filename' => basename($file)
                ], ...array_filter($data)],
        ])->getBody();

        return json_decode($body , true);
    }

    public function sendMediaUsingMadeLine($file , $media)
    {
        $madelineProtoClient = new MadelineProtoClient();
        $madelineProtoClient->start();
        $api = $madelineProtoClient->getMadelineProtoInstance()->messages
            ->sendMedia([
                'peer' => $this->to,
                'media' => $media,
                'parse_mode' => 'Markdown'
            ]);
        $madelineProtoClient->stop();

        return $api;
    }
}
