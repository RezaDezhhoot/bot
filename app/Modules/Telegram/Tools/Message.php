<?php

namespace App\Modules\Telegram\Tools;

use App\Modules\Telegram\Core\Telegram;

class Message extends Telegram
{
    public function send($message , $parse_mode = 'MarkDown')
    {
        return $this->http_request("sendMessage", array(
                'chat_id' => $this->to,
                'text' => $message,
                'reply_markup' => $this->keyboard,
                'parse_mode' => $parse_mode,
                'reply_to_message_id' => $this->replyId
            )
        );
    }

    public function edit($messageId , $message , $parse_mode = 'MarkDown')
    {
        return $this->http_request("editMessageText",array(
            'chat_id' => $this->to,
            'message_id'=> $messageId,
            'text' => $message,
            'reply_markup' => $this->keyboard,
            'parse_mode' => $parse_mode
        ));
    }

    public function editMessageReplyMarkup($messageId)
    {
        return $this->http_request("editMessageReplyMarkup",array(
            'chat_id' => $this->to,
            'message_id'=> $messageId,
            'reply_markup' => $this->keyboard,
        ));
    }
}
