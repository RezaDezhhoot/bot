<?php

namespace App\Modules\Telegram\Tools;

use App\Modules\Telegram\Core\Telegram;
use App\Modules\Telegram\Models\Action;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Utils extends Telegram
{

    public function setAction($action , $step = null , $model = null , $model_id = null , $message_id = null)
    {
        try {
            $user = User::query()->where('telegram_chat_id',$this->to)
                ->first();
            Action::query()->updateOrCreate([
                'user_id' => $user->id
            ],[
                'action' => $action,
                'step' => $step,
                'model' => $model,
                'model_id' => $model_id,
                'message_id' => $message_id
            ]);
        } catch (\Exception $e) {
            report($e);
        }
    }

    public function getAction(): Model|Builder|null
    {
        return Action::query()->whereHas('user',function ($q){
            return $q->where('telegram_chat_id',$this->to);
        })->first();
    }

    public function dropAction()
    {
        Action::query()->whereHas('user',function ($q){
            return $q->where('telegram_chat_id',$this->to);
        })->delete();
    }

    public static function toKeys($data , $action , $chunk = null) {
        $keys = [];
        foreach ($data as $index => $item) {
            $keys[] = self::key($item,json_encode([
                'action' => $action,
                'value' => $index
            ]));
        }

        return $chunk ? array_chunk($keys,$chunk) : $keys;
    }

    public static function key($text , $data , $url = null): array
    {
        $key = [
            'text' => $text,
            'callback_data' => $data
        ];

        if (!is_null($url))
            $key['url'] = $url;

        return $key;
    }

    public function delete($messageId): void
    {
        $result = $this->http_request("deleteMessage",array('chat_id' => $this->to,'message_id'=> $messageId));
    }

    public function isMember(...$channels): bool
    {
        if (sizeof($channels) > 0) {
            foreach ($channels as $item) {
                $result = $this->http_request("getChatMember",array('user_id' => $this->to,'chat_id'=> $item['id']));
                if ($result['result']['status'] == 'left') {
                    return false;
                }
            }
        }

        return true;
    }
}
