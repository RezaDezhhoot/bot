<?php

namespace App\Modules\Telegram\Commands;

use App\Modules\Telegram\Protocols\MadelineProtoClient;
use Illuminate\Console\Command;

class StartMadeLine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'made-line:login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $MadelineProtoClient = new MadelineProtoClient();
        $MadelineProtoClient->login(config('bot.token'));
    }
}
