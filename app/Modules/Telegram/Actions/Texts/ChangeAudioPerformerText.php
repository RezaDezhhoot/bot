<?php

namespace App\Modules\Telegram\Actions\Texts;

use App\Modules\File\Tools\Audio;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class ChangeAudioPerformerText implements \App\Modules\Telegram\Core\TextInterface
{
    public function process(User $user, $messageId, $text, $back = false): void
    {
        if (preg_match("/[a-zA-Z0-9]+/", $text) && strlen($text) <= 50) {
            $action = Utils::to($user->telegram_chat_id)->getAction();
            $fileInstance = Audio::on($action['model_id']);
            $fileInstance->update([
                'audio_performer' => $text,
                'audio_performer_changed' => true,
                'has_changed' => true
            ]);
            $file = $fileInstance->getFile();
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.audio_performer_changed'));
            $audio = new \App\Modules\Telegram\Actions\Medias\Audio();
            $audio->getAPI($user,$file)->keyboard()->edit($action['message_id'],$audio->getMessage($file));
            Utils::to($user->telegram_chat_id)->dropAction();
        } else {
            Message::to($user->telegram_chat_id)->send(__('validation.invalid_text'));
        }
    }
}
