<?php

namespace App\Modules\Telegram\Actions\Texts;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Actions\Medias\Audio;
use App\Modules\Telegram\Actions\Medias\Document;
use App\Modules\Telegram\Actions\Medias\Url;
use App\Modules\Telegram\Actions\Medias\Video;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetTypeQuery;
use App\Modules\Telegram\Core\TextInterface;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class ChangeNameText implements TextInterface
{
    public function process(User $user, $messageId, $text, $back = false): void
    {
        if (preg_match("/[a-zA-Z0-9]+/", $text) && strlen($text) <= 60) {
            $action = Utils::to($user->telegram_chat_id)->getAction();
            $fileInstance = General::on($action['model_id']);
            $file = $fileInstance->getFile();
            $valid_extension = pathinfo(basename($file->name), PATHINFO_EXTENSION);
            $new_extension = pathinfo(basename($text), PATHINFO_EXTENSION);
            if ($valid_extension == $new_extension) {
                $fileInstance->update([
                    'name' => $text,
                    'name_changed' => true,
                    'has_changed' => true
                ]);
                $file->refresh();
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.filename_changed'));
                switch ($file->type) {
                    case Types::LINK->value:
                        $url = new Url();
                        $url->getAPI($user,$file)->keyboard()->edit($action['message_id'],$url->getMessage($file));
                        break;
                    case Types::YOUTUBE->value:
                        $youtube = new YouTubeSetTypeQuery();
                        $youtube->getApi($user,$file)->keyboard()->edit($action['message_id'],$youtube->getMessage($file->youtube,$file->youtube->format));
                        break;
                    case Types::FILE->value:
                        switch ($file->format) {
                            case FileEnum::AUDIO->value:
                                $audio = new Audio();
                                $audio->getAPI($user,$file)->keyboard()->edit($action['message_id'],$audio->getMessage($file));
                                break;
                            case FileEnum::VIDEO->value:
                                $video = new Video();
                                $video->getAPI($user,$file)->keyboard()->edit($action['message_id'],$video->getMessage($file));
                                break;
                            case FileEnum::DOCUMENT->value:
                                $document = new Document();
                                $document->getAPI($user,$file)->keyboard()->edit($action['message_id'],$document->getMessage($file));
                                break;
                        }
                        break;
                }
                Utils::to($user->telegram_chat_id)->dropAction();
            } else {
                Message::to($user->telegram_chat_id)->send(__('validation.invalid_name'));
            }
        } else {
            Message::to($user->telegram_chat_id)->send(__('validation.invalid_text'));
        }
    }
}
