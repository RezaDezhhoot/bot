<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\Types;
use App\Modules\Telegram\Core\FileInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\File\Tools\Photo as PhotoAction;

class Photo implements FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        $fileInstance = null;

        if (!$isUrl) {
            $fileInstance = PhotoAction::on($file)->setUser($user)->setType(Types::FILE->value)->setFormat($fileEnum)->process()->create();

            $message = __('file.photo.result',[
                'size' => formatBytes($fileInstance->size),
                'resolution' => ($fileInstance->data['width'] ?? 0).'x'.($fileInstance->data['height'] ?? 0)
            ]);
            Message::to($user->telegram_chat_id)->reply($messageId)->addKey(
                Utils::key(__('file.link.get_download_link'),json_encode([
                    'action' => ActionEnum::GET_DOWNLOAD_LINK->value,
                    'file_id' => $fileInstance->id
                ]))
            )->keyboard()->send($message);
        }

    }
}
