<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\Types;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\File\Tools\Video as VideoAction;

class Video implements \App\Modules\Telegram\Core\FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        $fileInstance = null;
        if (!$isUrl) {
            $fileInstance = VideoAction::on($file)->setUser($user)->setType(Types::FILE->value)->setFormat($fileEnum)->process()
                ->setDefaultThumbnailIfDoesntExist($user->thumbnail)->create();

            $this->getAPI($user,$fileInstance)->reply($messageId)->keyboard()->send(
                $this->getMessage($fileInstance)
            ,null);
        }
    }

    public function getMessage($fileInstance)
    {
        return __('file.video.result',[
            'size' => formatBytes($fileInstance->size),
            'thumbnail' => $fileInstance->thumbnail ? __('general.words.yes') : __('general.words.no'),
            'resolution' => ($fileInstance->data['width'] ?? 0).'x'.($fileInstance->data['height'] ?? 0),
            'duration' => $fileInstance->time,
            'name' => $fileInstance->name
        ]);
    }

    public function getAPI(User $user , $fileInstance): Message
    {
        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('file.actions.change_name').(
                $fileInstance->name_changed ? '✅' : ''
                ),json_encode([
                'action' => ActionEnum::CHANGE_FILE_NAME->value,
                'file_id' => $fileInstance->id
            ])))
            ->addKey(Utils::key(__('file.actions.change_thumbnail').(
                $fileInstance->thumbnail_changed ? '✅' : ''
                ),json_encode([
                'action' => ActionEnum::CHANGE_THUMBNAIL->value,
                'file_id' => $fileInstance->id
            ])));

        if ($fileInstance->thumbnail) {
            $api = $api->addKey(Utils::key(__('file.actions.get_thumbnail'),json_encode([
                'action' => ActionEnum::GET_THUMBNAIL->value,
                'file_id' => $fileInstance->id
            ])));
        }

        $api = $api->addKey(Utils::key(__($fileInstance->soft_sub ? 'file.actions.change_soft_sub' : 'file.actions.add_soft_sub').(
            $fileInstance->soft_sub ? '✅' : ''
            ),json_encode([
            'action' => ActionEnum::ADD_SOFT_SUB->value,
            'file_id' => $fileInstance->id
        ])));

        if ($fileInstance->was_changed) {
            $api = $api->addKey(Utils::key(__('file.actions.apply_changes'),json_encode([
                'action' => ActionEnum::APPLY_CHANGES->value,
                'file_id' => $fileInstance->id
            ])));
        }

        return $api->addKey(Utils::key(__('file.link.get_download_link'),json_encode([
            'action' => ActionEnum::GET_DOWNLOAD_LINK->value,
            'file_id' => $fileInstance->id
        ])));
    }
}
