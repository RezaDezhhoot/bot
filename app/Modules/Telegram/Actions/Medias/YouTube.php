<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\Telegram\Core\FileInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\YouTube\Tools\YouTube as YouTubeTools;

class YouTube implements FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        try {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('general.processStarted'));
            $youtube = YouTubeTools::make()
                ->url($file,$user)
                ->getVideo()
                ->save();

            Message::to($user->telegram_chat_id)
                ->reply($messageId)
                ->addKey(
                    Utils::key(__('youtube.key.video_format'),json_encode([
                        'action' => ActionEnum::YOUTUBE_SET_VIDEO_FORMAT->value,
                        'youtube_id' => $youtube->id
                    ]))
                )
                ->addKey(
                    Utils::key(__('youtube.key.audio_format'),json_encode([
                        'action' => ActionEnum::YOUTUBE_SET_AUDIO_FORMAT->value,
                        'youtube_id' => $youtube->id
                    ]))
                )
                ->keyboard()
                ->send(
                    __('youtube.messages.select_format',[
                        'name' => $youtube->title
                    ])
                );

        } catch (\Exception $e) {
            report($e);
            Message::to($user->telegram_chat_id)->reply($messageId)->send(
                __('validation.invalid_url')
            );
        }
    }
}
