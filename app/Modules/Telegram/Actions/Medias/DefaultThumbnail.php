<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\Photo;
use App\Modules\Telegram\Core\FileInterface;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class DefaultThumbnail implements FileInterface
{
    public function process(User $user , $messageId , $file  , $isUrl = false , $fileEnum = null , $back = false): void
    {
        $fileInstance = null;

        if ($fileEnum == FileEnum::PHOTO->value) {
            $fileInstance = Photo::on($file)->setUser($user)->setType(Types::FILE->value)->setFormat($fileEnum)->process($file)->create();
            $user->update([
                'thumbnail' => $fileInstance->file_id
            ]);
            Message::to($user->telegram_chat_id)->send(__('file.thumbnail.default.changed'));
            Utils::to($user->telegram_chat_id)->dropAction();
        } else {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.thumbnail'));
        }
    }
}
