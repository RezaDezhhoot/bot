<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\ThumbnailEnum;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Core\FileInterface;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class Subtitle implements FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        $fileInstance = null;
        if ($fileEnum == FileEnum::DOCUMENT->value) {
            $action = Utils::to($user->telegram_chat_id)->getAction();
            $ext = mime2ext($file['mime_type']);
            if ($ext == 'srt') {
                $fileInstance = General::on($action['model_id']);
                $fileInstance->update([
                    'soft_sub' => $file['file_id'],
                    'has_changed' => true,
                    'subtitle_name' => $file['file_name'],
                ]);
                $file = $fileInstance->getFile();
                $video = new Video();
                $video->getAPI($user,$file)->keyboard()->edit($action['message_id'],$video->getMessage($file));
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.subtitle_added'));
                Utils::to($user->telegram_chat_id)->dropAction();
            }
        }
    }
}
