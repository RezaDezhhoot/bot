<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\ThumbnailEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\General;
use App\Modules\File\Tools\Photo;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class Thumbnail implements \App\Modules\Telegram\Core\FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        $fileInstance = null;
        if ($fileEnum == FileEnum::PHOTO->value) {
            $action = Utils::to($user->telegram_chat_id)->getAction();
            $thumbnailInstance = Photo::on($file)->setUser($user)->setType(Types::FILE->value)->setFormat($fileEnum)->process($file)->create();

            $fileInstance = General::on($action['model_id']);
            $fileInstance->update([
                'thumbnail' => $thumbnailInstance->file_id,
                'thumbnail_changed' => true,
                'has_changed' => true,
                'thumbnail_type' => ThumbnailEnum::CUSTOMIZED->value
            ]);

            $target_file = $fileInstance->getFile();
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.thumbnail_changed'));
            if ($target_file->type == Types::LINK->value) {
                $url = new Url();
                $url->getAPI($user,$target_file)->keyboard()->edit($action['message_id'],$url->getMessage($target_file));
            } else {
                switch ($target_file->format) {
                    case FileEnum::AUDIO->value:
                        $audio = new Audio();
                        $audio->getAPI($user,$target_file)->keyboard()->edit($action['message_id'],$audio->getMessage($target_file));
                        break;
                    case FileEnum::VIDEO->value:
                        $video = new Video();
                        $video->getAPI($user,$target_file)->keyboard()->edit($action['message_id'],$video->getMessage($target_file));
                        break;
                    case FileEnum::DOCUMENT->value:
                        $document = new Document();
                        $document->getAPI($user,$file)->keyboard()->edit($action['message_id'],$document->getMessage($file));
                        break;
                }
            }

            Utils::to($user->telegram_chat_id)->dropAction();
        } else {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.thumbnail'));
        }
    }
}
