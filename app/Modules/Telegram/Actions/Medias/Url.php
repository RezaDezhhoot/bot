<?php

namespace App\Modules\Telegram\Actions\Medias;

use App\Modules\File\Enums\Types;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\File\Tools\Url as UrlAction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Url implements \App\Modules\Telegram\Core\FileInterface
{
    public function process(User $user, $messageId, $file, $isUrl = false, $fileEnum = null, $back = false): void
    {
        set_time_limit(10);

        $remote_server = false;
        if ($isUrl) {
            try {
                $fileInfo = get_headers($file,1);
            } catch (\Exception $e) {
                logger('get_header');
                report($e);
                $fileInfo = Http::acceptJson()
                    ->get(config('bot.remote_servers.fileApi.get_file_info'),[
                        'url' => $file,
                    ]);
                if ($fileInfo->successful()) {
                    $fileInfo = $fileInfo->json();
                    $remote_server = true;
                }  else {
                    Message::to($user->telegram_chat_id)->reply($messageId)->send(
                        __('validation.invalid_url')
                    );
                    return;
                }
            }
            try {
                if (
                    ((isset($fileInfo['content-length']) && $fileInfo['content-length'] > 0) || (isset($fileInfo['Content-Length']) && $fileInfo['Content-Length'] > 0)) &&
                    (isset($fileInfo['content-type']) || isset($fileInfo['Content-Type']))
                ) {
                    DB::beginTransaction();
                    $fileInfo['url'] = $file;
                    $fileInfo['read_remote'] = $remote_server;
                    $file = UrlAction::on($fileInfo)->setUser($user)->setType(Types::LINK->value)
                        ->setDefaultThumbnailIfDoesntExist($user->thumbnail)
                        ->process()->create();
                    $file->update([
                        'file_id' => $file->id
                    ]);
                    DB::commit();
                    $this->getAPI($user,$file)->reply($messageId)->keyboard()->send(
                        $this->getMessage($file)
                        ,null);
                } else {
                    Message::to($user->telegram_chat_id)->reply($messageId)->send(
                        __('validation.invalid_url')
                    );
                }
            } catch (\Exception $e) {
                report($e);
                DB::rollBack();
                Message::to($user->telegram_chat_id)->reply($messageId)->send(
                    __('validation.invalid_url')
                );
            }
        }
    }

    public function getMessage($fileInstance , $message = null)
    {
        if (is_null($message)) {
            $message = __('file.url.result',[
                'name' => $fileInstance->name,
                'size' => formatBytes($fileInstance->size),
            ]);
        }

        return $message;
    }

    public function getAPI(User $user , $fileInstance , $on_download = false): Message
    {
        $api = Message::to($user->telegram_chat_id);

        if (! $on_download) {
            $api->addKey(Utils::key(__('file.actions.change_name').(
                $fileInstance->name_changed ? '✅' : ''
                ),json_encode([
                'action' => ActionEnum::CHANGE_FILE_NAME->value,
                'file_id' => $fileInstance->id
            ])))
                ->addKey(Utils::key(__('file.actions.change_thumbnail').(
                    $fileInstance->thumbnail_changed ? '✅' : ''
                    ),json_encode([
                    'action' => ActionEnum::CHANGE_THUMBNAIL->value,
                    'file_id' => $fileInstance->id
                ])));

            if ($fileInstance->thumbnail) {
                $api = $api->addKey(Utils::key(__('file.actions.get_thumbnail'),json_encode([
                    'action' => ActionEnum::GET_THUMBNAIL->value,
                    'file_id' => $fileInstance->id
                ])));
            }

            $api = $api->addKey(Utils::key(__('file.link.get_download_link'),json_encode([
                'action' => ActionEnum::GET_DOWNLOAD_LINK->value,
                'file_id' => $fileInstance->id
            ])));

            $api = $api->addKey(Utils::key(__($fileInstance->auto_convert ? 'file.actions.auto_convert_ok': 'file.actions.auto_convert_false'),json_encode([
                'action' => ActionEnum::AUTO_CONVERT->value,
                'file_id' => $fileInstance->id
            ])));

            $api = $api->addKey(Utils::key(__(! $fileInstance->auto_convert ? 'file.actions.get_as_document_ok': 'file.actions.get_as_document_false'),json_encode([
                'action' => ActionEnum::AUTO_CONVERT->value,
                'file_id' => $fileInstance->id
            ])));

            $api = $api->addKey(Utils::key(__('file.actions.start_download_link'),json_encode([
                'action' => ActionEnum::APPLY_CHANGES->value,
                'file_id' => $fileInstance->id
            ])));
        }

        return $api;
    }

}
