<?php

namespace App\Modules\Telegram\Actions\Queries\YouTube;

use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\YouTube\Models\YouTube;

class YouTubeSetAudioFormatQuery implements \App\Modules\Telegram\Core\QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        $youtube = YouTube::query()->where('user_id',$user->id)
            ->findOrFail($query['youtube_id']);
        $youtube->update([
            'video' => false,
            'audio' => true,
        ]);
        $formats = collect($youtube->formats)
            ->sortByDesc('size')
            ->where('resolution','audio only')
            ->sortByDesc('size')
            ->where('size' , '>' , 0)
            ->map(function ($v) use ($youtube) {
                return Utils::key(
                    ($v['tbr']).'K - '.formatBytes($v['size']),json_encode([
                        'action' => ActionEnum::YOUTUBE_SET_QUALITY->value,
                        'ids' => $youtube->id.'_'.$v['format_code'],
                    ])
                );
            })->values()->toArray();
        Message::to($user->telegram_chat_id)
            ->addKey(...$formats)
            ->keyboard()
            ->edit($messageId,__('youtube.messages.select_quality',[
                'name' => $youtube->title,
                'type' => __('youtube.key.audio_format')
            ]));
    }
}
