<?php

namespace App\Modules\Telegram\Actions\Queries\YouTube;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\ThumbnailEnum;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\YouTube\Enums\Types;
use App\Modules\YouTube\Models\YouTube;
use Illuminate\Support\Facades\DB;

class YouTubeSetTypeQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        try {
            DB::beginTransaction();
            $values = explode('_',$query['values']);

            $youtube = YouTube::query()->where('user_id',$user->id)
                ->findOrFail($values[0]);

            $youtube->update([
                'type' => $values[1]
            ]);
            $youtube->refresh();

            $format = $youtube->format;

            $mimes = new \Mimey\MimeTypes;
            $ext = $youtube->video ? 'mp4' : 'mp3';
            $file = $youtube->file()->create([
                'name' => $youtube->title.'.'.$ext,
                'format' => $youtube->video ? FileEnum::VIDEO->value : FileEnum::AUDIO->value,
                'user_id' => $user->id,
                'size' => $format['size'],
                'type' => \App\Modules\File\Enums\Types::YOUTUBE->value,
                'thumbnail' => $user->thumbnail ?? null,
                'duration' => $youtube->data['duration'],
                'mime' => $mimes->getMimeType($ext),
                'data' => $youtube->data,
                'thumbnail_type' => ThumbnailEnum::CUSTOMIZED->value,
                'auto_convert' => true
            ]);


            $this->getApi($user , $file)->keyboard()
                ->reply($messageId)
                ->edit($messageId,$this->getMessage($youtube,$format));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            Message::to($user->telegram_chat_id)->send(__('file.messages.process_error'));
        }
    }

    public function getMessage($youtube , $format)
    {
        return __('youtube.messages.final_result',[
            'name' => $youtube->file ? $youtube->file->name : $youtube->title,
            'type' => $youtube->audio ? __('youtube.key.audio_format') : __('youtube.key.video_format'),
            'quality' => $youtube->audio ? $format['tbr'].'k' : $format['resolution'],
            'size' => formatBytes($format['size']),
            'download_type' => $youtube->type == Types::DOWNLOAD_LINK->value  ? __('youtube.key.download_link') : __('youtube.key.telegram_file')
        ]);
    }

    public function getApi(User $user , $file): Message
    {
        return $file->youtube->type == Types::DOWNLOAD_LINK->value ? $this->getDownloadLinkTypeApi($user,$file) : $this->geTelegramTypeAPI($user,$file);
    }

    public function getDownloadLinkTypeApi(User $user , $fileInstance): Message
    {
        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('file.actions.change_name').(
                $fileInstance->name_changed ? '✅' : ''
                ),json_encode([
                'action' => ActionEnum::CHANGE_FILE_NAME->value,
                'file_id' => $fileInstance->id
            ])));

        return $api->addKey(Utils::key(__('file.link.get_download_link'),json_encode([
            'action' => ActionEnum::GET_DOWNLOAD_LINK->value,
            'file_id' => $fileInstance->id
        ])));
    }

    public function geTelegramTypeAPI(User $user , $fileInstance): Message
    {
        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('file.actions.change_name').(
                $fileInstance->name_changed ? '✅' : ''
                ),json_encode([
                'action' => ActionEnum::CHANGE_FILE_NAME->value,
                'file_id' => $fileInstance->id
            ])));


        $api->addKey(Utils::key(__($fileInstance->auto_convert ? 'file.actions.auto_convert_ok': 'file.actions.auto_convert_false'),json_encode([
            'action' => ActionEnum::AUTO_CONVERT->value,
            'file_id' => $fileInstance->id
        ])));

        $api->addKey(Utils::key(__(! $fileInstance->auto_convert ? 'file.actions.get_as_document_ok': 'file.actions.get_as_document_false'),json_encode([
            'action' => ActionEnum::AUTO_CONVERT->value,
            'file_id' => $fileInstance->id
        ])));

        return $api->addKey(Utils::key(__('file.actions.start_download_link'),json_encode([
            'action' => ActionEnum::APPLY_CHANGES->value,
            'file_id' => $fileInstance->id
        ])));
    }
}
