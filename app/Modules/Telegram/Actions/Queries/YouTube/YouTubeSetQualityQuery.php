<?php

namespace App\Modules\Telegram\Actions\Queries\YouTube;

use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use App\Modules\YouTube\Enums\Types;
use App\Modules\YouTube\Models\YouTube;

class YouTubeSetQualityQuery implements \App\Modules\Telegram\Core\QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        $ids = explode('_',$query['ids']);

        $youtube = YouTube::query()->where('user_id',$user->id)
            ->findOrFail($ids[0]);

        $youtube->update([
            'format_id' => $ids[1]
        ]);

        $format = collect(
            $youtube->formats
        )->where('format_code',$ids[1])->first();

        Message::to($user->telegram_chat_id)
            ->reply($messageId)
            ->addKey(
                Utils::key(
                    __('youtube.key.telegram_file'),json_encode([
                        'action' => ActionEnum::YOUTUBE_SET_TYPE->value,
                        'values' => $youtube->id.'_'.Types::TELEGRAM_FILE->value
                    ])
                )
            )
            ->addKey(
                Utils::key(
                    __('youtube.key.download_link'),json_encode([
                        'action' => ActionEnum::YOUTUBE_SET_TYPE->value,
                        'values' => $youtube->id.'_'.Types::DOWNLOAD_LINK->value
                    ])
                )
            )
            ->keyboard()
            ->edit($messageId,__('youtube.messages.select_type',[
                'name' => $youtube->title,
                'type' => $youtube->audio ? __('youtube.key.audio_format') : __('youtube.key.video_format'),
                'quality' => $youtube->audio ? $format['tbr'].'k' : $format['resolution'],
                'size' => formatBytes($format['size'])
            ]));
    }
}
