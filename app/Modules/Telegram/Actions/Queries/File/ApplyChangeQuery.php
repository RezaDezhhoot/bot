<?php

namespace App\Modules\Telegram\Actions\Queries\File;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\Document;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ProcessType;
use App\Modules\Telegram\Jobs\ProcessFileOnBackground;
use App\Modules\Telegram\Tools\File;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use Intervention\Image\Facades\Image;
use function App\Modules\Telegram\Actions\Queries\str_starts_with;

class ApplyChangeQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        set_time_limit(0);

        $fileInstance = Document::on($query['file_id']);
        $file = $fileInstance->getFile();

        $usage_data = $user->usage_data;
        if ($file['size'] >= $usage_data['max_file_size'] ) {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.subscription_limit',[
                'text' => __('subscription.limits.upload_size')
            ]));
            return;
        }

        if ($file['size'] >= $usage_data['remaining_volume'] ) {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.subscription_limit',[
                'text' => __('subscription.limits.daly_size')
            ]));
            return;
        }

        $thumbnail_path = null;
        $downloaded_path = null;
        if ($file->size <= \App\Modules\File\Models\File::MAX_FILE_SIZE_TO_SEND && is_null($file->soft_sub) && $file->type != Types::YOUTUBE->value) {
            if (
                $user->activeProcess()
                    ->where('file_id',$file->id)
                    ->where('process_type', ProcessType::NORMAL->value)
                    ->where('created_at', '>',now()->subMinutes(2))
                    ->exists()
            ) {
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.task_on_process'));
                return;
            }
            $user->activeProcess()->create([
                'process_type' => ProcessType::NORMAL->value,
                'file_id' => $file->id
            ]);

            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('general.processStarted'));

            try {
                if ($file->type == Types::FILE->value) {
                    $path = File::to($user->telegram_chat_id)->getFile($file->file_id);
                    if (!$path['ok']) {
                        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.link.file_not_found'));
                        return;
                    }
                    $downloaded_path = File::download($path['result']['file_path'],$file->name,$file->storage,null,'/'.$file->id);
                } elseif ($file->type == Types::LINK->value) {
                    $downloaded_path = General::downloadUrlFileWithProgress($file->name,$file,$messageId,$user,'/'.$file->id);
                }

                if (!$downloaded_path) {
                    Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.link.file_not_found'));
                    return;
                }

                $thumbnail = File::to($user->telegram_chat_id)->getFile($file->thumbnail);
                $thumbnail_data = null;
                if ($thumbnail && $thumbnail['ok']) {
                    $thumbnail_path = File::download($thumbnail['result']['file_path'],null,'public','/'.$file->id);
                    $thumbnail_data = [
                        'name' => 'thumbnail',
                        'contents' => File::getLocal($thumbnail_path),
                        'filename' => basename($thumbnail_path)
                    ];
                }

                $api = null;
                if ($file->type == Types::LINK->value && !$file->auto_convert) {
                    $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_data,$downloaded_path);
                } else {
                    switch ($file->format) {
                        case FileEnum::AUDIO->value:
                            $api = $this->sendAudio($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                            break;
                        case FileEnum::VIDEO->value:
                            $api = $this->sendVideo($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                            break;
                        case FileEnum::DOCUMENT->value:
                            if ($file->auto_convert && !is_null($file->mime)) {
                                $type = $file->mime;
                                if (str_starts_with($type,'video')) {
                                    $api = $this->sendVideo($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                                } elseif (str_starts_with($type,'image')) {
                                    $api = $this->sendImage($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                                } elseif (str_starts_with($type,'audio')) {
                                    $api = $this->sendAudio($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                                } else {
                                    $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                                }
                            } else {
                                $api = $this->sendDocument($user,$downloaded_path,$file,$thumbnail_data,$thumbnail_path);
                            }
                            break;
                    }
                }

                if (!is_null($api) && isset($api['ok'])) {
                    $file->update([
                        'path' => $downloaded_path,
                        'converted' => true,
                    ]);
                    Utils::to($user->telegram_chat_id)->delete($messageId);
                    $file->delete();
                }
            } catch (\Exception $e) {
                report($e);
                if ($file->type == Types::LINK->value) {
                    $urlInstance = (new \App\Modules\Telegram\Actions\Medias\Url());
                    $message = $urlInstance->getMessage($file);
                    $urlInstance->getAPI($user,$file)->keyboard()->edit($messageId ,$message);
                }

                Message::to($user->telegram_chat_id)->send(__('file.messages.process_error'));
            }
            removeFileDir($file , $file->id);

            dropActiveProcess($user,ProcessType::NORMAL->value,$file->id);
            removeEmptyFolders(storage_path());
        } else {

            if (
                $user->activeProcess()->where('file_id',$file->id)->where('process_type', ProcessType::JOB->value)->exists()
            ) {
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.task_on_process'));
                return;
            }
            $user->activeProcess()->create([
                'process_type' => ProcessType::JOB->value,
                'file_id' => $file->id
            ]);

            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('general.processStarted'));
            if ($file->type == Types::YOUTUBE->value) {
                dispatch(new ProcessFileOnBackground($file,$messageId))->onQueue('youtube');
            } else {
                dispatch(new ProcessFileOnBackground($file,$messageId))->onQueue('file');
            }
        }

    }

    public function sendImage(User $user , $downloaded_path , $file , $thumbnail_data , $thumbnail_path)
    {
        return File::to($user->telegram_chat_id)->sendFormDataImage($downloaded_path,$file->storage,
            $thumbnail_data
        );
    }

    public function sendAudio(User $user , $downloaded_path , $file , $thumbnail_data , $thumbnail_path)
    {
        return File::to($user->telegram_chat_id)->sendFormDataAudio($downloaded_path,$file->storage,
            [
                'name' => 'title',
                'contents' => $file->audio_title,
            ], [
                'name' => 'performer',
                'contents' => $file->audio_performer,
            ], $thumbnail_data
        );
    }

    public function sendVideo(User $user , $downloaded_path , $file , $thumbnail_data = null , $thumbnail_path =null)
    {
        if ($user->default_thumbnail && $thumbnail_data && $file->format == FileEnum::VIDEO->value && $file->type == Types::FILE->value) {
            $width = $file->data['width'] ?? null;
            $height = $file->data['height'] ?? null;
            $newFIle = Image::make($thumbnail_data['contents']);
            $newFIle->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public'.$thumbnail_path));
        }
        return File::to($user->telegram_chat_id)->sendFormDataVideo($downloaded_path,$file->storage,$thumbnail_data, [
            'name' => 'duration',
            'contents' => $file->duration
        ] );
    }

    public function sendDocument(User $user , $downloaded_path , $file , $thumbnail_data , $thumbnail_path)
    {
        return File::to($user->telegram_chat_id)->content_type_detection($file->auto_convert)
            ->sendFormDataDocument($downloaded_path,$file->storage,
            $thumbnail_data
        );
    }
}
