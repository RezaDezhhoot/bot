<?php

namespace App\Modules\Telegram\Actions\Queries\File;

use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\Document;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetTypeQuery;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\User\Models\User;

class AutoConvertQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        $fileInstance = Document::on($query['file_id']);
        $file = $fileInstance->getFile();
        $fileInstance->update([
            'auto_convert' => ! $file->auto_convert,
        ]);
        $file->refresh();
        if ($file->type == Types::LINK->value) {
            $media = new \App\Modules\Telegram\Actions\Medias\Url();
            $media->getAPI($user,$file)->keyboard()->edit($messageId,$media->getMessage($file),null);
        } elseif ($file->type == Types::YOUTUBE->value) {
            $media = new YouTubeSetTypeQuery();
            $media->getAPI($user,$file)->keyboard()->edit($messageId,$media->getMessage($file->youtube,$file->youtube->format));
        } else {
            $media = new \App\Modules\Telegram\Actions\Medias\Document();
            $media->getAPI($user,$file)->keyboard()->edit($messageId,$media->getMessage($file),null);
        }
    }
}
