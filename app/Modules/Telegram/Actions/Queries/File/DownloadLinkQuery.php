<?php

namespace App\Modules\Telegram\Actions\Queries\File;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\Types;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ProcessType;
use App\Modules\Telegram\Jobs\PrepareDownloadLinkJob;
use App\Modules\Telegram\Tools\File;
use App\Modules\Telegram\Tools\Message;
use App\Modules\User\Models\User;

class DownloadLinkQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        set_time_limit(0);

        $fileInstance = General::on($query['file_id']);
        $file = $fileInstance->getFile();

        $usage_data = $user->usage_data;
        if ($file['size'] >= $usage_data['max_file_size'] ) {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.subscription_limit',[
                'text' => __('subscription.limits.upload_size')
            ]));
            return;
        }
        if ($file['size'] >= $usage_data['remaining_volume'] ) {
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.subscription_limit',[
                'text' => __('subscription.limits.daly_size')
            ]));
            return;
        }

        $uploaded_path = null;

        if ($file->size < \App\Modules\File\Models\File::MAX_FILE_SIZE_TO_SEND && $file->type != Types::YOUTUBE->value) {
            if (
                $user->activeProcess()
                    ->where('file_id',$file->id)
                    ->where('process_type', ProcessType::NORMAL->value)
                    ->where('created_at', '>',now()->subMinutes(2))
                    ->exists()
            ) {
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.task_on_process'));
                return;
            }
            $user->activeProcess()->create([
                'process_type' => ProcessType::NORMAL->value,
                'file_id' => $file->id
            ]);

            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('general.processStarted'));

            try {
                if ($file->type == Types::FILE->value) {
                    $path = \App\Modules\Telegram\Tools\File::to($user->telegram_chat_id)->getFile($file->file_id);
                    if ($path['ok']) {
                        if (! $file->converted) {
                            $uploaded_path = File::download($path['result']['file_path'],$file->type != FileEnum::PHOTO->value ? $file->name : null,$file->storage,null,'/'.$file->id);
                            $fileInstance->update([
                                'link_generated_at' => now(),
                                'path' => $uploaded_path,
                                'converted' => true,
                            ]);
                            Message::to($user->telegram_chat_id)->edit($messageId,__('file.link.download_link_generated',[
                                'link' => route('api.file.get',[base64_encode($file->id)])
                            ]));
                        }
                    } else {
                        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.link.file_not_found'));
                    }
                } elseif ($file->type == Types::LINK->value && $file->url) {
                    if (! $file->converted) {
                        $uploaded_path = General::downloadUrlFileWithProgress($file->name,$file,$messageId,$user,'/'.$file->id,false);
                        $fileInstance->update([
                            'link_generated_at' => now(),
                            'path' => $uploaded_path,
                            'converted' => true,
                        ]);
                        Message::to($user->telegram_chat_id)->edit($messageId,__('file.link.download_link_generated',[
                            'link' => route('api.file.get',[base64_encode($file->id)])
                        ]));
                    }
                }
                dropActiveProcess($user,ProcessType::NORMAL->value,$file->id);
            } catch (\Exception $e) {
                report($e);
                if ($file->type == Types::LINK->value) {
                    $urlInstance = (new \App\Modules\Telegram\Actions\Medias\Url());
                    $message = $urlInstance->getMessage($file);
                    $urlInstance->getAPI($user,$file)->keyboard()->edit($messageId ,$message);
                }
                removeFileDir($file , $file->id);
                Message::to($user->telegram_chat_id)->send(__('file.messages.process_error'));
            }
            dropActiveProcess($user,ProcessType::NORMAL->value,$file->id);
        } else {
            if (
                $user->activeProcess()->where('file_id',$file->id)->where('process_type', ProcessType::JOB->value)->exists()
            ) {
                Message::to($user->telegram_chat_id)->reply($messageId)->send(__('validation.task_on_process'));
                return;
            }
            $user->activeProcess()->create([
                'process_type' => ProcessType::JOB->value,
                'file_id' => $file->id
            ]);
            Message::to($user->telegram_chat_id)->reply($messageId)->send(__('general.processStarted'));
            dispatch(new PrepareDownloadLinkJob($file,$messageId));
        }
    }
}
