<?php

namespace App\Modules\Telegram\Actions\Queries\File;

use App\Modules\File\Models\File;
use App\Modules\File\Tools\General;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class ChangeFileNameQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        $file = General::on($query['file_id'])->getFile();
        Utils::to($user->telegram_chat_id)->setAction(ActionEnum::CHANGE_FILE_NAME->value,null,File::class,$query['file_id'],$messageId);
        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.change_name_message',[
            'name' => $file->name
        ]),null);
    }

}
