<?php

namespace App\Modules\Telegram\Actions\Queries\Upgrade;

use App\Modules\Currency\Enums\CryptoCurrencyEnum;
use App\Modules\Payment\Controllers\Bot\PaymentController;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class UpgradeQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null , $back = false): void
    {
        Message::to($user->telegram_chat_id)
            ->edit($messageId,__('payment.messages.creating'));

        $payment = PaymentController::payment($user,$query['subscription_id']);
        if ($payment['status']) {
            $key = Utils::key(__('payment.payment.paid'),json_encode([
                'action' => ActionEnum::FINISH_PAYMENT->value,
                'payment_id' => $payment['payment_id']
            ]));
            $message = $payment['message'];
        } else {
            $key = Utils::key(__('payment.payment.try_again'),json_encode([
                'action' => ActionEnum::UPGRADE->value,
                'subscription_id' => $query['subscription_id']
            ]));
            $message = __('payment.payment.error_payment_creation');
        }

        Message::to($user->telegram_chat_id)->reply($messageId)
            ->addKey($key)
            ->addBackKey(ActionEnum::UPGRADE->value)
            ->keyboard()
            ->edit($messageId,$message);
    }

    public function callback(User $user, $messageId, $query)
    {
        $callback = PaymentController::callback($user,$query['payment_id']);
        $message = $callback['result'];

        if ($callback['status']) {
            Message::to($user->telegram_chat_id)->reply($messageId)
                ->edit($messageId,$message);
        } else {
            Message::to($user->telegram_chat_id)->reply($messageId)
                ->send($message);
        }
    }
}
