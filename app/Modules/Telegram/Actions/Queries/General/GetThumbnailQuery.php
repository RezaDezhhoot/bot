<?php

namespace App\Modules\Telegram\Actions\Queries\General;

use App\Modules\File\Tools\Photo;
use App\Modules\Telegram\Enums\TelegramFilePathEnum;
use App\Modules\Telegram\Tools\File;
use App\Modules\Telegram\Tools\Message;
use App\Modules\User\Models\User;
use function App\Modules\Telegram\Actions\Queries\str_starts_with;

class GetThumbnailQuery implements \App\Modules\Telegram\Core\QueryInterface
{
    public function process(User $user, $messageId, $query = null, $back = false): void
    {
        $fileInstance = Photo::on($query['file_id']);
        $file = $fileInstance->getFile();
        if (! is_null($file->thumbnail)) {
            $api = File::to($user->telegram_chat_id)->getFile($file->thumbnail);

            if ($api['ok']) {
                if (str_starts_with($api['result']['file_path'], TelegramFilePathEnum::THUMBNAIL->value)) {
                    $path = File::download($api['result']['file_path']);
                    File::to($user->telegram_chat_id)->reply($messageId)->sendFormDataImage($path);
                    File::deleteFIle($path);
                } else {
                    File::to($user->telegram_chat_id)->reply($messageId)->sendPhoto($file->thumbnail);
                }
                return;
            }
        }

        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.messages.thumbnail_doesnt_exist'));
    }
}
