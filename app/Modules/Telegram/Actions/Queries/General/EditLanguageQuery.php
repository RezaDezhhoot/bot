<?php

namespace App\Modules\Telegram\Actions\Queries\General;

use App\Modules\Localization\Enums\Langs;
use App\Modules\Telegram\Base\Messages;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class EditLanguageQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null , $back = false): void
    {
        $message = Message::to($user->telegram_chat_id)->addKey(
            ...Utils::toKeys(Langs::getLabels($user->lang),ActionEnum::SEND_LANGUAGE->value , 2)
        );

        if (!is_null($query)) $message->addBackKey(ActionEnum::SETTINGS->value)->keyboard()->edit($messageId,Messages::getSelectLangMessage());
        else $message->keyboard()->send(Messages::getSelectLangMessage());
    }

    public function change(User $user , $messageId , $query)
    {
        $user->update([
            'lang' => $query['value']
        ]);
        $this->process($user , $messageId , $query);
    }
}
