<?php

namespace App\Modules\Telegram\Actions\Queries\General;

use App\Modules\File\Tools\Photo;
use App\Modules\Telegram\Core\QueryInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\File;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\DB;

class DefaultThumbnailQuery implements QueryInterface
{
    public function process(User $user, $messageId, $query = null , $back = false): void
    {
        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('file.thumbnail.default.btn.get_current_default_thumbnail'),json_encode([
                'action' => ActionEnum::GET_DEFAULT_THUMBNAIL->value
            ])))
            ->addKey(Utils::key(__('file.thumbnail.default.btn.change_default_thumbnail'),json_encode([
                'action' => ActionEnum::UPDATE_DEFAULT_THUMBNAIL->value
            ])));

        if (! is_null($user->thumbnail) && File::to($user->telegram_chat_id)->getFile($user->thumbnail)['ok'] )
            $api = $api->addKey(Utils::key(__('file.thumbnail.default.btn.delete_default_thumbnail'),json_encode([
                'action' => ActionEnum::DELETE_DEFAULT_THUMBNAIL->value
            ])));

        $api->addBackKey(ActionEnum::SETTINGS->value)->keyboard()->edit($messageId,__('file.thumbnail.default.title'));
    }

    public function getDefault(User $user , $messageId , $query = null , $back = false)
    {
        if (! is_null($user->thumbnail)) {
            $file =  File::to($user->telegram_chat_id)->getFile($user->thumbnail);

            if ($file['ok']) {
                return File::to($user->telegram_chat_id)->reply($messageId)->sendPhoto($user->thumbnail);
            }
        }

        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.thumbnail.default.doesnt_exist'));
    }

    public function changeDefault(User $user , $messageId , $query = null , $back = false)
    {
        Utils::to($user->telegram_chat_id)->setAction(ActionEnum::UPDATE_DEFAULT_THUMBNAIL);
        Message::to($user->telegram_chat_id)->reply($messageId)->send(__('file.thumbnail.default.update'));
    }

    public function deleteDefault(User $user , $messageId , $query = null , $back = false)
    {
        if (!is_null($user->thumbnail)) {
            try {
                DB::beginTransaction();
                Photo::on($user->thumbnail)->delete();
                $user->update([
                    'thumbnail' => null
                ]);
                DB::commit();
                Message::to($user->telegram_chat_id)->send(__('file.thumbnail.default.deleted'));
                $this->process($user,$messageId,$query);
            } catch (\Exception $e) {
                DB::rollBack();
                report($e);
            }
        }
    }
}
