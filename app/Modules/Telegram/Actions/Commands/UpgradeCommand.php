<?php

namespace App\Modules\Telegram\Actions\Commands;

use App\Modules\Subscription\Models\Subscription;
use App\Modules\Telegram\Core\CommandInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class UpgradeCommand implements CommandInterface
{
    public function execute(User $user, $messageId, $message = null , $back = false): void
    {
        $subscriptions = Subscription::query()->latest()->orderByDesc('price')->active()->get()->toArray();
        $message = __('subscription.title')."\n\n";
        $keys = [];

        foreach ($subscriptions as $key => $subscription) {
            $message .= ($key + 1).' - '.$subscription['title']."\n";
            $message .= __('subscription.size_allowed',['size_allowed' => formatBytes($subscription['subscription_size_byte']) ])."\n";
            $message .= __('subscription.daily_volume',['daily_volume' => formatBytes($subscription['daly_volume_byte'])])."\n";
            $message .= __('subscription.price',['price' => number_format($subscription['price'],2).' '.$subscription['fiat_currency']])."\n";

            $message .= "\n\n";
            $keys[] = Utils::key(__('subscription.buy',['title' => $subscription['title']]),json_encode([
                'action' => ActionEnum::SELECT_SUBSCRIPTION->value,
                'subscription_id' => $subscription['id']
            ]));
        }
        $message .= __('subscription.alert');

        $api = Message::to($user->telegram_chat_id)->reply($messageId)
            ->addKey(...$keys)
            ->keyboard();

        $back ? $api->edit($messageId,$message) : $api->send($message);
    }
}
