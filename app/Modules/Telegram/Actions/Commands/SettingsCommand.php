<?php

namespace App\Modules\Telegram\Actions\Commands;

use App\Modules\Telegram\Core\CommandInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class SettingsCommand implements CommandInterface
{
    public function execute(User $user, $messageId, $message = null , $back = false): void
    {
        $message = __('general.commands.settings');

        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('settings.btn.change_language'),json_encode(['action' => ActionEnum::EDIT_LANGUAGE->value])))
            ->addKey(Utils::key(__('settings.btn.default_thumbnail'),json_encode(['action' => ActionEnum::DEFAULT_THUMBNAIL->value])))
            ->addKey(Utils::key(__($user->default_thumbnail ? 'settings.btn.change_thumbnail_size_ok' : 'settings.btn.change_thumbnail_size_false'),json_encode(['action' => ActionEnum::DEFAULT_THUMBNAIL_SET_ORIGINAL_SIZE->value])))
            ->addBackKey(ActionEnum::START->value)
            ->keyboard();
        $back ? $api->edit($messageId,$message) : $api->send($message);
    }

    public function originalSize(User $user, $messageId, $query = null)
    {
        $user->update([
            'default_thumbnail' => ! $user->default_thumbnail
        ]);
        $user->refresh();
        $this->execute($user,$messageId,$query,true);
    }
}
