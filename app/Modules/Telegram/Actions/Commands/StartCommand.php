<?php

namespace App\Modules\Telegram\Actions\Commands;

use App\Modules\Telegram\Core\CommandInterface;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;

class StartCommand implements CommandInterface
{
    public function execute(User $user , $messageId , $message = null , $back = false): void
    {
        Utils::to($user->telegram_chat_id)->dropAction();
        if ($back) {
            Message::to($user->telegram_chat_id)->edit($messageId,trans('general.commands.start'));
        } else {
            Message::to($user->telegram_chat_id)->send(trans('general.commands.start'));
        }
    }
}
