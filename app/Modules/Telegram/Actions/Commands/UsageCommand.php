<?php

namespace App\Modules\Telegram\Actions\Commands;

use App\Modules\Settings\Models\Setting;
use App\Modules\Telegram\Core\CommandInterface;
use App\Modules\Telegram\Tools\Message;
use App\Modules\User\Models\User;

class UsageCommand implements CommandInterface
{
    public function execute(User $user , $messageId , $message = null , $back = false): void
    {
        $data_usage = $user->usage_data;

        $max_file_size = formatBytes($data_usage['max_file_size']);
        $daly_volume = formatBytes($data_usage['daly_volume']);
        $remaining_volume = formatBytes($data_usage['remaining_volume']);
        unset($data_usage);
        $message = trans('general.commands.usage',get_defined_vars());

        if ($back) {
            Message::to($user->telegram_chat_id)->reply($messageId)->edit($messageId,$message);
        } else {
            Message::to($user->telegram_chat_id)->reply($messageId)->send($message);
        }
    }
}
