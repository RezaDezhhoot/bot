<?php

namespace App\Modules\Telegram\Actions\Commands;

use App\Modules\Telegram\Core\CommandInterface;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\File;
use App\Modules\Telegram\Tools\Message;
use App\Modules\Telegram\Tools\Utils;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Storage;

class DefaultThumbnailCommand implements CommandInterface
{

    public function execute(User $user, $messageId, $message = null, $back = false): void
    {
        $api = Message::to($user->telegram_chat_id)
            ->addKey(Utils::key(__('file.thumbnail.default.btn.get_current_default_thumbnail'),json_encode([
                'action' => ActionEnum::GET_DEFAULT_THUMBNAIL->value
            ])))
            ->addKey(Utils::key(__('file.thumbnail.default.btn.change_default_thumbnail'),json_encode([
                'action' => ActionEnum::UPDATE_DEFAULT_THUMBNAIL->value
            ])));

        if (! is_null($user->thumbnail) && File::to($user->telegram_chat_id)->getFile($user->thumbnail)['ok'] )
            $api = $api->addKey(Utils::key(__('file.thumbnail.default.btn.delete_default_thumbnail'),json_encode([
                'action' => ActionEnum::DELETE_DEFAULT_THUMBNAIL->value
            ])));

        $api->keyboard()->reply($messageId)->send(__('file.thumbnail.default.title'));
    }
}
