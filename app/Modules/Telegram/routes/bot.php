<?php

use Illuminate\Support\Facades\Route;

Route::post('/bot', [\App\Modules\Telegram\Controllers\Bot\BotController::class,'run']);
