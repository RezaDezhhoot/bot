<?php

namespace App\Modules\Telegram\Enums;

enum ProtocolEnum: string
{
    case NORMAL = 'normal';
    case MADE_LINE = 'made_line';
    case STANDARD = 'standard';
}
