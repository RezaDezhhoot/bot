<?php

namespace App\Modules\Telegram\Enums;

enum ProcessType: string
{
    case JOB = 'job';
    case NORMAL = 'normal';
}
