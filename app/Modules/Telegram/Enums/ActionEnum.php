<?php

namespace App\Modules\Telegram\Enums;

use App\Traits\ArrayableEnum;

enum ActionEnum: string
{
    use ArrayableEnum;

    case EDIT_LANGUAGE = 'edit_language';
    case SEND_LANGUAGE = 'send_language';

    case EDIT_THUMBNAIL = 'edit_thumbnail';
    case DEFAULT_THUMBNAIL = 'default_thumbnail';
    case DEFAULT_THUMBNAIL_SET_ORIGINAL_SIZE = 'default_thumbnail_set_original_size';
    case GET_DEFAULT_THUMBNAIL = 'get_default_thumbnail';
    case UPDATE_DEFAULT_THUMBNAIL = 'update_default_thumbnail';
    case DELETE_DEFAULT_THUMBNAIL = 'delete_default_thumbnail';

    case FINISH_PAYMENT = 'finish_payment';
    case SELECT_SUBSCRIPTION = 'select_subscription';

    case SETTINGS = 'settings';
    case START = 'start';
    case UPGRADE = 'upgrade';

    case BACK = 'back';

    case GET_DOWNLOAD_LINK = 'get_download_link';
    case CHANGE_FILE_NAME = 'change_file_name';
    case CHANGE_AUDIO_TITLE = 'change_audio_title';
    case CHANGE_AUDIO_PERFORMER = 'change_audio_performer';
    case CHANGE_THUMBNAIL = 'change_thumbnail';
    case GET_THUMBNAIL = 'get_thumbnail';
    case ADD_SOFT_SUB = 'add_soft_sub';
    case AUTO_CONVERT = 'AUTO_CONVERT';
    case GET_AS_TELEGRAM_DOCUMENT = 'get_as_telegram_document';
    case START_DOWNLOAD_LINK = 'start_download_link';
    case APPLY_CHANGES = 'apply_changes';

    case YOUTUBE_SET_VIDEO_FORMAT = 'youtube_set_video_format';
    case YOUTUBE_SET_AUDIO_FORMAT = 'youtube_set_audio_format';
    case YOUTUBE_SET_QUALITY = 'youtube_set_quality';
    case YOUTUBE_SET_TYPE = 'youtube_set_type';

    case SELECT_GATEWAY = 'select_gateway';
}
