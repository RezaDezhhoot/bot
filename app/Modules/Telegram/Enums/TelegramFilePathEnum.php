<?php

namespace App\Modules\Telegram\Enums;

enum TelegramFilePathEnum: string
{
    case THUMBNAIL = 'thumbnails';
}
