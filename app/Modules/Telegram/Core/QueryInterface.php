<?php

namespace App\Modules\Telegram\Core;

use App\Modules\User\Models\User;

interface QueryInterface
{
    public function process(User $user , $messageId , $query = null , $back = false): void;
}
