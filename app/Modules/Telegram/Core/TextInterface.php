<?php

namespace App\Modules\Telegram\Core;

use App\Modules\User\Models\User;

interface TextInterface
{
    public function process(User $user , $messageId , $text , $back = false): void;
}
