<?php

namespace App\Modules\Telegram\Core;

use App\Modules\User\Models\User;

interface FileInterface
{
    public function process(User $user , $messageId , $file  , $isUrl = false , $fileEnum = null , $back = false): void;
}
