<?php

namespace App\Modules\Telegram\Core;

use App\Modules\User\Models\User;

interface CommandInterface
{
    public function execute(User $user , $messageId , $message = null , $back = false): void;
}
