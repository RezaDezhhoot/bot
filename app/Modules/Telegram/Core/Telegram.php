<?php

namespace App\Modules\Telegram\Core;

use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Utils;
use CURLFile;
use http\Exception\InvalidArgumentException;

abstract class Telegram
{
    protected $replyId = null;

    protected $keys = [];

    public $to;

    public $keyboard = null;

    public static function to($to): static
    {
        return new static($to);
    }

    public function __construct($to)
    {
        $this->to = $to;
    }

    public function reply($messageId): static
    {
        $this->replyId = $messageId;
        return $this;
    }

    public function addKey(...$key): static
    {
        foreach ($key as $item) {
            $this->keys[] = array_is_list($item) ? $item : [$item];
        }
        return $this;
    }

    public function addBackKey($step = null): static
    {
        return $this->addKey(
            Utils::key(__('general.btn.back'),json_encode(['action' => ActionEnum::BACK->value, 'step' => $step]))
        );
    }

    public function keyboard(): static
    {
        $reply = [
            'inline_keyboard' => $this->keys,
        ];

        $this->keyboard =  json_encode($reply,true);

        return  $this;
    }

    protected function http_request($route , $query = [])
    {
        $url = config('bot.domain').config('bot.token')."/".$route;
        $ch_session = curl_init();
        curl_setopt($ch_session, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_session, CURLOPT_URL, $url);
        curl_setopt($ch_session, CURLOPT_POST, 1);
        curl_setopt($ch_session, CURLOPT_POSTFIELDS,http_build_query($query));
        $result = json_decode(curl_exec($ch_session),true);
        curl_close($ch_session);
        return $result;
    }
}
