<?php

namespace App\Modules\Telegram\Controllers\Bot;

use App\Http\Controllers\Controller;
use App\Modules\File\Enums\FileEnum;
use App\Modules\Telegram\Actions\Commands\DefaultThumbnailCommand;
use App\Modules\Telegram\Actions\Commands\SettingsCommand;
use App\Modules\Telegram\Actions\Commands\StartCommand;
use App\Modules\Telegram\Actions\Commands\UpgradeCommand;
use App\Modules\Telegram\Actions\Commands\UsageCommand;
use App\Modules\Telegram\Actions\Medias\Audio;
use App\Modules\Telegram\Actions\Medias\DefaultThumbnail;
use App\Modules\Telegram\Actions\Medias\Document;
use App\Modules\Telegram\Actions\Medias\Photo;
use App\Modules\Telegram\Actions\Medias\Subtitle;
use App\Modules\Telegram\Actions\Medias\Thumbnail;
use App\Modules\Telegram\Actions\Medias\Url;
use App\Modules\Telegram\Actions\Medias\Video;
use App\Modules\Telegram\Actions\Medias\YouTube;
use App\Modules\Telegram\Actions\Queries\File\AddSubTitleQuery;
use App\Modules\Telegram\Actions\Queries\File\ApplyChangeQuery;
use App\Modules\Telegram\Actions\Queries\File\AutoConvertQuery;
use App\Modules\Telegram\Actions\Queries\File\ChangeAudioPerformerQuery;
use App\Modules\Telegram\Actions\Queries\File\ChangeAudioTitleQuery;
use App\Modules\Telegram\Actions\Queries\File\ChangeFileNameQuery;
use App\Modules\Telegram\Actions\Queries\File\ChangeThumbnailQuery;
use App\Modules\Telegram\Actions\Queries\File\DownloadLinkQuery;
use App\Modules\Telegram\Actions\Queries\General\DefaultThumbnailQuery;
use App\Modules\Telegram\Actions\Queries\General\EditLanguageQuery;
use App\Modules\Telegram\Actions\Queries\General\GetThumbnailQuery;
use App\Modules\Telegram\Actions\Queries\Upgrade\UpgradeQuery;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetAudioFormatQuery;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetQualityQuery;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetTypeQuery;
use App\Modules\Telegram\Actions\Queries\YouTube\YouTubeSetVideoFormatQuery;
use App\Modules\Telegram\Actions\Texts\ChangeAudioPerformerText;
use App\Modules\Telegram\Actions\Texts\ChangeAudioTitleText;
use App\Modules\Telegram\Actions\Texts\ChangeNameText;
use App\Modules\Telegram\Enums\ActionEnum;
use App\Modules\Telegram\Tools\Message;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProcess;
use Illuminate\Support\Facades\App;

class BotController extends Controller
{
    public $chatId , $message , $messageId , $callback_query , $user , $back = false , $fileEnum , $file , $isUrl , $isCommand = false;

    public function run()
    {
        try {
            $request = (file_get_contents('php://input'));
            file_put_contents('bot.json',$request);
            $request = json_decode($request,true);

            if (isset($request["callback_query"])) {
                $this->messageId = $request['callback_query']['message']['message_id'];
                $this->chatId = $request['callback_query']['from']['id'];
                $this->user = $request['callback_query']['from'];

                $this->callback_query = json_decode($request['callback_query']['data'],true);
            } elseif (isset($request['message']['text'])) {
                $this->messageId = $request['message']['message_id'];
                $this->chatId = $request['message']['from']['id'];
                $this->user = $request['message']['from'];
                $this->isCommand = ($request['message']['entities'][0]['type'] ?? null) == 'bot_command';
                $this->isUrl = ( ($request['message']['entities'][0]['type'] ?? null) == 'url') && filter_var($request['message']['text'], FILTER_VALIDATE_URL);

                if ($this->isUrl) {
                    $this->file = $request['message']['text'];
                } else {
                    $this->message = $request['message']['text'];
                }
            } elseif (isset($request['message']['document'])) {
                $this->messageId = $request['message']['message_id'];
                $this->chatId = $request['message']['from']['id'];
                $this->user = $request['message']['from'];

                $this->fileEnum = FileEnum::DOCUMENT->value;
                $this->file = $request['message']['document'];
            } elseif (isset($request['message']['photo'])) {
                $this->messageId = $request['message']['message_id'];
                $this->chatId = $request['message']['from']['id'];
                $this->user = $request['message']['from'];

                $this->fileEnum = FileEnum::PHOTO->value;
                $this->file = $request['message']['photo'];
            } elseif (isset($request['message']['audio'])) {
                $this->messageId = $request['message']['message_id'];
                $this->chatId = $request['message']['from']['id'];
                $this->user = $request['message']['from'];

                $this->fileEnum = FileEnum::AUDIO->value;
                $this->file = $request['message']['audio'];
            } elseif (isset($request['message']['video'])) {
                $this->messageId = $request['message']['message_id'];
                $this->chatId = $request['message']['from']['id'];
                $this->user = $request['message']['from'];

                $this->fileEnum = FileEnum::VIDEO->value;
                $this->file = $request['message']['video'];
            } else {
                exit();
            }
            if (UserProcess::query()->where('telegram_chat_id',$this->chatId)->where('created_at','>',now()->subMinute())->count() < 12) {
                $initial_memory = memory_get_usage();
                $initial_time = time();
                $this->process();
                $final_memory = memory_get_usage();
                $final_time = time();

                UserProcess::query()->create([
                    'telegram_chat_id' => $this->chatId,
                    'cpu_usage' => sys_getloadavg()[0],
                    'memory_usage' => $final_memory - $initial_memory,
                    'time_left' => $final_time - $initial_time
                ]);
            } else {
                Message::to($this->chatId)->reply($this->messageId)->send('⚠️Server too busy. Please try again later.');
            }
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            report($e);
            Message::to($this->chatId)->reply($this->messageId)->send('❌️Server error. Please try again later.');
        }

        exit();
    }

    public function process()
    {
        $user = User::query()->where('telegram_chat_id',$this->chatId)->firstOr(function () {
            $user = User::query()->create([
                'telegram_chat_id' => $this->chatId,
                'name' => ($this->user['first_name'] ?? '').' '.($this->user['last_name'] ?? '')
            ]);
            $user->refresh();
            (new EditLanguageQuery())->process($user ,$this->messageId);
            exit();
        });
        App::setLocale($user->lang);

        if ($this->callback_query) {
            $this->processCallback($user);
        } elseif ($this->isCommand) {
            $this->processCommand($user);
        } elseif ($this->file) {
            $this->processFile($user);
        } elseif ($user->action) {
            $this->processAction($user);
        } else {
            exit();
        }
    }

    private function processCommand(User $user)
    {
        switch ($this->message) {
            case config('bot.commands.start'):
                (new StartCommand())->execute($user , $this->messageId , $this->message);
                break;
            case config('bot.commands.usage'):
                (new UsageCommand())->execute($user , $this->messageId , $this->message);
                break;
            case config('bot.commands.upgrade'):
                (new UpgradeCommand())->execute($user , $this->messageId , $this->message);
                break;
            case config('bot.commands.settings'):
                (new SettingsCommand())->execute($user , $this->messageId , $this->message);
                break;
            case config('bot.commands.default_thumbnail'):
                (new DefaultThumbnailCommand())->execute($user , $this->messageId , $this->message);
                break;
            default:
        }
    }

    private function processCallback(User $user)
    {
        switch ($this->callback_query['action']) {
            // SUBSCRIPTION
            case ActionEnum::SELECT_SUBSCRIPTION->value:
                (new UpgradeQuery())->process($user,$this->messageId ,$this->callback_query,$this->back);
                break;
            case ActionEnum::FINISH_PAYMENT->value:
                (new UpgradeQuery())->callback($user ,$this->messageId,$this->callback_query,$this->back);
                break;
            // END SUBSCRIPTION
            //----------------------------------------------------------------------------------------------------------
            // LANGUAGE
            case ActionEnum::EDIT_LANGUAGE->value:
                (new EditLanguageQuery())->process($user,$this->messageId,$this->callback_query,$this->back);
                break;
            case ActionEnum::SEND_LANGUAGE->value:
                (new EditLanguageQuery())->change($user , $this->messageId , $this->callback_query,$this->back);
                break;
            // END LANGUAGE
            //----------------------------------------------------------------------------------------------------------
            // DEFAULT THUMBNAIL
            case ActionEnum::DEFAULT_THUMBNAIL->value:
                (new DefaultThumbnailQuery())->process($user,$this->messageId , $this->callback_query,$this->back);
                break;
            case ActionEnum::DEFAULT_THUMBNAIL_SET_ORIGINAL_SIZE->value:
                (new SettingsCommand())->originalSize($user,$this->messageId , $this->callback_query);
                break;
            case ActionEnum::GET_DEFAULT_THUMBNAIL->value:
                (new DefaultThumbnailQuery())->getDefault($user,$this->messageId , $this->callback_query);
                break;
            case ActionEnum::UPDATE_DEFAULT_THUMBNAIL->value:
                (new DefaultThumbnailQuery())->changeDefault($user,$this->messageId , $this->callback_query);
                break;
            case ActionEnum::DELETE_DEFAULT_THUMBNAIL->value:
                (new DefaultThumbnailQuery())->deleteDefault($user,$this->messageId , $this->callback_query);
                break;
            // END DEFAULT THUMBNAIL
            //----------------------------------------------------------------------------------------------------------
            // FILE
            case ActionEnum::CHANGE_FILE_NAME->value:
                (new ChangeFileNameQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::CHANGE_AUDIO_TITLE->value:
                (new ChangeAudioTitleQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::CHANGE_AUDIO_PERFORMER->value:
                (new ChangeAudioPerformerQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::CHANGE_THUMBNAIL->value:
                (new ChangeThumbnailQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::GET_THUMBNAIL->value:
                (new GetThumbnailQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::AUTO_CONVERT->value:
                (new AutoConvertQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::APPLY_CHANGES->value:
                (new ApplyChangeQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::ADD_SOFT_SUB->value:
                (new AddSubTitleQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            // END FILE
            //----------------------------------------------------------------------------------------------------------
            // YOUTUBE
            case ActionEnum::YOUTUBE_SET_VIDEO_FORMAT->value:
                (new YouTubeSetVideoFormatQuery)->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::YOUTUBE_SET_AUDIO_FORMAT->value:
                (new YouTubeSetAudioFormatQuery)->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::YOUTUBE_SET_QUALITY->value:
                (new YouTubeSetQualityQuery)->process($user,$this->messageId,$this->callback_query);
                break;
            case ActionEnum::YOUTUBE_SET_TYPE->value:
                (new YouTubeSetTypeQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            // END YOUTUBE
            //----------------------------------------------------------------------------------------------------------
            // COMMANDS
            case ActionEnum::START->value:
                (new StartCommand())->execute($user , $this->messageId,null,$this->back);
                break;
            case ActionEnum::SETTINGS->value:
                (new SettingsCommand())->execute($user , $this->messageId,null,$this->back);
                break;
            case ActionEnum::UPGRADE->value:
                (new UpgradeCommand())->execute($user , $this->messageId,null,$this->back);
                break;
            // END COMMANDS
            //----------------------------------------------------------------------------------------------------------
            // GET DOWNLOAD URL
            case ActionEnum::GET_DOWNLOAD_LINK->value:
                (new DownloadLinkQuery())->process($user,$this->messageId,$this->callback_query);
                break;
            // END GET DOWNLOAD URL
            //----------------------------------------------------------------------------------------------------------
            // BACK
            case ActionEnum::BACK->value:
                $this->callback_query['action'] = $this->callback_query['step'];
                $this->back = true;
                $this->processCallback($user);
                break;
        }
    }

    public function processFile(User $user)
    {
        if ($user->action) {
            switch ($user->action->action){
                case ActionEnum::UPDATE_DEFAULT_THUMBNAIL->value:
                    (new DefaultThumbnail())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    return;
                case ActionEnum::CHANGE_THUMBNAIL->value:
                    (new Thumbnail())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    return;
                case ActionEnum::ADD_SOFT_SUB->value:
                    (new Subtitle())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    return;
            }
        }

        if ($this->isUrl) {
            if (preg_match('/(?:youtube\.com\/(?:watch\?.*v=|embed\/|v\/)|youtu\.be\/)([a-zA-Z0-9_-]{11})/i', $this->file)) {
                (new YouTube())->process($user,$this->messageId,$this->file,$this->isUrl);
            } else {
                (new Url())->process($user,$this->messageId,$this->file,$this->isUrl);
            }
        } else {
            switch ($this->fileEnum) {
                case FileEnum::PHOTO->value:
                    (new Photo())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    break;
                case FileEnum::AUDIO->value:
                    (new Audio())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    break;
                case FileEnum::VIDEO->value:
                    (new Video())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    break;
                case FileEnum::DOCUMENT->value:
                    (new Document())->process($user,$this->messageId,$this->file,$this->isUrl,$this->fileEnum);
                    break;
            }
        }
    }

    private function processAction(User $user)
    {
        switch ($user->action->action){
            case ActionEnum::CHANGE_FILE_NAME->value:
                (new ChangeNameText())->process($user,$this->messageId,$this->message);
                break;
            case ActionEnum::CHANGE_AUDIO_TITLE->value:
                (new ChangeAudioTitleText())->process($user,$this->messageId,$this->message);
                break;
            case ActionEnum::CHANGE_AUDIO_PERFORMER->value:
                (new ChangeAudioPerformerText())->process($user,$this->messageId,$this->message);
                break;
        }
    }
}
