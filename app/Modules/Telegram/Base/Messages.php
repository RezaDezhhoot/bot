<?php

namespace App\Modules\Telegram\Base;

use App\Modules\Localization\Enums\Langs;

class Messages
{
    public static function getSelectLangMessage(): string
    {
        return  "•\n".
            trans('general.lang.select',[],Langs::FA->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::AR->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::EN->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::IT->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::TR->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::RU->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::ES->value)
            ."\n\n".
            trans('general.lang.select',[],Langs::OZ->value).
             "\n•";
    }
}
