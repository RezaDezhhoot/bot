<?php

namespace App\Modules\File\Tools;

use App\Modules\File\Core\File;
use danog\MadelineProto\Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class General extends File
{

    public function create(): Model|Builder
    {
        // TODO: Implement create() method.
    }

    public function update($data): bool
    {
        if ($this->file instanceof \App\Modules\File\Models\File)
        {
            $updated =  $this->file->update($data);
            $this->file->refresh();
            return $updated;
        }
        return false;
    }

    public function process(): static
    {
        // TODO: Implement process() method.
    }

    public static function download($name ,$url , $storage = 'public' , $prefix = null): string|bool
    {
        $path = generateTemplatePath($prefix).'/'.$name;
        $ok = Storage::disk($storage)->put($path,file_get_contents($url));
        return  $ok ? $path : false;
    }

    public static function downloadUrlFileWithProgress($name , $file , $messageId , $user, $prefix = null , $to_telegram = true): string|bool
    {
        set_time_limit(0);

        $urlInstance = (new \App\Modules\Telegram\Actions\Medias\Url());
        try {
            $path = generateTemplatePath($prefix).'/';
            $path_with_name = $path.$name;
            Storage::makeDirectory($path);

            $localFile = Storage::path(
                $path_with_name
            );

            $ch = curl_init($file->url);
            $fp = fopen($localFile, "wb");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_NOPROGRESS, false);
            curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) use ($user,$file,$urlInstance,$messageId) {
                if ($downloadSize > 0) {
                    $progress = ($downloaded / $downloadSize) * 100;
                    $downloadSpeed = curl_getinfo($resource, CURLINFO_SPEED_DOWNLOAD);

                    $message = $urlInstance->getMessage($file, __('file.url.result2',[
                        'name' => $file->name,
                        'size' => formatBytes($file->size),
                        'progress' => round($progress,1).'% - ('.formatBytes($downloadSpeed).'/S)'
                    ]));
                    $urlInstance->getAPI($user,$file,true)->edit($messageId ,$message);
                }
            });

            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);

            curl_close($ch);
            $result = fclose($fp);

            if ($result && $to_telegram) {
                $message = $urlInstance->getMessage($file, __('file.url.result3',[
                    'name' => $file->name,
                    'size' => formatBytes($file->size),
                ]));
                $urlInstance->getAPI($user,$file,true)->edit($messageId ,$message);
            }

            if ($result) {
                return $path_with_name;
            } else throw new Exception('Error while downloading file');
        } catch (\Exception $e) {
            report($e);
            $message = $urlInstance->getMessage($file, __('file.url.result1',[
                'name' => $file->name,
                'size' => formatBytes($file->size),
            ]));
            $urlInstance->getAPI($user,$file)->keyboard()->edit($messageId ,$message);
            return false;
        }
    }
}
