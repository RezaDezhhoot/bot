<?php

namespace App\Modules\File\Tools;

use App\Modules\File\Core\File;
use App\Modules\File\Models\File as FileModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Photo extends File
{

    public function create(): Model|Builder
    {
        $file = FileModel::query()->create([
            'name' => Str::uuid(),
            'user_id' => $this->user->id,
            'format' => $this->format,
            'size' => $this->size,
            'type' => $this->type,
            'path' => $this->path,
            'data' => $this->file,
            'storage' => $this->storage,
            'file_id' => $this->file['file_id'] ?? null
        ]);
        $file->refresh();

        return $file;
    }

    public function update($data): bool
    {
        // TODO: Implement update() method.
    }

    public function process(): static
    {
        $this->file = end($this->file);
        $this->size = $this->file['file_size'];
        return $this;
    }
}
