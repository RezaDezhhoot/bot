<?php

namespace App\Modules\File\Tools;

use App\Modules\File\Core\File;
use App\Modules\File\Enums\ThumbnailEnum;
use App\Modules\File\Models\File as FileModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Audio extends File
{
    public $title , $performer , $duration ;

    public function create(): Model|Builder
    {
        $file = FileModel::query()->create([
            'name' => $this->name,
            'user_id' => $this->user->id,
            'format' => $this->format,
            'size' => $this->size,
            'type' => $this->type,
            'path' => $this->path,
            'data' => $this->file,
            'storage' => $this->storage,
            'file_id' => $this->file_id ?? null,
            'audio_title' => $this->title,
            'audio_performer' => $this->performer,
            'thumbnail' => $this->thumbnail,
            'duration' => $this->duration,
            'thumbnail_type' => $this->thumbnail_type,
            'mime' => $this->mime
        ]);
        $file->refresh();

        return $file;
    }

    public function update($data): bool
    {
        if ($this->file instanceof \App\Modules\File\Models\File) {
            $updated =  $this->file->update($data);
            $this->file->refresh();
            return $updated;
        }

        return false;
    }

    public function process(): static
    {
        $this->mime = $this->file['mime_type'] ?? 'audio/mpeg';
        if (!isset($this->file['file_name'])) {
            $ex = mime2ext($this->mime);
            $this->name = Str::uuid().'.'.$ex;
        } else {
            $this->name = $this->file['file_name'];
        }
        $this->size = $this->file['file_size'];
        $this->title = $this->file['title'] ?? null;
        $this->performer = $this->file['performer'] ?? null;
        $this->file_id = $this->file['file_id'];
        $this->duration = $this->file['duration'] ?? 0;
        if (isset($this->file['thumbnail'])) {
            $this->thumbnail = $this->file['thumbnail']['file_id'];
            $this->thumbnail_type = ThumbnailEnum::DEFAULT->value;
        }

        return $this;
    }
}
