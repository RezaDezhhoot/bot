<?php

namespace App\Modules\File\Tools;

use App\Modules\File\Core\File;
use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Models\File as FileModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Url extends File
{
    public $url , $read_remote = false;
    public function create(): Model|Builder
    {
        $file = FileModel::query()->create([
            'name' => $this->name,
            'user_id' => $this->user->id,
            'format' => $this->format,
            'size' => $this->size,
            'type' => $this->type,
            'path' => $this->path,
            'data' => $this->file,
            'storage' => $this->storage,
            'file_id' => $this->file_id ?? null,
            'thumbnail' => $this->thumbnail,
            'thumbnail_type' => $this->thumbnail_type,
            'mime' => $this->mime,
            'url' => $this->url,
            'auto_convert' => true,
            'read_remote' => $this->read_remote
        ]);
        $file->refresh();
        $this->file = $file;
        return $file;
    }

    public function update($data): bool
    {
        if ($this->file instanceof FileModel) {
            $updated =  $this->file->update($data);
            $this->file->refresh();
            return $updated;
        }

        return false;
    }

    public function process(): static
    {
        $this->size = $this->file['content-length'] ?? $this->file['Content-Length'];
        $this->mime = $this->file['content-type'] ?? $this->file['Content-Type'];
        $this->url = $this->file['url'];
        $this->read_remote = $this->file['read_remote'] ?? false;
        $parts =  parse_url($this->url);
        $url = $parts['scheme'] . '://' . $parts['host'] . $parts['path'];

        if (isset($this->file['content-disposition']) || isset($this->file['Content-Disposition'])) {
            $attachment = explode('filename=',($this->file['content-disposition'] ?? $this->file['Content-Disposition']));
            $filename = $attachment[count($attachment) - 1];
            $this->name = $filename;
        } elseif (preg_match('/[^\/?]+\.[^\/.]+$/', $url, $matches)) {
            $this->name = $matches[0];
        } else {
            $ex = mime2ext($this->mime);
            $this->name = Str::uuid().'.'.$ex;
        }
        $category = explode('/',$this->mime)[0];
        $this->format = match ($category) {
            'video' => FileEnum::VIDEO->value,
            'audio' => FileEnum::AUDIO->value,
            'image' => FileEnum::PHOTO->value,
            default => FileEnum::DOCUMENT->value,
        };

        return $this;
    }
}
