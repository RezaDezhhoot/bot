<?php

namespace App\Modules\File\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Tools\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    public function __invoke($token)
    {
        $file = General::on(base64_decode($token))->getFile();
        $file->increment('downloads');
        return response()->download(Storage::path($file->path), $file->name)
            ->deleteFileAfterSend(false);
    }
}
