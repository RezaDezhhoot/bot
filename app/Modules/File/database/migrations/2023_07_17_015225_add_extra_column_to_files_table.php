<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('files', function (Blueprint $table) {
            $table->json('data')->nullable();
            $table->boolean('name_changed')->default(false);
            $table->boolean('thumbnail_changed')->default(false);
            $table->boolean('audio_title_changed')->default(false);
            $table->boolean('audio_performer_changed')->default(false);
            $table->text('soft_sub')->nullable();
            $table->string('file_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn(['data','name_changed','thumbnail_changed','audio_title_changed','audio_performer_changed','soft_sub','file_id']);
        });
    }
};
