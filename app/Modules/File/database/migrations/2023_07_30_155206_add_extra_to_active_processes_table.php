<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('active_processes', function (Blueprint $table) {
            $table->string('process_type')->nullable()->default(\App\Modules\Telegram\Enums\ProcessType::NORMAL->value);
            $table->foreignId('file_id')->nullable()->constrained('files')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('active_processes', function (Blueprint $table) {
            $table->dropColumn(['process_type','file_id']);
        });
    }
};
