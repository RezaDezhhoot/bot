<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'v1',
    'middleware' => ['api'],
    'as' => 'file.'
],function (){
    Route::get('file/{token}',\App\Modules\File\Controllers\Api\V1\FileController::class)->name('get');
});
