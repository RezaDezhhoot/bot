<?php

namespace App\Modules\File\Observers;

use App\Modules\File\Models\File;
use Illuminate\Support\Facades\Storage;

class FileObserver
{
    public function deleted(File $file): void
    {
        if (!is_null($file->path) && Storage::disk($file->storage)->exists($file->path)) {
            Storage::disk($file->storage)->delete($file->path);
        }
    }
}
