<?php

namespace App\Modules\File\Commands;

use App\Modules\File\Models\File;
use Illuminate\Console\Command;

class RemoveFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        File::query()
            ->oldest()->whereNotNull('link_generated_at')
            ->where('link_generated_at' , '<' ,now()->subDays(1))
            ->chunk(20,function ($files) {
                foreach ($files as $file) {
                    $file->delete();
                }
            });
    }
}
