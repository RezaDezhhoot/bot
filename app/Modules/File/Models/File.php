<?php

namespace App\Modules\File\Models;

use App\Modules\User\Models\User;
use App\Modules\YouTube\Models\YouTube;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed $has_changed
 * @property mixed $auto_convert
 * @property mixed $path
 * @property mixed $storage
 * @property mixed $user
 * @property mixed $soft_sub
 * @property mixed $subtitle_name
 * @property mixed $name
 */
class File extends Model
{
    use HasFactory , SoftDeletes;

    protected $guarded = ['id'];

    public $casts = [
        'data' => 'array'
    ];

    public $appends = ['was_changed','url'];

    const TEMPLATE_FOLDER = '/templates';

    const MAX_FILE_SIZE_TO_SEND = 2 * 1024 * 1024;


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeToday($q)
    {
        return $q->whereRaw('DATE(created_at) = ?', [now()->format('Y-m-d')]);
    }

    public function scopeConverted($q)
    {
        return $q->where('converted',true);
    }

    public function getTimeAttribute(): string
    {
        $medias = $this->duration ?? 0;
        $hours = floor($medias / 3600);
        $minutes = floor($medias / 60 % 60);
        $secs = floor($medias % 60);
        return sprintf('%02d:%02d:%02d', $hours,$minutes,$secs);
    }

    public function getWasChangedAttribute(): bool
    {
        return $this->has_changed || $this->auto_convert;
    }

    public function getWasChangedUrlAttribute(): bool
    {
        return $this->has_changed || !$this->auto_convert;
    }

    public function youtube(): BelongsTo
    {
        return  $this->belongsTo(YouTube::class);
    }

    public function url(): Attribute
    {
        return Attribute::get(function ($value){
            $url = $value;
            if (!is_null($url)) {
                if ($this->read_remote) {
                    $url = config('bot.remote_servers.fileApi.get_file_content').'?url='.$url;
                }
            }

            return $url;
        });
    }
}
