<?php

namespace App\Modules\File\Enums;

enum FileEnum: string
{
    case PHOTO = 'photo';
    case VIDEO = 'video';
    case AUDIO = 'audio';
    case DOCUMENT = 'document';
}
