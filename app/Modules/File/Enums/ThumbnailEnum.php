<?php

namespace App\Modules\File\Enums;

enum ThumbnailEnum: string
{
    case DEFAULT = 'default';
    case CUSTOMIZED = 'customized';
}
