<?php

namespace App\Modules\File\Enums;

use App\Traits\ArrayableEnum;

enum Types: string
{
    use ArrayableEnum;

    case MEDIA = 'media';
    case FILE = 'file';
    case LINK = 'link';
    case YOUTUBE = 'youtube';

    public function label(): string
    {
        return match ($this) {
            self::FILE => trans('file.types.file'),
            self::MEDIA => trans('file.types.media'),
            self::LINK => trans('file.types.link'),
            self::YOUTUBE => trans('file.types.youtube'),
        };
    }
}
