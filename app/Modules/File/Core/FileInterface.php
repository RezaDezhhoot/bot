<?php

namespace App\Modules\File\Core;

use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface FileInterface
{
    public function setUser(User $user): static;

    public function setFormat($format): static;

    public function setType($type): static;

    public function setPath($path): static;

    public function setStorage($storage): static;

    public function setThumbnail($thumbnail): static;

    public function setDefaultThumbnailIfDoesntExist($thumbnail): static;

    public function nameChanged(): static;

    public function thumbnailChanged(): static;

    public function getFile(): Model|Builder|array|null;

    public function create(): Model|Builder;

    public function update($data): bool;

    public function delete(): bool;

    public static function download($name , $url , $storage = 'public', $prefix = null): string|bool;

    public function process(): static;
}
