<?php

namespace App\Modules\File\Core;

use App\Modules\File\Enums\FileEnum;
use App\Modules\File\Enums\ThumbnailEnum;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

abstract class File implements FileInterface
{
    protected $name , $user , $format , $size , $type , $path = null , $storage = 'public' , $thumbnail = null , $data = null;
    protected $name_changed = false , $mime = null, $thumbnail_changed = false , $file_id ,  $thumbnail_type = null;

    protected $file = null;

    public static function on($file): static
    {
        return new static($file);
    }

    public function __construct($file)
    {
        if (is_array($file))
            $this->file = $file;
        else {
            $this->file = \App\Modules\File\Models\File::query()->where('id',$file)->orWhere('file_id',$file)->firstOrFail();
        }
    }

    public function setUser(User $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function setFormat($format): static
    {
        $this->format = $format;
        return $this;
    }

    public function setType($type): static
    {
        $this->type = $type;
        return $this;
    }

    public function setPath($path): static
    {
        $this->path = $path;
        return $this;
    }

    public function setStorage($storage): static
    {
        $this->storage = $storage;
        return $this;
    }

    public function setThumbnail($thumbnail): static
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    public function setDefaultThumbnailIfDoesntExist($thumbnail): static
    {
        if (is_null($this->thumbnail)) {
            $this->thumbnail = $thumbnail;
            $this->thumbnail_type = ThumbnailEnum::CUSTOMIZED->value;
        }
        return $this;
    }


    public function nameChanged(): static
    {
        $this->name_changed = true;
        return $this;
    }

    public function thumbnailChanged(): static
    {
        $this->thumbnail_changed = true;
        return $this;
    }

    public function getFile(): Model|Builder|array|null
    {
        return $this->file;
    }

    public function delete(): bool
    {
        if ($this->file instanceof \App\Modules\File\Models\File)
            return $this->file->delete();

        return false;
    }

    public static function download($name,$url , $storage = 'public', $prefix = null): string|bool
    {
        return '';
    }
}
