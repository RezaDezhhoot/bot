<?php
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth','admin']
],function (){
    Route::get('/dashboard',\App\Modules\Dashboard\Controllers\Admin\Dashboard::class)->name('dashboard');
});
