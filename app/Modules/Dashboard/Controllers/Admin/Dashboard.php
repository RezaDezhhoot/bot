<?php

namespace App\Modules\Dashboard\Controllers\Admin;

use App\Http\Controllers\BaseComponent;
use App\Modules\Currency\Enums\FiatCurrency;
use App\Modules\File\Models\File;
use App\Modules\Payment\Models\Payment;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProcess;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;

class Dashboard extends BaseComponent
{
    public $from_date , $to_date , $box;

    public $to_date_viwe , $from_date_view;

    protected $queryString = ['to_date','from_date'];

    public function mount()
    {
        if (
            !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$this->to_date) ||
            !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$this->from_date)
        )
        {
            $this->reset(['to_date','from_date']);
        }

        if (!isset($this->to_date)){
            $this->to_date =  Carbon::now()->format('Y-m-d');
        }
        $this->to_date_viwe = $this->dateConverter($this->to_date);

        if (!isset($this->from_date)){
            $this->from_date = Carbon::now()->subDays(5)->format('Y-m-d');
        }
        $this->from_date_view = $this->dateConverter($this->from_date);

        $this->getData();
    }

    public function confirmFilter()
    {
        $from_date = $this->dateConverter($this->from_date_view,'m');
        $to_date = $this->dateConverter($this->to_date_viwe ,'m');
        redirect()->route('admin.dashboard',
            [
                'from_date'=> $from_date,
                'to_date'=> $to_date,
            ]
        );
    }

    public function runChart()
    {
        $this->emit('runChart',$this->getChartData());
    }

    public function getData()
    {
        $this->box = [
            'users' => User::query()->count(),
            'files' => File::query()->count(),
            'processed_files' => File::query()->withTrashed()->converted()->count(),
            'payments' => Payment::query()->paid()->cursor()->map(function ($v) {
                $v->fiat_price = exchange(
                    $v->fiat_currency , FiatCurrency::USD->value, (float) $v->fiat_price
                );
                return $v;
            })->sum('fiat_price'),
            'space_in_use' => formatBytes(File::query()->whereNotNull('link_generated_at')->sum('size')),
            'downloads' => File::query()->withTrashed()->sum('downloads')
        ];
    }

    public function getChartData(): bool|string
    {
        $dates = $this->getDates();
        $chart = [];
        $chartModels = ['payments','memory_usage','cpu_usage','files'];
        foreach ($chartModels as $chartModel ) {
            $chart[$chartModel] = [];
            $chart['label'] = [];
            for ($i = 0 ; $i< count($dates); $i++) {
                switch ($chartModel) {
                    case 'payments':
                        $chart[$chartModel][] = Payment::query()->paid()
                            ->whereBetween('created_at',[$dates[$i]->format('Y-m-d') . " 00:00:00", $dates[$i]->format('Y-m-d') . " 23:59:59" ])
                            ->cursor()->map(function ($v) {
                                $v->fiat_price = exchange(
                                    $v->fiat_currency , FiatCurrency::USD->value, (float) $v->fiat_price
                                );
                                return $v;
                            })->sum('fiat_price');
                        break;
                    case 'memory_usage':
                        $chart[$chartModel][] = UserProcess::query()
                            ->whereBetween('created_at',[$dates[$i]->format('Y-m-d') . " 00:00:00", $dates[$i]->format('Y-m-d') . " 23:59:59" ])
                            ->sum('memory_usage');
                        break;
                    case 'cpu_usage':
                        $chart[$chartModel][] = UserProcess::query()
                            ->whereBetween('created_at',[$dates[$i]->format('Y-m-d') . " 00:00:00", $dates[$i]->format('Y-m-d') . " 23:59:59" ])
                            ->sum('cpu_usage');
                        break;
                    case 'files':
                        $chart[$chartModel][] = File::query()
                            ->whereBetween('created_at',[$dates[$i]->format('Y-m-d') . " 00:00:00", $dates[$i]->format('Y-m-d') . " 23:59:59" ])
                            ->count();
                        break;
                }

                $chart['label'][] = (string)$dates[$i]->format('Y-m-d');
            }
        }
        return json_encode($chart);
    }

    public function getDates()
    {
        $period = CarbonPeriod::create($this->from_date, $this->to_date);
        foreach ($period as $date) {
            $date->format('Y-m-d');
        }
        return $period->toArray();
    }

    public function render()
    {
        return view('admin.dashboard.dashboard')->extends('admin.layouts.admin');
    }
}
