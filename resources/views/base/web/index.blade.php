<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Telegram Bot - Telefilegram</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f7f7f7;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }

        header {
            background-color: #4CAF50;
            color: #fff;
            text-align: center;
            padding: 20px;
        }

        h1 {
            margin: 0;
            font-size: 36px;
        }

        .container {
            max-width: 1200px;
            margin: 0 auto;
            padding: 40px 20px;
            flex: 1;
        }

        .feature-box {
            display: flex;
            align-items: center;
            margin-bottom: 20px;
        }

        .feature-icon {
            flex: 0 0 80px;
            height: 80px;
            background-color: #4CAF50;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 24px;
            color: #fff;
            margin-right: 20px;
        }

        .feature-text {
            flex: 1;
        }

        .cta-button {
            display: inline-block;
            background-color: #4CAF50;
            color: #fff;
            padding: 10px 20px;
            font-size: 18px;
            border-radius: 5px;
            text-decoration: none;
        }

        .cta-button:hover {
            background-color: #45a049;
        }

        footer {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 20px;
        }
    </style>
</head>
<body>
<header>
    <h1>Telefilegram</h1>
    <p>Awesome Telegram Bot for All Your Needs</p>
</header>

<div class="container">
    <div class="feature-box">
        <div class="feature-icon">🚀</div>
        <div class="feature-text">
            <h2>Powerful Features</h2>
            <p>Discover a wide range of powerful and useful features to enhance your Telegram experience.</p>
        </div>
    </div>

    <div class="feature-box">
        <div class="feature-icon">🤖</div>
        <div class="feature-text">
            <h2>Smart Automation</h2>
            <p>Let the bot handle repetitive tasks and automate your workflows efficiently.</p>
        </div>
    </div>

    <div class="feature-box">
        <div class="feature-icon">🔒</div>
        <div class="feature-text">
            <h2>Secure & Private</h2>
            <p>Your data and conversations are safe and secure with end-to-end encryption.</p>
        </div>
    </div>

    <div class="cta">
        <a href="{{ config('bot.address') }}" class="cta-button">Start Now</a>
    </div>
</div>

<footer>
    <p>Contact: <a style="color: #fff" href="https://t.me/telefilegram_sup">TeleFileGram-Sup</a></p>
</footer>
</body>
</html>
