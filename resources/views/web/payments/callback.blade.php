<!DOCTYPE html>
<html lang="{{str_replace('_', '-', app()->getLocale())}}">
<head>
    <meta charset="UTF-8">
    <title>{{ __('payment.invoice.invoice') }}</title>
    <!-- Include Bootstrap CSS -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white text-center">
                    <h1>{{ __('payment.invoice.invoice') }}</h1>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-6">
                            <p>{{ __('payment.invoice.invoice') }} #{{ $payment->tracking_code }}</p>
                            <p>{{ __('payment.invoice.date') }}: {{ $payment->date }}</p>
                        </div>
                        <div class="col-6 text-right">
                            <p>{{ __('payment.invoice.customer') }}: {{ $payment->user->name ?? '-' }}</p>
                            <p>{{ __('payment.invoice.phone') }}: {{ $payment->user->phone ?? '-' }}</p>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ __('payment.invoice.subscription') }}</th>
                            <th>{{ __('payment.invoice.currency') }}</th>
                            <th>{{ __('payment.invoice.amount') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $payment->subscription->title }}</td>
                            <td>{{ $payment->fiat_currency }}</td>
                            <td>{{ number_format($payment->fiat_price) }}</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" class="text-right"><strong>{{ __('payment.invoice.total') }}:</strong></td>
                            <td class="bg-danger text-white">
                                {{ number_format($payment->fiat_price).' '.$payment->fiat_currency }}
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="card-footer text-muted text-center">
                    <p>
                        <a href="{{config('bot.address')}}">{{ __('general.btn.back') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Include Bootstrap JS and jQuery -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
</body>
</html>
