<div>
    @section('title','واحد های پول')
    <x-admin.form-control mode="{{$mode}}" title="واحد های پول"/>

    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title">{{ $header }}</h3>
        </div>
        <x-admin.forms.validation-errors/>
        <div class="card-body">
            <div class="row">
                <x-admin.forms.input  type="number" id="value" label="مقدار واحد*" wire:model.defer="value"/>
            </div>
        </div>
    </div>
</div>
