<div>
    @section('title','رسیده ها ')
    <x-admin.form-control  title="رسیده ها"/>
    <div class="card card-custom">
        <div class="card-body">
           <div class="row">
               <x-admin.forms.dropdown id="status" with="6" :data="$data['status']" label="وضعیت کلی رسید" wire:model="status"/>
               <x-admin.forms.dropdown id="transaction_status" with="6" :data="$data['transaction_status']" label="وضعیت سفارش روی بلاکچین" wire:model="transaction_status"/>
           </div>
            <div id="kt_datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                @include('admin.layouts.advance-table')
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped table-bordered" id="kt_datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <td>کد رهگیری</td>
                                <td>مبلغ فیات</td>
                                <td>واحد فیات</td>
                                <td>مبلغ کریپتو</td>
                                <td>واحد کریپتو</td>
                                <td>درگاه</td>
                                <td>وضیعت</td>
                                <td>اشتراک</td>
                                <td>کاربر</td>
                                <td>وضعیت سفارش روی بلاکچین</td>
                                <td>مبلغ سفارش</td>
                                <td>مبلغ پرداخت شده</td>
                                <td>ادرس پرداخت</td>
                                <td>تاریخ </td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->tracking_code }}</td>
                                    <td>{{ number_format($item->fiat_price) }}</td>
                                    <td>{{ $item->fiat_label }}</td>
                                    <td>{{ $item->crypto_price ?? '-' }}</td>
                                    <td>{{ $item->crypto_label ?? '-' }}</td>
                                    <td>{{ $item->gateway }}</td>
                                    <td>{{ $item->status_label }}</td>
                                    <td>
                                         <span>
                                                عنوان : {{ $item->subscription_data['title'] }}
                                            </span>
                                        <br>
                                        <span>
                                               قیمت : {{ $item->subscription_data['price'].' '.$item->subscription_data['fiat_currency'] }}
                                            </span>
                                        <br>
                                        <span>
                                               اعتبار : {{ $item->subscription_data['subscription_day'] }}
                                            </span>
                                        <br>
                                        <span>
                                               حجم فایل : {{ to_gb($item->subscription_data['subscription_size'])." GB" }}
                                            </span>
                                        <br>
                                        <span>
                                               حجم روزانه : {{ to_gb($item->subscription_data['daly_volume'])." GB" }}
                                        </span>
                                    </td>
                                    <td>
                                         <span>
                                            Id : {{ $item->user->id ?? '-' }}
                                        </span>
                                        <br>
                                        <span>
                                            Telegram Id : {{ $item->user->telegram_chat_id ?? '-' }}
                                        </span>
                                        <br>
                                        <span>
                                            نام  : {{ $item->user->name ?? '-' }}
                                        </span>
                                        <br>
                                        <span>
                                            شماره  : {{ $item->user->phone ?? '-' }}
                                        </span>
                                    </td>
                                    <td>{{ $item->transaction_status }}</td>
                                    <td>{{ $item->paid_amount }}</td>
                                    <td>{{ $item->actually_paid }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->date }}</td>
                                </tr>
                            @empty
                                <td class="text-center" colspan="20">
                                    دیتایی جهت نمایش وجود ندارد
                                </td>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$items->links('admin.layouts.paginate')}}
        </div>
    </div>
</div>
