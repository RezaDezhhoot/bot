<div xmlns:wire="http://www.w3.org/1999/xhtml">
    @section('title','کاربر ')
    <x-admin.form-control mode="{{$mode}}" title="کاربران"/>
    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title">{{ $header }}</h3>
        </div>
        <x-admin.forms.validation-errors/>
        <div class="card-body">
            <div class="row">
                <x-admin.forms.input with="4" type="text" id="first_name" label="نام*" wire:model.defer="name"/>
                <x-admin.forms.input with="4" type="text" id="phone" label="شماره همراه*" wire:model.defer="phone"/>
                <x-admin.forms.input with="4" type="text" id="country_code" label="کد کشور*" wire:model.defer="country_code"/>
                <x-admin.forms.input with="6" type="email" id="email" label="ایمیل" wire:model.defer="email"/>
                <x-admin.forms.dropdown with="6" id="role" :data="$data['role']" label="نقش*" wire:model.defer="role"/>
                @if($mode == self::CREATE_MODE)
                    <x-admin.forms.input type="password" help="حداقل {{ 6 }} حرف شامل اعداد و حروف" id="password" label="گذرواژه*" wire:model.defer="password"/>
                @endif
                <x-admin.forms.input  type="number" min="1" max="100000000" id="day" label="تمدید" help="برحسب روز از تاریخ حال" wire:model.defer="day"/>

            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function deleteItem(id) {
            Swal.fire({
                title: 'حذف تاریخپه!',
                text: 'آیا از حذف این تاریخپه اطمینان دارید؟',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'خیر',
                confirmButtonText: 'بله'
            }).then((result) => {
                if (result.value) {
                @this.call('deleteConfig', id)
                }
            })
        }
    </script>
@endpush
