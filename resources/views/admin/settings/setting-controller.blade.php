<div>
    @section('title','تنظیمات پایه')
    <x-admin.form-control title="تنطیمات "/>
    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title">{{ $header }}</h3>
        </div>
        <x-admin.forms.validation-errors/>
        <div class="card-body">
{{--            <x-admin.form-section  label="کانال های اجباری">--}}
{{--                <div class="border p-3">--}}
{{--                    <x-admin.button class="btn btn-light-primary font-weight-bolder btn-sm" content="افزودن کانال" wire:click="addChannel()" />--}}
{{--                    @foreach($channels as $key => $item)--}}
{{--                        <div class="form-group d-flex align-items-center col-12">--}}
{{--                            <input class="form-control col-4" id="{{ $key }}channelsLink" type="text" placeholder="لینک" wire:model.defer="channels.{{$key}}.link">--}}
{{--                            <input class="form-control col-4" id="{{ $key }}channelsId" type="text" placeholder="ای دی" wire:model.defer="channels.{{$key}}.id">--}}
{{--                            <input class="form-control col-3" id="{{ $key }}channelsTitle" type="text" placeholder="عنوان" wire:model.defer="channels.{{$key}}.title">--}}
{{--                            <div><button class="btn btn-light-danger font-weight-bolder btn-sm" wire:click="deleteChannel({{ $key }})">حذف</button></div>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}

{{--            </x-admin.form-section>--}}
            <div class="row">
                <x-admin.forms.input with="4" type="number" id="default_size" help="بر حسب مگابایت"  label="حجم اپلود فایل پیشفرض*" wire:model.defer="default_size"/>
                <x-admin.forms.input with="4" type="number" id="default_daly_volume" help="بر حسب مگابایت"  label="حجم پیشفرض روزانه*" wire:model.defer="default_daly_volume"/>
            </div>
        </div>
    </div>
</div>
