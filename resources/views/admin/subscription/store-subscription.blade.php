<div xmlns:wire="http://www.w3.org/1999/xhtml">
    @section('title','اشتراک ')
    <x-admin.form-control deleteAble="true" deleteContent="حذف اشتراک" mode="{{$mode}}" title="اشتراک"/>
    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title">{{ $header }}</h3>
        </div>
        <x-admin.forms.validation-errors/>
        <div class="card-body">
            <div class="row">
                <x-admin.forms.input with="4" type="text" id="title" label="عنوان*" wire:model.defer="title"/>
                <x-admin.forms.input with="4" type="number" help="برحسب ارز فیات" id="price" label="قیمت *" wire:model.defer="price"/>
                <x-admin.forms.input with="4" type="number" id="subscription_day" help="بر حسب روز" label="اعتبار اشتراک*" wire:model.defer="subscription_day"/>
                <x-admin.forms.input with="4" type="number" id="subscription_size" help="بر حسب مگابایت"  label="حجم اشتراک برای اپلود*" wire:model.defer="subscription_size"/>
                <x-admin.forms.input with="4" type="number" id="daly_volume" help="بر حسب مگابایت"  label="حجم روزانه*" wire:model.defer="daly_volume"/>
                <x-admin.forms.dropdown with="4" id="status" :data="$data['status']" label="وضعیت*" wire:model.defer="status"/>
                <x-admin.forms.dropdown with="6" id="crypto_currency" :data="$data['crypto_currency']" label="واحد پول کریپتو*" wire:model.defer="crypto_currency"/>
                <x-admin.forms.dropdown with="6" id="fiat_currency" :data="$data['fiat_currency']" label="واحد پول فیات*" wire:model.defer="fiat_currency"/>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function deleteItem(id) {
            Swal.fire({
                title: 'حذف !',
                text: 'آیا از حذف ا اطمینان دارید؟',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'خیر',
                confirmButtonText: 'بله'
            }).then((result) => {
                if (result.value) {
                @this.call('deleteItem')
                }
            })
        }
    </script>
@endpush
