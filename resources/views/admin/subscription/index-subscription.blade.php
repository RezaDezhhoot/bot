<div>
    @section('title','پلن های اشتراکی ')
    <x-admin.form-control link="{{ route('admin.subscription.store',['create'] ) }}" title="پلن های اشتراکی"/>
    <div class="card card-custom">
        <div class="card-body">
            <x-admin.forms.dropdown id="status" :data="$data['status']" label="وضعیت" wire:model="status"/>
            <div id="kt_datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                @include('admin.layouts.advance-table')
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped table-bordered" id="kt_datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th> عنوان</th>
                                <th> قیمت</th>
                                <th>اعتبار</th>
                                <th>حجم مجاز</th>
                                <th>وضعیت</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ number_format($item->price).' '.$item->fiat_currency }}</td>
                                    <td>{{ $item->subscription_day }} روز </td>
                                    <td>{{ $item->subscription_size }} MB</td>
                                    <td>{{ $item->status }}</td>
                                    <td>
                                        <x-admin.edit-btn href="{{ route('admin.subscription.store',['edit', $item->id]) }}" />
                                    </td>
                                </tr>
                            @empty
                                <td class="text-center" colspan="12">
                                    دیتایی جهت نمایش وجود ندارد
                                </td>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$items->links('admin.layouts.paginate')}}
        </div>
    </div>
</div>
